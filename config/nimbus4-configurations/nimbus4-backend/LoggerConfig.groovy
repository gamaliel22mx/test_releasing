import org.apache.log4j.CustodianDailyRollingFileAppender

import static com.verecloud.nimbus4.util.Nimbus4Constants.INTEGRATION_CALLS_LOGGER

String catalinaBase = System.properties.getProperty('catalina.base')
String userHome = System.properties.getProperty('user.home')
if (!catalinaBase) catalinaBase = userHome
String logDir = "${catalinaBase}/Nimbus4Logs/nimbus4-backend"

// log4j configuration
log4j = {
	// Example of changing the log pattern for the default console appender:
	//
	appenders {
//		console name: 'stdout', layout: pattern(conversionPattern: '%c{2} %m%n')
		appender new CustodianDailyRollingFileAppender(name: "integrationRollingAppender", file: "${logDir}/integration-calls.log", layout: pattern(conversionPattern: '%p %t - %m%n'), maxNumberOfDays: 30, compress: true)
	}

	info 'integrationRollingAppender': INTEGRATION_CALLS_LOGGER

	error 'org.codehaus.groovy.grails.web.servlet',        // controllers
			'org.codehaus.groovy.grails.web.pages',          // GSP
			'org.codehaus.groovy.grails.web.sitemesh',       // layouts
			'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
			'org.codehaus.groovy.grails.web.mapping',        // URL mapping
			'org.codehaus.groovy.grails.commons',            // core / classloading
			'org.codehaus.groovy.grails.plugins',            // plugins
			'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
			'org.springframework',
			'org.hibernate',
			'net.sf.ehcache.hibernate'

	environments {
		development {
			debug "com.verecloud"
		}

		test {
			debug 'com.odobo',
					'org.pac4j',
					"grails.app.filters",
					"grails.app.controllers",
					"grails.app.services",
					"grails.app.domains",
					"com.verecloud",
					"org.apache.shiro",
					"grails.app.realms"
		}
		idt2 {
			info 'com.verecloud.nimbus4.integration.handlers'
		}
		iat {
			debug 'com.verecloud.nimbus4',
					'com.odobo',
					'org.pac4j',
					"grails.app.filters",
					"grails.app.controllers",
					"grails.app.services",
					"grails.app.domains",
					"com.verecloud",
					"org.apache.shiro",
					"grails.app.realms"

			info 'com.verecloud.nimbus4.integration.handlers'
		}

	}
}