environments {
	itg {
		rabbitmq {
			connectionfactory {
				username = 'itg'
				password = 'itg'
				hostname = 'bluesky-db-dev'
				virtualHost = 'itg'
				className = 'com.rabbitmq.client.ConnectionFactory'
			}
		}
	}
}
