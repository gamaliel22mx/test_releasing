import org.apache.log4j.CustodianDailyRollingFileAppender

import static com.verecloud.nimbus4.util.Nimbus4Constants.INTEGRATION_CALLS_LOGGER

String catalinaBase = System.properties.getProperty('catalina.base')
String userHome = System.properties.getProperty('user.home')
if (!catalinaBase) catalinaBase = userHome
String logDir = "${catalinaBase}/Nimbus4Logs/nimbus4-soap"

// log4j configuration
log4j = {

	// Example of changing the log pattern for the default console appender:
	//
	appenders {
		console name: 'stdout', layout: pattern(conversionPattern: '%-5p  %c %d{dd.MM.yyyy HH:mm:ss} -- %m%n')
		appender new CustodianDailyRollingFileAppender(name: "integrationRollingAppender", file: "${logDir}/integration-calls.log", layout: pattern(conversionPattern: '%p %t - %m%n'), maxNumberOfDays: 30, compress: true)
	}

	info 'integrationRollingAppender': INTEGRATION_CALLS_LOGGER

	info 'com.verecloud',
			"grails.app.filters",
			"grails.app.controllers",
			"grails.app.services",
			"grails.app.domains",
			"grails.app.realms",
			"org.apache.shiro",
			'org.apache.cxf',
			'org.grails.cxf'

	error 'org.codehaus.groovy.grails.web.servlet',        // controllers
			'org.codehaus.groovy.grails.web.pages',          // GSP
			'org.codehaus.groovy.grails.web.sitemesh',       // layouts
			'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
			'org.codehaus.groovy.grails.web.mapping',        // URL mapping
			'org.codehaus.groovy.grails.commons',            // core / classloading
			'org.codehaus.groovy.grails.plugins',            // plugins
			'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
			'org.springframework',
			'org.hibernate',
			'net.sf.ehcache.hibernate',
			"grails.app.filters",
			"grails.app.controllers",
			"grails.app.domains"

	debug 'com.verecloud.nimbus4.ws'

}