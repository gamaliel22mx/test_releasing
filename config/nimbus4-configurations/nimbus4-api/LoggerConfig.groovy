import org.apache.log4j.CustodianDailyRollingFileAppender

import static com.verecloud.nimbus4.util.Nimbus4Constants.INTEGRATION_CALLS_LOGGER
import static com.verecloud.nimbus4.util.Nimbus4Constants.NIMBUS4_API_INCOMING_CALLS_LOGGER

String catalinaBase = System.properties.getProperty('catalina.base')
String userHome = System.properties.getProperty('user.home')
if (!catalinaBase) catalinaBase = userHome
String logDir = "${catalinaBase}/Nimbus4Logs/nimbus4-api"

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    appenders {
        console name: 'stdout', layout: pattern(conversionPattern: '%-5p  %c %d{dd.MM.yyyy HH:mm:ss} -- %m%n')
        appender new CustodianDailyRollingFileAppender(name: "apiRollingAppender", file: "${logDir}/nimbus4-api-incoming-calls.log", layout: pattern(conversionPattern: '%p %t - %m%n'), maxNumberOfDays: 30, compress: true)
        appender new CustodianDailyRollingFileAppender(name: "integrationRollingAppender", file: "${logDir}/integration-calls.log", layout: pattern(conversionPattern: '%p %t - %m%n'), maxNumberOfDays: 30, compress: true)
    }

    info 'apiRollingAppender': NIMBUS4_API_INCOMING_CALLS_LOGGER
    info 'integrationRollingAppender': INTEGRATION_CALLS_LOGGER

    debug 'grails.app'

    info "grails.app.filters",
            "grails.app.controllers",
//            "grails.app.services",
            "grails.app.domains",
            "com.verecloud.nimbus4"
    error 'org.codehaus.groovy.grails.web.servlet',        // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate',
            "grails.app.filters",
            "grails.app.controllers",
            "grails.app.services",
            "grails.app.domains",
            "com.verecloud.nimbus4"
    debug "com.verecloud.nimbus4.multitenancy.DomainTenantResolver"
    //"com.verecloud.nimbus4.gson.serializer"
    warn "com.verecloud.nimbus4.permission"

    environments {
        development {
            debug 'com.odobo',
//					'org.pac4j',
//					"grails.app.filters",
                    "grails.app.controllers",
                    "grails.app.services",
                    "grails.app.domains",
//					"com.verecloud",
//					"org.apache.shiro",
                    //"org.apache.http.headers",
                    //"org.apache.http.wire",
                    "grails.app.realms",
                    "com.verecloud.nimbus4.sql"
//                    "security"
        }
    }
}