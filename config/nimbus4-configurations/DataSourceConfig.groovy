// environment specific settings
environments {
	//standalone envs
	itg {
		dataSource {
			username = 'ci_bluesky_itg'
			password = 'ci_bluesky_itg'
			dbCreate = "create" // one of 'create', 'create-drop', 'update', 'validate', '', "none" 
			url = "jdbc:mysql://bluesky-db-dev:3306/ci_bluesky_itg?autoReconnect=true"
		}
		mongo {
			clientURI = "mongodb://nimbusadmin:nimbusadmin@bluesky-db-dev/itg"
			db = "itg"
			clean = false
		}
	}
}
