//Environment Specific Changes
environments {
	itg {
		configurations = [
				"customerPortal.content.baseurl"               : "http://ciitg-bluesky.verecloud.com/content/",
				"orderConfirmation.email.from"                 : "test.support@verecloud.com",
				"orderConfirmation.email.salesTeamEmailAddress": "test.sales@verecloud.com",
				"ecommerceURL"                                 : "https://hybris-uat.wgcloudconnect.com/blueskyb2bacceleratorstorefront/bluesky/en/USD/productlist"
		]
		mail = [
				host    : "mail10.myoutlookonline.com",
				port    : 587,
				protocol: "smtp",
				username: "test.support@verecloud.com",
				password: "Tsupport!",
				props   : [
						"mail.smtp.auth"           : true,
						"mail.smtp.starttls.enable": true,
						"mail.smtp.quitwait"       : false
				],
				support : "test.support@verecloud.com",
				from    : "test.support@verecloud.com"
		]
	}
}

//Added for DB Migration Plugin
environments {
	itg { grails { plugin { databasemigration = [updateOnStart: false] } } }
}

//added for oauth credentials
environments {
	itg {
		admin.client.id = "2121d837de4eaa730c3b69560d1b538e181ebbb"
		global.client.id = "4d6965dcb02bb5bc82e6e75b42895270ada591eb"
		global.client.secret = "9e3e3a9e161bb45fa756e4f12fbd4ad1a1652f758bb390480812a7badd24f658"
		oauth.server.url = "http://localhost:3000/oauth20"
		stored.procedures.location = '/home/gs-user/gitrepo/nimbus4/sql'
	}

}

//added for activit-rest
environments {
	itg {
		activiti.rest.baseurl = "http://ciitg-bluesky-apps:9080/activiti-rest"
		activiti.username = "kermit"
		activiti.password = "kermit"
	}
}
