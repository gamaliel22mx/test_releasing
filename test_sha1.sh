#!/bin/bash
#./deploy_app.sh -a com.verecloud.nimbus4,nimbus4-admin,LATEST,release_candidate -a com.verecloud.nimbus4,nimbus4-api,LATEST,release_candidate

tomcat_path="/home/gs-user/apache-tomcat-8.0.14"
group_id=""
artifact_id=""
version=""
repository_id=""
artifact_hash=""

is_deploy=false

url_nexus="https://nexus.verecloud.com/service/local/artifact/maven"
art_redir="/redirect"
art_resolve="/resolve"
NEXUS_USER="deployment"
NEXUS_PASSWORD="nexusAWS-artifactDeploy_1"

ENTITY="" 
CONTENT=""

parse(){
        IFS=',' read -a array <<< "$1"
        group_id=${array[0]}
        artifact_id=${array[1]}
        version=${array[2]}
        repository_id=${array[3]}
}

process_hash(){
	ORIGINAL_IFS=$IFS
    	IFS=\>
    	read -d \< ENTITY CONTENT
    	IFS=$ORIGINAL_IFS
}

get_hash(){
        echo "................ get hash for artifact ..................................................."
	
	url="${url_nexus}${art_resolve}?g=${group_id}&a=${artifact_id}&v=${version}&r=${repository_id}&e=war"
        filename="/opt/artifact/${artifact_id}.xml" 

	if [ -a $filename ]
        then
                rm -r $filename
      	fi

        curl -sS -L -u ${NEXUS_USER}:${NEXUS_PASSWORD} ${url} -o $filename

	echo "------------------ before searching the hash"

	while process_hash; do
    		if [[ $ENTITY = "sha1" ]] ; then
        		artifact_hash=$CONTENT
			echo "----- The artifact hash is: $CONTENT ------------"
			break
    		fi
	done < $filename

	 echo "------------------ after searching the hash"

		
        echo "................ get hash for artifact  done ............................................."
}

fetchArtifact(){
        echo "................ donwloding artifact: ${artifact_id} ${version} ${repository_id} ... ... ..."
        url="${url_nexus}${art_redir}?g=${group_id}&a=${artifact_id}&v=${version}&r=${repository_id}&e=war"
	filename="/opt/artifact/${artifact_id}.war"
        curl -sS -L -u ${NEXUS_USER}:${NEXUS_PASSWORD} ${url} -o $filename
		
	if [ -z $filename ]
        then
			echo "................. Error artifact not donwloded from nexus .........."
            exit 1    
        fi
		
	if [ -s $file_size ]
        then
			echo "................. artifact donwloded .........."    
        fi
}

verifyArtifact(){
	filename="/opt/artifact/${artifact_id}.war"
	temp=($(sha1sum $filename))

        if [ "$artifact_hash" == "$temp"  ]
        then
	    echo "................................................................................................................"
            echo "--- The artifact downloaded correctly ---"
	    echo "hash file:  $temp"
            echo "hash nexus: $artifact_hash"
            echo "................................................................................................................"
	else
	    echo "................................................................................................................"
            echo "--- Error diferent hash ---"
	    echo "hash file:  $temp"
	    echo "hash nexus: $artifact_hash"
            echo "................................................................................................................"
        fi
}

processArtifact(){
        parse "$1"
		
		if [[ -z $version ]]
		then
			version="LATEST"
		fi
		
		if [[ -z $repository_id ]]
		then
			repository_id="release_candidate"
		fi
		
        if [[ -z $group_id ]] || [[ -z $artifact_id ]]
        then
                echo "--- Missing value: $1"
        else
		get_hash
                fetchArtifact
		verifyArtifact
        fi
}

while getopts "ha:" option; do
        case $option in
        h)
                mesage
        ;;
        a)
                processArtifact "$OPTARG"
        ;;
        \?)
                echo "--- Invalid opcion: $OPTARG"
        ;;
        :)
                echo "--- Option -$OPTARG requieres an argument"
        ;;
        esac
done


