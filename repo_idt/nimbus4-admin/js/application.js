if (typeof jQuery !== 'undefined') {
	(function ($) {
		$('#spinner').ajaxStart(function () {
			$(this).fadeIn();
		}).ajaxStop(function () {
			$(this).fadeOut();
		});
	})(jQuery);
}

function getCurrentUri() {
	var currentUri = window.location.pathname + window.location.search;
	currentUri = currentUri.substring(1);
	var index = currentUri.indexOf("/");
	if (index > 0) {
		currentUri = currentUri.substring(index);
	}
	return currentUri;
}

function validateTenantValue() {
	$("#targetUri").val(getCurrentUri());
	var tenants = $("#tenant").data("source");
	var isValidTenant = false;
	//TODO get messages from properties file.... getting null therefore hardcoded
	var invalidTenantMessage = "Tenant not found."//messages.default.tenant.invalid()
	var errorMessage = "Some error occurred while processing your request."//messages.default.tenant.error()
	var message = ""
	try {
		isValidTenant = $.inArray($("#tenant").val(), tenants) != -1;
		message = isValidTenant ? "" : invalidTenantMessage
	} catch (ex) {
		message = errorMessage
	}
	if (message.length > 0) {
		message = '<div class="alert alert-warning alert-dismissible" role="alert"> ' +
		'<button type="button" class="close" data-dismiss="alert">' +
		'<span aria-hidden="true">&times;</span>' +
		'<span class="sr-only">Close</span>' +
		'</button> ' +
		message +
		'</div>';
		$("#errorDiv").html(message);
	}

	return isValidTenant;
}
function getDataAsJson(form) {
	var array = $(form).serializeArray();
	var json = {};
	jQuery.each(array, function () {
		json[this.name] = this.value || '';
	});

	delete json.formData;

	var jsonString = JSON.stringify(json);

	$("#formData").val(jsonString);
	return jsonString;
}