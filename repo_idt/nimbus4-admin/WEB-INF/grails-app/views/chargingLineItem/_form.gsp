<%@ page import="com.verecloud.nimbus4.billing.ChargingLineItem" %>


<div class="${hasErrors(bean: chargingLineItemInstance, field: 'quantity', 'error')} ">
	<label for="quantity" class="control-label"><g:message code="chargingLineItem.quantity.label" default="Quantity"/> </label>
	<div>
		
			<g:field class="form-control" name="quantity" type="number" value="${chargingLineItemInstance.quantity}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'unitPrice', 'error')} ">
	<label for="unitPrice" class="control-label"><g:message code="chargingLineItem.unitPrice.label" default="Unit Price"/> </label>
	<div>
		
			<g:field class="form-control" name="unitPrice" value="${chargingLineItemInstance.unitPrice}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'chargeType', 'error')} required">
	<label for="chargeType" class="control-label"><g:message code="chargingLineItem.chargeType.label" default="Charge Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="chargeType" from="${com.verecloud.nimbus4.service.enums.ChargeType?.values()}" keys="${com.verecloud.nimbus4.service.enums.ChargeType.values()*.name()}" required="" value="${chargingLineItemInstance?.chargeType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'chargingLineItemGroup', 'error')} required">
	<label for="chargingLineItemGroup" class="control-label"><g:message code="chargingLineItem.chargingLineItemGroup.label" default="Charging Line Item Group"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="chargingLineItemGroup" name="chargingLineItemGroup.id" from="${com.verecloud.nimbus4.billing.ChargingLineItemGroup.list()}" optionKey="id" required="" value="${chargingLineItemInstance?.chargingLineItemGroup?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'createDate', 'error')} required">
	<label for="createDate" class="control-label"><g:message code="chargingLineItem.createDate.label" default="Create Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="createDate" precision="day"  value="${chargingLineItemInstance?.createDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'distributorCurrency', 'error')} required">
	<label for="distributorCurrency" class="control-label"><g:message code="chargingLineItem.distributorCurrency.label" default="Distributor Currency"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:currencySelect class='form-control' name="distributorCurrency" value="${chargingLineItemInstance?.distributorCurrency}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'distributorRatedCharges', 'error')} required">
	<label for="distributorRatedCharges" class="control-label"><g:message code="chargingLineItem.distributorRatedCharges.label" default="Distributor Rated Charges"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="distributorRatedCharges" value="${chargingLineItemInstance.distributorRatedCharges}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'period', 'error')} ">
	<label for="period" class="control-label"><g:message code="chargingLineItem.period.label" default="Period"/> </label>
	<div>
		
			<g:textField class="form-control" name="period" value="${chargingLineItemInstance?.period}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'productCode', 'error')} ">
	<label for="productCode" class="control-label"><g:message code="chargingLineItem.productCode.label" default="Product Code"/> </label>
	<div>
		
			<g:textField class="form-control" name="productCode" value="${chargingLineItemInstance?.productCode}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'productDescription', 'error')} ">
	<label for="productDescription" class="control-label"><g:message code="chargingLineItem.productDescription.label" default="Product Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="productDescription" value="${chargingLineItemInstance?.productDescription}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'resellerCurrency', 'error')} required">
	<label for="resellerCurrency" class="control-label"><g:message code="chargingLineItem.resellerCurrency.label" default="Reseller Currency"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:currencySelect class='form-control' name="resellerCurrency" value="${chargingLineItemInstance?.resellerCurrency}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'resellerRatedCharges', 'error')} required">
	<label for="resellerRatedCharges" class="control-label"><g:message code="chargingLineItem.resellerRatedCharges.label" default="Reseller Rated Charges"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="resellerRatedCharges" value="${chargingLineItemInstance.resellerRatedCharges}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'supplierCurrency', 'error')} required">
	<label for="supplierCurrency" class="control-label"><g:message code="chargingLineItem.supplierCurrency.label" default="Supplier Currency"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:currencySelect class='form-control' name="supplierCurrency" value="${chargingLineItemInstance?.supplierCurrency}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'supplierProductCode', 'error')} ">
	<label for="supplierProductCode" class="control-label"><g:message code="chargingLineItem.supplierProductCode.label" default="Supplier Product Code"/> </label>
	<div>
		
			<g:textField class="form-control" name="supplierProductCode" value="${chargingLineItemInstance?.supplierProductCode}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'supplierRatedCharges', 'error')} required">
	<label for="supplierRatedCharges" class="control-label"><g:message code="chargingLineItem.supplierRatedCharges.label" default="Supplier Rated Charges"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="supplierRatedCharges" value="${chargingLineItemInstance.supplierRatedCharges}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="chargingLineItem.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

