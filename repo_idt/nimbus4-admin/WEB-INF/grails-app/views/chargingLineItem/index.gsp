<%@ page import="com.verecloud.nimbus4.billing.ChargingLineItem" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'chargingLineItem.label', default: 'ChargingLineItem')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-chargingLineItem" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="quantity" title="${message(code: 'chargingLineItem.quantity.label', default: 'Quantity')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="unitPrice" title="${message(code: 'chargingLineItem.unitPrice.label', default: 'Unit Price')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="chargeType" title="${message(code: 'chargingLineItem.chargeType.label', default: 'Charge Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="chargingLineItem.chargingLineItemGroup.label" default="Charging Line Item Group"/></th>
				
					<g:sortableColumn property="createDate" title="${message(code: 'chargingLineItem.createDate.label', default: 'Create Date')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="distributorCurrency" title="${message(code: 'chargingLineItem.distributorCurrency.label', default: 'Distributor Currency')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="distributorRatedCharges" title="${message(code: 'chargingLineItem.distributorRatedCharges.label', default: 'Distributor Rated Charges')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="period" title="${message(code: 'chargingLineItem.period.label', default: 'Period')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="productCode" title="${message(code: 'chargingLineItem.productCode.label', default: 'Product Code')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="productDescription" title="${message(code: 'chargingLineItem.productDescription.label', default: 'Product Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="resellerCurrency" title="${message(code: 'chargingLineItem.resellerCurrency.label', default: 'Reseller Currency')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="resellerRatedCharges" title="${message(code: 'chargingLineItem.resellerRatedCharges.label', default: 'Reseller Rated Charges')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="supplierCurrency" title="${message(code: 'chargingLineItem.supplierCurrency.label', default: 'Supplier Currency')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="supplierProductCode" title="${message(code: 'chargingLineItem.supplierProductCode.label', default: 'Supplier Product Code')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="supplierRatedCharges" title="${message(code: 'chargingLineItem.supplierRatedCharges.label', default: 'Supplier Rated Charges')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="chargingLineItem.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${chargingLineItemInstanceList}" status="i" var="chargingLineItemInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${chargingLineItemInstance.id}">${chargingLineItemInstance}</g:link></td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "quantity")}</td>
					
					<td>
						
						${fieldValue(bean: chargingLineItemInstance, field: "unitPrice")}
						
					</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "chargeType")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(chargingLineItemInstance?.chargingLineItemGroup)?.class?.simpleName}" action="show" id="${chargingLineItemInstance?.chargingLineItemGroup?.id}">${chargingLineItemInstance?.chargingLineItemGroup?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${chargingLineItemInstance.createDate}"/></td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "distributorCurrency")}</td>
					
					<td>
						
						${fieldValue(bean: chargingLineItemInstance, field: "distributorRatedCharges")}
						
					</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "period")}</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "productCode")}</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "productDescription")}</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "resellerCurrency")}</td>
					
					<td>
						
						${fieldValue(bean: chargingLineItemInstance, field: "resellerRatedCharges")}
						
					</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "supplierCurrency")}</td>
					
					<td>${fieldValue(bean: chargingLineItemInstance, field: "supplierProductCode")}</td>
					
					<td>
						
						${fieldValue(bean: chargingLineItemInstance, field: "supplierRatedCharges")}
						
					</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${chargingLineItemInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(chargingLineItemInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${chargingLineItemInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
