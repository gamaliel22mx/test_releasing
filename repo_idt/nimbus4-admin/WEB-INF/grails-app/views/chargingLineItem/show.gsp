<%@ page import="com.verecloud.nimbus4.billing.ChargingLineItem" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'chargingLineItem.label', default: 'ChargingLineItem')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-chargingLineItem" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${chargingLineItemInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(chargingLineItemInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.quantity.label" default="Quantity" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "quantity")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.unitPrice.label" default="Unit Price" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: chargingLineItemInstance, field: "unitPrice")}
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.chargeType.label" default="Charge Type" /></td>
			
			<td valign="top" class="value">${chargingLineItemInstance?.chargeType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.chargingLineItemGroup.label" default="Charging Line Item Group" /></td>
			
			<td valign="top" class="value"><g:link controller="${chargingLineItemInstance?.chargingLineItemGroup?.class?.simpleName}" action="show" id="${chargingLineItemInstance?.chargingLineItemGroup?.id}">${chargingLineItemInstance?.chargingLineItemGroup?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.createDate.label" default="Create Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingLineItemInstance?.createDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.distributorCurrency.label" default="Distributor Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "distributorCurrency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.distributorRatedCharges.label" default="Distributor Rated Charges" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: chargingLineItemInstance, field: "distributorRatedCharges")}
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.period.label" default="Period" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "period")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.productCode.label" default="Product Code" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "productCode")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.productDescription.label" default="Product Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "productDescription")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.resellerCurrency.label" default="Reseller Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "resellerCurrency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.resellerRatedCharges.label" default="Reseller Rated Charges" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: chargingLineItemInstance, field: "resellerRatedCharges")}
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.supplierCurrency.label" default="Supplier Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "supplierCurrency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.supplierProductCode.label" default="Supplier Product Code" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemInstance, field: "supplierProductCode")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItem.supplierRatedCharges.label" default="Supplier Rated Charges" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: chargingLineItemInstance, field: "supplierRatedCharges")}
				
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
