<%@ page import="com.verecloud.nimbus4.party.User" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-user" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'user.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'user.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="externalId" title="${message(code: 'user.externalId.label', default: 'External Id')}"/>
				
					<g:sortableColumn property="firstName" title="${message(code: 'user.firstName.label', default: 'First Name')}"/>
				
					<g:sortableColumn property="lastName" title="${message(code: 'user.lastName.label', default: 'Last Name')}"/>
				
					<g:sortableColumn property="email" title="${message(code: 'user.email.label', default: 'Email')}"/>
				
					<g:sortableColumn property="phoneNumber" title="${message(code: 'user.phoneNumber.label', default: 'Phone Number')}"/>
				
					<g:sortableColumn property="activationDate" title="${message(code: 'user.activationDate.label', default: 'Activation Date')}"/>
				
					<g:sortableColumn property="deactivationDate" title="${message(code: 'user.deactivationDate.label', default: 'Deactivation Date')}"/>
				
					<g:sortableColumn property="password" title="${message(code: 'user.password.label', default: 'Password')}"/>
				
					<g:sortableColumn property="token" title="${message(code: 'user.token.label', default: 'Token')}"/>
				
					<g:sortableColumn property="admin" title="${message(code: 'user.admin.label', default: 'Admin')}"/>
				
					<g:sortableColumn property="passwordExpirationDate" title="${message(code: 'user.passwordExpirationDate.label', default: 'Password Expiration Date')}"/>
				
					<g:sortableColumn property="tokenExpirationDate" title="${message(code: 'user.tokenExpirationDate.label', default: 'Token Expiration Date')}"/>
				
					<g:sortableColumn property="language" title="${message(code: 'user.language.label', default: 'Language')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'user.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'user.lastUpdated.label', default: 'Last Updated')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'user.status.label', default: 'Status')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${userInstanceList}" status="i" var="userInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${userInstance.id}">${userInstance}</g:link></td>
					
					<td>${fieldValue(bean: userInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "externalId")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "firstName")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "lastName")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "email")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "phoneNumber")}</td>
					
					<td><g:formatDate date="${userInstance.activationDate}"/></td>
					
					<td><g:formatDate date="${userInstance.deactivationDate}"/></td>
					
					<td>${fieldValue(bean: userInstance, field: "password")}</td>
					
					<td>${fieldValue(bean: userInstance, field: "token")}</td>
					
					<td><g:formatBoolean boolean="${userInstance.admin}"/></td>
							
					<td><g:formatDate date="${userInstance.passwordExpirationDate}"/></td>
					
					<td><g:formatDate date="${userInstance.tokenExpirationDate}"/></td>
					
					<td>${fieldValue(bean: userInstance, field: "language")}</td>
					
					<td><g:formatDate date="${userInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${userInstance.lastUpdated}"/></td>
					
					<td>${fieldValue(bean: userInstance, field: "status")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${userInstanceCount}"/>
	</div>
</section>

</body>

</html>
