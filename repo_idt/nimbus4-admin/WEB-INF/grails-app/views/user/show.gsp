<%@ page import="com.verecloud.nimbus4.party.User" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-user" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.externalId.label" default="External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "externalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.firstName.label" default="First Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "firstName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.lastName.label" default="Last Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "lastName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.email.label" default="Email" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "email")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.phoneNumber.label" default="Phone Number" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "phoneNumber")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.activationDate.label" default="Activation Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userInstance?.activationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.deactivationDate.label" default="Deactivation Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userInstance?.deactivationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.password.label" default="Password" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "password")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.token.label" default="Token" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "token")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.admin.label" default="Admin" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${userInstance?.admin}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.passwordExpirationDate.label" default="Password Expiration Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userInstance?.passwordExpirationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.tokenExpirationDate.label" default="Token Expiration Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userInstance?.tokenExpirationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.language.label" default="Language" /></td>
			
			<td valign="top" class="value">${userInstance?.language?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.permissions.label" default="Permissions" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userInstance, field: "permissions")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${userInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="user.userGroupRoles.label" default="User Group Roles" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${userInstance?.userGroupRoles}" var="u">
	<g:if test="${u}">
		<li>
			<g:if test="${u?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(u?.value?.class)}">
					<g:link
							controller="${u?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${u?.value?.id}">
						${u?.key?.encodeAsHTML()+" : "+u?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${u?.value?.class?.simpleName}"
							action="show"
							id="${u?.value?.id}">
						${u?.key?.encodeAsHTML()+" : "+u?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(u?.class)}">
					<g:link
							controller="${u?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${u?.id}">
						${u?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${u?.class?.simpleName}"
							action="show"
							id="${u?.id}">
						${u?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
