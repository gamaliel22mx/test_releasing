<label style="margin-top: 20px;">Act as :</label>

<%
	String roles = nimbus.fetchActAsRole([id: tenantId])?.toString()
	List roleList = []
	if (roles) {
		roleList = roles.split(",")
	}
%>
<g:select class="form-control" name="role" id="role" from="${roleList}" keys="${roleList*.toString()}"/>