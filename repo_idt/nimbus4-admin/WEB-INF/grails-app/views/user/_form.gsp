<%@ page import="com.verecloud.nimbus4.party.User" %>


<div class="${hasErrors(bean: userInstance, field: 'externalId', 'error')} ">
	<label for="externalId" class="control-label"><g:message code="user.externalId.label" default="External Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="externalId" readonly="readonly" value="${userInstance?.externalId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'firstName', 'error')} required">
	<label for="firstName" class="control-label"><g:message code="user.firstName.label" default="First Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="firstName" required="" value="${userInstance?.firstName}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'lastName', 'error')} required">
	<label for="lastName" class="control-label"><g:message code="user.lastName.label" default="Last Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="lastName" required="" value="${userInstance?.lastName}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'email', 'error')} required">
	<label for="email" class="control-label"><g:message code="user.email.label" default="Email"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" type="email" name="email" required="" value="${userInstance?.email}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'phoneNumber', 'error')} ">
	<label for="phoneNumber" class="control-label"><g:message code="user.phoneNumber.label" default="Phone Number"/> </label>
	<div>
		
			<g:textField class="form-control" name="phoneNumber" value="${userInstance?.phoneNumber}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'activationDate', 'error')} ">
	<label for="activationDate" class="control-label"><g:message code="user.activationDate.label" default="Activation Date"/> </label>
	<div>
		
			<bs:datePicker name="activationDate" precision="day"  value="${userInstance?.activationDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'deactivationDate', 'error')} ">
	<label for="deactivationDate" class="control-label"><g:message code="user.deactivationDate.label" default="Deactivation Date"/> </label>
	<div>
		
			<bs:datePicker name="deactivationDate" precision="day"  value="${userInstance?.deactivationDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'password', 'error')} required">
	<label for="password" class="control-label"><g:message code="user.password.label" default="Password"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" type="password" name="password" required="" value="${userInstance?.password}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'token', 'error')} ">
	<label for="token" class="control-label"><g:message code="user.token.label" default="Token"/> </label>
	<div>
		
			<g:textField class="form-control" name="token" value="${userInstance?.token}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'admin', 'error')} ">
	<label for="admin" class="control-label"><g:message code="user.admin.label" default="Admin"/> </label>
	<div>
		
			<nimbus:checkBox name="admin" value="${userInstance?.admin}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'passwordExpirationDate', 'error')} ">
	<label for="passwordExpirationDate" class="control-label"><g:message code="user.passwordExpirationDate.label" default="Password Expiration Date"/> </label>
	<div>
		
			<bs:datePicker name="passwordExpirationDate" precision="day"  value="${userInstance?.passwordExpirationDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'tokenExpirationDate', 'error')} ">
	<label for="tokenExpirationDate" class="control-label"><g:message code="user.tokenExpirationDate.label" default="Token Expiration Date"/> </label>
	<div>
		
			<bs:datePicker name="tokenExpirationDate" precision="day"  value="${userInstance?.tokenExpirationDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'language', 'error')} ">
	<label for="language" class="control-label"><g:message code="user.language.label" default="Language"/> </label>
	<div>
		
			<g:select class="form-control" name="language" from="${com.verecloud.nimbus4.service.enums.Language?.values()}" keys="${com.verecloud.nimbus4.service.enums.Language.values()*.name()}" value="${userInstance?.language?.name()}" noSelection="['': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'permissions', 'error')} ">
	<label for="permissions" class="control-label"><g:message code="user.permissions.label" default="Permissions"/> </label>
	<div>
		
			<g:textArea class="form-control" name="permissions" cols="40" rows="5" value="${userInstance?.permissions?.join("\n")}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="user.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.PartyStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.PartyStatus.values()*.name()}" required="" value="${userInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userInstance, field: 'userGroupRoles', 'error')} ">
	<label for="userGroupRoles" class="control-label"><g:message code="user.userGroupRoles.label" default="User Group Roles"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${userInstance?.userGroupRoles}" var="u">
			    <g:if test="${u}">
                    <li>
                        <g:if test="${u?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(u?.value?.class)}">
                                <g:link
                                    controller="${u?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${u?.value?.id}">
                                    ${u?.key?.encodeAsHTML()+" : "+u?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${u?.value?.class?.simpleName}"
                                    action="show"
                                    id="${u?.value?.id}">
                                    ${u?.key?.encodeAsHTML()+" : "+u?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(u?.class)}">
                                <g:link
                                    controller="${u?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${u.id}">
                                    ${u?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${u?.class?.simpleName}"
                                    action="show"
                                    id="${u.id}">
                                    ${u?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'userGroupRole').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['user.id': userInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

