<%@ page import="com.verecloud.nimbus4.Note" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'note.label', default: 'Note')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-note" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'note.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'note.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="title" title="${message(code: 'note.title.label', default: 'Title')}"/>
				
					<g:sortableColumn property="detail" title="${message(code: 'note.detail.label', default: 'Detail')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'note.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'note.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="note.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${noteInstanceList}" status="i" var="noteInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${noteInstance.id}">${noteInstance}</g:link></td>
					
					<td>${fieldValue(bean: noteInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: noteInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: noteInstance, field: "title")}</td>
					
					<td>${fieldValue(bean: noteInstance, field: "detail")}</td>
					
					<td><g:formatDate date="${noteInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${noteInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${noteInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(noteInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${noteInstanceCount}"/>
	</div>
</section>

</body>

</html>
