<%@ page import="com.verecloud.nimbus4.Note" %>


<div class="${hasErrors(bean: noteInstance, field: 'title', 'error')} ">
	<label for="title" class="control-label"><g:message code="note.title.label" default="Title"/> </label>
	<div>
		
			<g:textField class="form-control" name="title" value="${noteInstance?.title}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: noteInstance, field: 'detail', 'error')} ">
	<label for="detail" class="control-label"><g:message code="note.detail.label" default="Detail"/> </label>
	<div>
		
			<g:textField class="form-control" name="detail" value="${noteInstance?.detail}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: noteInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="note.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

