<%@ page import="com.verecloud.nimbus4.Note" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'note.label', default: 'Note')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-note" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${noteInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(noteInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: noteInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: noteInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.title.label" default="Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: noteInstance, field: "title")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.detail.label" default="Detail" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: noteInstance, field: "detail")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${noteInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="note.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${noteInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
