<%@ page import="com.verecloud.nimbus4.product.ProductSpecification" %>


<div class="${hasErrors(bean: productSpecificationInstance, field: 'productAddendumUrl', 'error')} ">
	<label for="productAddendumUrl" class="control-label"><g:message code="productSpecification.productAddendumUrl.label" default="Product Addendum Url"/> </label>
	<div>
		
			<g:field class="form-control" type="url" name="productAddendumUrl" value="${productSpecificationInstance?.productAddendumUrl}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'name', 'error')} required">
	<label for="name" class="control-label"><g:message code="productSpecification.name.label" default="Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="name" required="" value="${productSpecificationInstance?.name}"/>
		
	</div>
</div>
<g:set var="timePeriodInstance" value="${productSpecificationInstance?.activeFor}"/>
<fieldset class="embedded"><legend><g:message code="productSpecification.activeFor.label" default="Active For"/></legend>
	
<div class="${hasErrors(bean: productSpecificationInstance, field: 'activeFor.endDate', 'error')} ">
	<label for="activeFor.endDate" class="control-label"><g:message code="productSpecification.activeFor.endDate.label" default="End Date"/> </label>
	<div>
		
			<bs:datePicker name="activeFor.endDate" precision="day"  value="${timePeriodInstance?.endDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'activeFor.startDate', 'error')} required">
	<label for="activeFor.startDate" class="control-label"><g:message code="productSpecification.activeFor.startDate.label" default="Start Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="activeFor.startDate" precision="day"  value="${timePeriodInstance?.startDate}"  />
		
	</div>
</div>
</fieldset>
<div class="${hasErrors(bean: productSpecificationInstance, field: 'externalId', 'error')} ">
	<label for="externalId" class="control-label"><g:message code="productSpecification.externalId.label" default="External Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="externalId" value="${productSpecificationInstance?.externalId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="productSpecification.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${productSpecificationInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'eventsMediator', 'error')} ">
	<label for="eventsMediator" class="control-label"><g:message code="productSpecification.eventsMediator.label" default="Events Mediator"/> </label>
	<div>
		
			<g:select class="form-control" id="eventsMediator" name="eventsMediator.id" from="${com.verecloud.nimbus4.product.ProductEventMediator.list()}" optionKey="id" value="${productSpecificationInstance?.eventsMediator?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'schema', 'error')} required">
	<label for="schema" class="control-label"><g:message code="productSpecification.schema.label" default="Schema"/> <span class="required-indicator">*</span></label>
	<div>
		

		
           <g:textArea class="form-control" name="schema" value="${(productSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(productSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'categories', 'error')} ">
	<label for="categories" class="control-label"><g:message code="productSpecification.categories.label" default="Categories"/> </label>
	<div>
		
			<g:if test="${productSpecificationInstance?.categories?.getClass() && Map.isAssignableFrom(productSpecificationInstance?.categories?.getClass())}"><g:select class="form-control" name="categories" from="${com.verecloud.nimbus4.product.Category.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.categories*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="categories" from="${com.verecloud.nimbus4.product.Category.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.categories*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'pricePlans', 'error')} ">
	<label for="pricePlans" class="control-label"><g:message code="productSpecification.pricePlans.label" default="Price Plans"/> </label>
	<div>
		
			<g:if test="${productSpecificationInstance?.pricePlans?.getClass() && Map.isAssignableFrom(productSpecificationInstance?.pricePlans?.getClass())}"><g:select class="form-control" name="pricePlans" from="${com.verecloud.nimbus4.product.PricePlan.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.pricePlans*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="pricePlans" from="${com.verecloud.nimbus4.product.PricePlan.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.pricePlans*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'productAddendumAccepted', 'error')} ">
	<label for="productAddendumAccepted" class="control-label"><g:message code="productSpecification.productAddendumAccepted.label" default="Product Addendum Accepted"/> </label>
	<div>
		
			<nimbus:checkBox name="productAddendumAccepted" value="${productSpecificationInstance?.productAddendumAccepted}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'productVisualizers', 'error')} ">
	<label for="productVisualizers" class="control-label"><g:message code="productSpecification.productVisualizers.label" default="Product Visualizers"/> </label>
	<div>
		
			<g:if test="${productSpecificationInstance?.productVisualizers?.getClass() && Map.isAssignableFrom(productSpecificationInstance?.productVisualizers?.getClass())}"><g:select class="form-control" name="productVisualizers" from="${com.verecloud.nimbus4.product.ProductVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.productVisualizers*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="productVisualizers" from="${com.verecloud.nimbus4.product.ProductVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.productVisualizers*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'serviceSpecifications', 'error')} ">
	<label for="serviceSpecifications" class="control-label"><g:message code="productSpecification.serviceSpecifications.label" default="Service Specifications"/> </label>
	<div>
		
			<g:if test="${productSpecificationInstance?.serviceSpecifications?.getClass() && Map.isAssignableFrom(productSpecificationInstance?.serviceSpecifications?.getClass())}"><g:select class="form-control" name="serviceSpecifications" from="${com.verecloud.nimbus4.service.ServiceSpecification.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.serviceSpecifications*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="serviceSpecifications" from="${com.verecloud.nimbus4.service.ServiceSpecification.list()}" multiple="multiple" optionKey="id" size="5" value="${productSpecificationInstance?.serviceSpecifications*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="productSpecification.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.SpecificationStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.SpecificationStatus.values()*.name()}" required="" value="${productSpecificationInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productSpecificationInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="productSpecification.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(false)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>

