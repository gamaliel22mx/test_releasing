<%@ page import="com.verecloud.nimbus4.product.ProductSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productSpecification.label', default: 'ProductSpecification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-productSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'productSpecification.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'productSpecification.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="productAddendumUrl" title="${message(code: 'productSpecification.productAddendumUrl.label', default: 'Product Addendum Url')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'productSpecification.name.label', default: 'Name')}"/>
				
				<th><g:message code="productSpecification.activeFor.label" default="Active For"/></th>
				
					<g:sortableColumn property="externalId" title="${message(code: 'productSpecification.externalId.label', default: 'External Id')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'productSpecification.description.label', default: 'Description')}"/>
				
				<th><g:message code="productSpecification.eventsMediator.label" default="Events Mediator"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'productSpecification.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'productSpecification.lastUpdated.label', default: 'Last Updated')}"/>
				
					<g:sortableColumn property="productAddendumAccepted" title="${message(code: 'productSpecification.productAddendumAccepted.label', default: 'Product Addendum Accepted')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'productSpecification.status.label', default: 'Status')}"/>
				
				<th><g:message code="productSpecification.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${productSpecificationInstanceList}" status="i" var="productSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${productSpecificationInstance.id}">${productSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "productAddendumUrl")}</td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "activeFor")}</td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "externalId")}</td>
					
					<td>${fieldValue(bean: productSpecificationInstance, field: "description")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(productSpecificationInstance?.eventsMediator)?.class?.simpleName}" action="show" id="${productSpecificationInstance?.eventsMediator?.id}">${productSpecificationInstance?.eventsMediator?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${productSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${productSpecificationInstance.lastUpdated}"/></td>
					
					<td><g:formatBoolean boolean="${productSpecificationInstance.productAddendumAccepted}"/></td>
							
					<td>${fieldValue(bean: productSpecificationInstance, field: "status")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${productSpecificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productSpecificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${productSpecificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
