<%@ page import="com.verecloud.nimbus4.product.ProductSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productSpecification.label', default: 'ProductSpecification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-productSpecification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${productSpecificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productSpecificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.productAddendumUrl.label" default="Product Addendum Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "productAddendumUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.activeFor.label" default="Active For" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "activeFor")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.externalId.label" default="External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "externalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productSpecificationInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.eventsMediator.label" default="Events Mediator" /></td>
			
			<td valign="top" class="value"><g:link controller="${productSpecificationInstance?.eventsMediator?.class?.simpleName}" action="show" id="${productSpecificationInstance?.eventsMediator?.id}">${productSpecificationInstance?.eventsMediator?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.schema.label" default="Schema" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="schema" value="${(productSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(productSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.categories.label" default="Categories" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productSpecificationInstance?.categories}" var="c">
	<g:if test="${c}">
		<li>
			<g:if test="${c?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
					<g:link
							controller="${c?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.value?.class?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
					<g:link
							controller="${c?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.class?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productSpecificationInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productSpecificationInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.pricePlans.label" default="Price Plans" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productSpecificationInstance?.pricePlans}" var="p">
	<g:if test="${p}">
		<li>
			<g:if test="${p?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(p?.value?.class)}">
					<g:link
							controller="${p?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${p?.value?.id}">
						${p?.key?.encodeAsHTML()+" : "+p?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${p?.value?.class?.simpleName}"
							action="show"
							id="${p?.value?.id}">
						${p?.key?.encodeAsHTML()+" : "+p?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(p?.class)}">
					<g:link
							controller="${p?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${p?.id}">
						${p?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${p?.class?.simpleName}"
							action="show"
							id="${p?.id}">
						${p?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.productAddendumAccepted.label" default="Product Addendum Accepted" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${productSpecificationInstance?.productAddendumAccepted}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.productVisualizers.label" default="Product Visualizers" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productSpecificationInstance?.productVisualizers}" var="p">
	<g:if test="${p}">
		<li>
			<g:if test="${p?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(p?.value?.class)}">
					<g:link
							controller="${p?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${p?.value?.id}">
						${p?.key?.encodeAsHTML()+" : "+p?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${p?.value?.class?.simpleName}"
							action="show"
							id="${p?.value?.id}">
						${p?.key?.encodeAsHTML()+" : "+p?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(p?.class)}">
					<g:link
							controller="${p?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${p?.id}">
						${p?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${p?.class?.simpleName}"
							action="show"
							id="${p?.id}">
						${p?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.serviceSpecifications.label" default="Service Specifications" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productSpecificationInstance?.serviceSpecifications}" var="s">
	<g:if test="${s}">
		<li>
			<g:if test="${s?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.value?.class)}">
					<g:link
							controller="${s?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${s?.value?.id}">
						${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${s?.value?.class?.simpleName}"
							action="show"
							id="${s?.value?.id}">
						${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.class)}">
					<g:link
							controller="${s?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${s?.id}">
						${s?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${s?.class?.simpleName}"
							action="show"
							id="${s?.id}">
						${s?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productSpecification.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${productSpecificationInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(true)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
