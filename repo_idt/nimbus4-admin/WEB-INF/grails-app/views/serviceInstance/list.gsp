<%@ page import="com.verecloud.nimbus4.service.ServiceInstance" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceInstance.label', default: 'ServiceInstance')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-serviceInstance" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'serviceInstance.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'serviceInstance.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'serviceInstance.status.label', default: 'Status')}"/>
				
				<th><g:message code="serviceInstance.serviceSpecification.label" default="Service Specification"/></th>
				
				<th><g:message code="serviceInstance.parent.label" default="Parent"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'serviceInstance.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'serviceInstance.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="serviceInstance.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${serviceInstanceInstanceList}" status="i" var="serviceInstanceInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${serviceInstanceInstance.id}">${serviceInstanceInstance}</g:link></td>
					
					<td>${fieldValue(bean: serviceInstanceInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: serviceInstanceInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: serviceInstanceInstance, field: "status")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(serviceInstanceInstance?.serviceSpecification)?.class?.simpleName}" action="show" id="${serviceInstanceInstance?.serviceSpecification?.id}">${serviceInstanceInstance?.serviceSpecification?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(serviceInstanceInstance?.parent)?.class?.simpleName}" action="show" id="${serviceInstanceInstance?.parent?.id}">${serviceInstanceInstance?.parent?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${serviceInstanceInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${serviceInstanceInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceInstanceInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceInstanceInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${serviceInstanceInstanceCount}"/>
	</div>
</section>

</body>

</html>
