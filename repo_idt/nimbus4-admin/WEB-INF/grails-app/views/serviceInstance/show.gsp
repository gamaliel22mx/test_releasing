<%@ page import="com.verecloud.nimbus4.service.ServiceInstance" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceInstance.label', default: 'ServiceInstance')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-serviceInstance" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceInstanceInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceInstanceInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceInstanceInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceInstanceInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${serviceInstanceInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.serviceSpecification.label" default="Service Specification" /></td>
			
			<td valign="top" class="value"><g:link controller="${serviceInstanceInstance?.serviceSpecification?.class?.simpleName}" action="show" id="${serviceInstanceInstance?.serviceSpecification?.id}">${serviceInstanceInstance?.serviceSpecification?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.parent.label" default="Parent" /></td>
			
			<td valign="top" class="value"><g:link controller="${serviceInstanceInstance?.parent?.class?.simpleName}" action="show" id="${serviceInstanceInstance?.parent?.id}">${serviceInstanceInstance?.parent?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(serviceInstanceInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(serviceInstanceInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceInstanceInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceInstanceInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceInstance.resourceInstances.label" default="Resource Instances" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${serviceInstanceInstance?.resourceInstances}" var="r">
	<g:if test="${r}">
		<li>
			<g:if test="${r?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.value?.class)}">
					<g:link
							controller="${r?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${r?.value?.id}">
						${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${r?.value?.class?.simpleName}"
							action="show"
							id="${r?.value?.id}">
						${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.class)}">
					<g:link
							controller="${r?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${r?.id}">
						${r?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${r?.class?.simpleName}"
							action="show"
							id="${r?.id}">
						${r?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
