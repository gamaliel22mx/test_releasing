<%@ page import="com.verecloud.nimbus4.service.ServiceInstance" %>


<div class="${hasErrors(bean: serviceInstanceInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="serviceInstance.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.InstanceStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.InstanceStatus.values()*.name()}" required="" value="${serviceInstanceInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceInstanceInstance, field: 'serviceSpecification', 'error')} required">
	<label for="serviceSpecification" class="control-label"><g:message code="serviceInstance.serviceSpecification.label" default="Service Specification"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="serviceSpecification" name="serviceSpecification.id" from="${com.verecloud.nimbus4.service.ServiceSpecification.list()}" optionKey="id" required="" value="${serviceInstanceInstance?.serviceSpecification?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceInstanceInstance, field: 'parent', 'error')} ">
	<label for="parent" class="control-label"><g:message code="serviceInstance.parent.label" default="Parent"/> </label>
	<div>
		
			<g:select class="form-control" id="parent" name="parent.id" from="${com.verecloud.nimbus4.product.ProductInstance.list()}" optionKey="id" value="${serviceInstanceInstance?.parent?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceInstanceInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="serviceInstance.configuration.label" default="Configuration"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(serviceInstanceInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(serviceInstanceInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: serviceInstanceInstance, field: 'resourceInstances', 'error')} ">
	<label for="resourceInstances" class="control-label"><g:message code="serviceInstance.resourceInstances.label" default="Resource Instances"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${serviceInstanceInstance?.resourceInstances}" var="r">
			    <g:if test="${r}">
                    <li>
                        <g:if test="${r?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.value?.class)}">
                                <g:link
                                    controller="${r?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${r?.value?.id}">
                                    ${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${r?.value?.class?.simpleName}"
                                    action="show"
                                    id="${r?.value?.id}">
                                    ${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.class)}">
                                <g:link
                                    controller="${r?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${r.id}">
                                    ${r?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${r?.class?.simpleName}"
                                    action="show"
                                    id="${r.id}">
                                    ${r?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'resourceInstance').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['serviceInstance.id': serviceInstanceInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: serviceInstanceInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="serviceInstance.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>

