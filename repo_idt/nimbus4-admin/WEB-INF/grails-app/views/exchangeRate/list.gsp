<%@ page import="com.verecloud.nimbus4.billing.ExchangeRate" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'exchangeRate.label', default: 'ExchangeRate')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-exchangeRate" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="effectiveTo" title="${message(code: 'exchangeRate.effectiveTo.label', default: 'Effective To')}"/>
				
					<g:sortableColumn property="rate" title="${message(code: 'exchangeRate.rate.label', default: 'Rate')}"/>
				
					<g:sortableColumn property="effectiveFrom" title="${message(code: 'exchangeRate.effectiveFrom.label', default: 'Effective From')}"/>
				
					<g:sortableColumn property="fromCurrency" title="${message(code: 'exchangeRate.fromCurrency.label', default: 'From Currency')}"/>
				
					<g:sortableColumn property="toCurrency" title="${message(code: 'exchangeRate.toCurrency.label', default: 'To Currency')}"/>
				
				<th><g:message code="exchangeRate.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${exchangeRateInstanceList}" status="i" var="exchangeRateInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${exchangeRateInstance.id}">${exchangeRateInstance}</g:link></td>
					
					<td><g:formatDate date="${exchangeRateInstance.effectiveTo}"/></td>
					
					<td>
						
                        <g:formatNumber number="${exchangeRateInstance?.rate}" type="number" maxFractionDigits="10" />
						
					</td>
					
					<td><g:formatDate date="${exchangeRateInstance.effectiveFrom}"/></td>
					
					<td>${fieldValue(bean: exchangeRateInstance, field: "fromCurrency")}</td>
					
					<td>${fieldValue(bean: exchangeRateInstance, field: "toCurrency")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${exchangeRateInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(exchangeRateInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${exchangeRateInstanceCount}"/>
	</div>
</section>

</body>

</html>
