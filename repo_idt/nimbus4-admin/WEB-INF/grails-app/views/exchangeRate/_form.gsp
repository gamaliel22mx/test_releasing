<%@ page import="com.verecloud.nimbus4.billing.ExchangeRate" %>


<div class="${hasErrors(bean: exchangeRateInstance, field: 'effectiveTo', 'error')} ">
	<label for="effectiveTo" class="control-label"><g:message code="exchangeRate.effectiveTo.label" default="Effective To"/> </label>
	<div>
		
			<bs:datePicker name="effectiveTo" precision="day"  value="${exchangeRateInstance?.effectiveTo}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: exchangeRateInstance, field: 'rate', 'error')} required">
	<label for="rate" class="control-label"><g:message code="exchangeRate.rate.label" default="Rate"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="rate" value="${exchangeRateInstance.rate}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: exchangeRateInstance, field: 'effectiveFrom', 'error')} required">
	<label for="effectiveFrom" class="control-label"><g:message code="exchangeRate.effectiveFrom.label" default="Effective From"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="effectiveFrom" precision="day"  value="${exchangeRateInstance?.effectiveFrom}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: exchangeRateInstance, field: 'fromCurrency', 'error')} required">
	<label for="fromCurrency" class="control-label"><g:message code="exchangeRate.fromCurrency.label" default="From Currency"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:currencySelect class='form-control' name="fromCurrency" value="${exchangeRateInstance?.fromCurrency}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: exchangeRateInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="exchangeRate.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<div class="${hasErrors(bean: exchangeRateInstance, field: 'toCurrency', 'error')} required">
	<label for="toCurrency" class="control-label"><g:message code="exchangeRate.toCurrency.label" default="To Currency"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:currencySelect class='form-control' name="toCurrency" value="${exchangeRateInstance?.toCurrency}"  />
		
	</div>
</div>

