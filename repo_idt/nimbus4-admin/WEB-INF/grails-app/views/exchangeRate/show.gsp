<%@ page import="com.verecloud.nimbus4.billing.ExchangeRate" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'exchangeRate.label', default: 'ExchangeRate')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-exchangeRate" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="exchangeRate.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${exchangeRateInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(exchangeRateInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="exchangeRate.effectiveTo.label" default="Effective To" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${exchangeRateInstance?.effectiveTo}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="exchangeRate.rate.label" default="Rate" /></td>
			
			<td valign="top" class="value">
				
				<g:formatNumber number="${exchangeRateInstance?.rate}" type="number" maxFractionDigits="10" />
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="exchangeRate.effectiveFrom.label" default="Effective From" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${exchangeRateInstance?.effectiveFrom}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="exchangeRate.fromCurrency.label" default="From Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: exchangeRateInstance, field: "fromCurrency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="exchangeRate.toCurrency.label" default="To Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: exchangeRateInstance, field: "toCurrency")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
