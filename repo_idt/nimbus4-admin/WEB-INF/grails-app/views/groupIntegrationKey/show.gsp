<%@ page import="com.verecloud.nimbus4.party.GroupIntegrationKey" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupIntegrationKey.label', default: 'GroupIntegrationKey')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-groupIntegrationKey" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationKey.group.label" default="Group" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupIntegrationKeyInstance?.group?.class?.simpleName}" action="show" id="${groupIntegrationKeyInstance?.group?.id}">${groupIntegrationKeyInstance?.group?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationKey.foreignKey.label" default="Foreign Key" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupIntegrationKeyInstance, field: "foreignKey")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationKey.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${groupIntegrationKeyInstance?.type?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
