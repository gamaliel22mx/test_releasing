<%@ page import="com.verecloud.nimbus4.party.GroupIntegrationKey" %>


<div class="${hasErrors(bean: groupIntegrationKeyInstance, field: 'group', 'error')} required">
	<label for="group" class="control-label"><g:message code="groupIntegrationKey.group.label" default="Group"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="group" name="group.id" from="${com.verecloud.nimbus4.party.Group.list()}" optionKey="id" required="" value="${groupIntegrationKeyInstance?.group?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupIntegrationKeyInstance, field: 'foreignKey', 'error')} ">
	<label for="foreignKey" class="control-label"><g:message code="groupIntegrationKey.foreignKey.label" default="Foreign Key"/> </label>
	<div>
		
			<g:textField class="form-control" name="foreignKey" value="${groupIntegrationKeyInstance?.foreignKey}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupIntegrationKeyInstance, field: 'type', 'error')} required">
	<label for="type" class="control-label"><g:message code="groupIntegrationKey.type.label" default="Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="type" from="${com.verecloud.nimbus4.service.enums.GroupIntegrationType?.values()}" keys="${com.verecloud.nimbus4.service.enums.GroupIntegrationType.values()*.name()}" required="" value="${groupIntegrationKeyInstance?.type?.name()}"/>
		
	</div>
</div>

