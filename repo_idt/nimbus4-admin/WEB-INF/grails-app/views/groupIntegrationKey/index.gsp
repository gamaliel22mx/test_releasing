<%@ page import="com.verecloud.nimbus4.party.GroupIntegrationKey" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupIntegrationKey.label', default: 'GroupIntegrationKey')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-groupIntegrationKey" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
				<th><g:message code="groupIntegrationKey.group.label" default="Group"/></th>
				
					<g:sortableColumn property="foreignKey" title="${message(code: 'groupIntegrationKey.foreignKey.label', default: 'Foreign Key')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="type" title="${message(code: 'groupIntegrationKey.type.label', default: 'Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupIntegrationKeyInstanceList}" status="i" var="groupIntegrationKeyInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupIntegrationKeyInstance.id}">${groupIntegrationKeyInstance}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupIntegrationKeyInstance?.group)?.class?.simpleName}" action="show" id="${groupIntegrationKeyInstance?.group?.id}">${groupIntegrationKeyInstance?.group?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: groupIntegrationKeyInstance, field: "foreignKey")}</td>
					
					<td>${fieldValue(bean: groupIntegrationKeyInstance, field: "type")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupIntegrationKeyInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
