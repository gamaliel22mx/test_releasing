<%@ page import="com.verecloud.nimbus4.party.GroupIntegrationSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupIntegrationSpecification.label', default: 'GroupIntegrationSpecification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-groupIntegrationSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'groupIntegrationSpecification.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'groupIntegrationSpecification.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'groupIntegrationSpecification.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'groupIntegrationSpecification.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'groupIntegrationSpecification.lastUpdated.label', default: 'Last Updated')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupIntegrationSpecificationInstanceList}" status="i" var="groupIntegrationSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupIntegrationSpecificationInstance.id}">${groupIntegrationSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupIntegrationSpecificationInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupIntegrationSpecificationInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: groupIntegrationSpecificationInstance, field: "name")}</td>
					
					<td><g:formatDate date="${groupIntegrationSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${groupIntegrationSpecificationInstance.lastUpdated}"/></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupIntegrationSpecificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
