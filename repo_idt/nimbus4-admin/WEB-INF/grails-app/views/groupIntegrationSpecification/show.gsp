<%@ page import="com.verecloud.nimbus4.party.GroupIntegrationSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupIntegrationSpecification.label', default: 'GroupIntegrationSpecification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-groupIntegrationSpecification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupIntegrationSpecificationInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupIntegrationSpecificationInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupIntegrationSpecificationInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.schema.label" default="Schema" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="schema" value="${(groupIntegrationSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(groupIntegrationSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupIntegrationSpecificationInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.eventHandlers.label" default="Event Handlers" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupIntegrationSpecificationInstance?.eventHandlers}" var="e">
	<g:if test="${e}">
		<li>
			<g:if test="${e?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(e?.value?.class)}">
					<g:link
							controller="${e?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${e?.value?.id}">
						${e?.key?.encodeAsHTML()+" : "+e?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${e?.value?.class?.simpleName}"
							action="show"
							id="${e?.value?.id}">
						${e?.key?.encodeAsHTML()+" : "+e?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(e?.class)}">
					<g:link
							controller="${e?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${e?.id}">
						${e?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${e?.class?.simpleName}"
							action="show"
							id="${e?.id}">
						${e?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupIntegrationSpecification.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupIntegrationSpecificationInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(true)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
