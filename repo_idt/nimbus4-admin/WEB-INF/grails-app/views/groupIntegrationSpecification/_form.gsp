<%@ page import="com.verecloud.nimbus4.party.GroupIntegrationSpecification" %>


<div class="${hasErrors(bean: groupIntegrationSpecificationInstance, field: 'name', 'error')} required">
	<label for="name" class="control-label"><g:message code="groupIntegrationSpecification.name.label" default="Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="name" required="" value="${groupIntegrationSpecificationInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupIntegrationSpecificationInstance, field: 'schema', 'error')} ">
	<label for="schema" class="control-label"><g:message code="groupIntegrationSpecification.schema.label" default="Schema"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="schema" value="${(groupIntegrationSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(groupIntegrationSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: groupIntegrationSpecificationInstance, field: 'eventHandlers', 'error')} ">
	<label for="eventHandlers" class="control-label"><g:message code="groupIntegrationSpecification.eventHandlers.label" default="Event Handlers"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${groupIntegrationSpecificationInstance?.eventHandlers}" var="e">
			    <g:if test="${e}">
                    <li>
                        <g:if test="${e?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(e?.value?.class)}">
                                <g:link
                                    controller="${e?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${e?.value?.id}">
                                    ${e?.key?.encodeAsHTML()+" : "+e?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${e?.value?.class?.simpleName}"
                                    action="show"
                                    id="${e?.value?.id}">
                                    ${e?.key?.encodeAsHTML()+" : "+e?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(e?.class)}">
                                <g:link
                                    controller="${e?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${e.id}">
                                    ${e?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${e?.class?.simpleName}"
                                    action="show"
                                    id="${e.id}">
                                    ${e?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'groupEventHandler').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['groupIntegrationSpecification.id': groupIntegrationSpecificationInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(false)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>

