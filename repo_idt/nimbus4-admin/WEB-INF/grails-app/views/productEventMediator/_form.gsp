<%@ page import="com.verecloud.nimbus4.product.ProductEventMediator" %>


<div class="${hasErrors(bean: productEventMediatorInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="productEventMediator.name.label" default="Name"/> </label>
	<div>
		
			<g:textField class="form-control" name="name" value="${productEventMediatorInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productEventMediatorInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="productEventMediator.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${productEventMediatorInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productEventMediatorInstance, field: 'groovyScript', 'error')} ">
	<label for="groovyScript" class="control-label"><g:message code="productEventMediator.groovyScript.label" default="Groovy Script"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="groovyScript" value="${productEventMediatorInstance?.groovyScript}" />
            <div id="groovyScriptResizable"><div id="groovyScriptEditor">${productEventMediatorInstance?.groovyScript}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: productEventMediatorInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="productEventMediator.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #groovyScriptResizable { width: 1000px; height: 1300px; padding: 5px; border: 1px solid #aedeae}
                     #groovyScriptResizable{position: relative}
                     #groovyScriptEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                groovyScriptEditor = ace.edit("groovyScriptEditor");
                                var groovyScript = $('textarea[name="groovyScript"]').hide();
                                groovyScriptEditor.session.setMode("ace/mode/groovy");
                                groovyScriptEditor.setTheme("ace/theme/tomorrow");
                                groovyScriptEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('groovyScriptEditor').style.fontSize='15px'
                                groovyScriptEditor.setReadOnly(false)
                                groovyScriptEditor.getSession().on('change', function(){
                                    groovyScript.val(groovyScriptEditor.getSession().getValue());
                                });
                                 jq("#groovyScriptResizable").resizable({
                                    resize: function( event, ui ) {
                                    groovyScriptEditor.resize();
                                  }
                                 });

            
                    });
                </script>

