<%@ page import="com.verecloud.nimbus4.product.ProductEventMediator" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productEventMediator.label', default: 'ProductEventMediator')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-productEventMediator" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'productEventMediator.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'productEventMediator.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'productEventMediator.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="description" title="${message(code: 'productEventMediator.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'productEventMediator.groovyScript.label', default: 'Groovy Script')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'productEventMediator.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'productEventMediator.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="productEventMediator.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${productEventMediatorInstanceList}" status="i" var="productEventMediatorInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${productEventMediatorInstance.id}">${productEventMediatorInstance}</g:link></td>
					
					<td>${fieldValue(bean: productEventMediatorInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: productEventMediatorInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: productEventMediatorInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: productEventMediatorInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: productEventMediatorInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${productEventMediatorInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${productEventMediatorInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${productEventMediatorInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productEventMediatorInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${productEventMediatorInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
