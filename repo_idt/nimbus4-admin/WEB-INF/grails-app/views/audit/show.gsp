<%@ page import="org.codehaus.groovy.grails.plugins.orm.auditable.AuditLogEvent" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'auditLogEvent.label', default: 'AuditLogEvent')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-auditLogEvent" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.actor.label" default="Actor" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "actor")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.uri.label" default="Uri" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "uri")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.className.label" default="Class Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "className")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.persistedObjectId.label" default="Persisted Object Id" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "persistedObjectId")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.persistedObjectVersion.label" default="Persisted Object Version" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "persistedObjectVersion")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.eventName.label" default="Event Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "eventName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.propertyName.label" default="Property Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "propertyName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.oldValue.label" default="Old Value" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "oldValue")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.newValue.label" default="New Value" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: auditLogEventInstance, field: "newValue")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${auditLogEventInstance?.dateCreated}" /></td>
                
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="auditLogEvent.lastUpdated.label" default="Last Updated" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${auditLogEventInstance?.lastUpdated}" /></td>
                
			</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
