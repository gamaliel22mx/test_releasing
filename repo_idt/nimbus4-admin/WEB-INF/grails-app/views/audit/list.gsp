<%@ page import="org.codehaus.groovy.grails.plugins.orm.auditable.AuditLogEvent" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: "${params.controller}.label", default: "${params.controller?.capitalize()}")}"/>
	<title><g:message code="default.history.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-auditLogEvent" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>

				<g:sortableColumn property="id" title="${message(code: 'auditLogEvent.id.label', default: 'Id')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="actor" title="${message(code: 'auditLogEvent.actor.label', default: 'Actor')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="uri" title="${message(code: 'auditLogEvent.uri.label', default: 'Uri')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="className" title="${message(code: 'auditLogEvent.className.label', default: 'Class Name')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="persistedObjectId" title="${message(code: 'auditLogEvent.persistedObjectId.label', default: 'Persisted Object Id')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="persistedObjectVersion" title="${message(code: 'auditLogEvent.persistedObjectVersion.label', default: 'Persisted Object Version')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="eventName" title="${message(code: 'auditLogEvent.eventName.label', default: 'Event Name')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="propertyName" title="${message(code: 'auditLogEvent.propertyName.label', default: 'Property Name')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="oldValue" title="${message(code: 'auditLogEvent.oldValue.label', default: 'Old Value')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="newValue" title="${message(code: 'auditLogEvent.newValue.label', default: 'New Value')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="dateCreated" title="${message(code: 'auditLogEvent.dateCreated.label', default: 'Date Created')}" params="['clazz':clazz.name]"/>

				<g:sortableColumn property="lastUpdated" title="${message(code: 'auditLogEvent.lastUpdated.label', default: 'Last Updated')}" params="['clazz':clazz.name]"/>

			</tr>
			</thead>
			<tbody>
			<g:each in="${auditLogEventInstanceList}" status="i" var="auditLogEventInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link url="/nimbus4-admin/${params.controller}/history/show/${auditLogEventInstance?.id}">${auditLogEventInstance?.id}</g:link></td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "actor")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "uri")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "className")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "persistedObjectId")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "persistedObjectVersion")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "eventName")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "propertyName")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "oldValue")}</td>

					<td>${fieldValue(bean: auditLogEventInstance, field: "newValue")}</td>

					<td><g:formatDate date="${auditLogEventInstance.dateCreated}"/></td>

					<td><g:formatDate date="${auditLogEventInstance.lastUpdated}"/></td>

				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${auditLogEventInstanceCount}"/>
	</div>
</section>

</body>

</html>
