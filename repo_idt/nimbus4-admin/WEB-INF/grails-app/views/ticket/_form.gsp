<%@ page import="com.verecloud.nimbus4.product.Ticket" %>


<div class="${hasErrors(bean: ticketInstance, field: 'id', 'error')} ">
	<label for="id" class="control-label"><g:message code="ticket.id.label" default="Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="id" value="${ticketInstance?.id}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'accountId', 'error')} ">
	<label for="accountId" class="control-label"><g:message code="ticket.accountId.label" default="Account Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="accountId" value="${ticketInstance?.accountId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'createDate', 'error')} ">
	<label for="createDate" class="control-label"><g:message code="ticket.createDate.label" default="Create Date"/> </label>
	<div>
		
			<bs:datePicker name="createDate" precision="day"  value="${ticketInstance?.createDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="ticket.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${ticketInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'productInstanceId', 'error')} ">
	<label for="productInstanceId" class="control-label"><g:message code="ticket.productInstanceId.label" default="Product Instance Id"/> </label>
	<div>
		
			<g:textArea class="form-control" name="productInstanceId" cols="40" rows="5" value="${ticketInstance?.productInstanceId?.join("\n")}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="ticket.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.product.enums.TicketStatus?.values()}" keys="${com.verecloud.nimbus4.product.enums.TicketStatus.values()*.name()}" required="" value="${ticketInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'subject', 'error')} ">
	<label for="subject" class="control-label"><g:message code="ticket.subject.label" default="Subject"/> </label>
	<div>
		
			<g:textField class="form-control" name="subject" value="${ticketInstance?.subject}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: ticketInstance, field: 'type', 'error')} required">
	<label for="type" class="control-label"><g:message code="ticket.type.label" default="Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="type" from="${com.verecloud.nimbus4.product.enums.TicketType?.values()}" keys="${com.verecloud.nimbus4.product.enums.TicketType.values()*.name()}" required="" value="${ticketInstance?.type?.name()}"/>
		
	</div>
</div>

