<%@ page import="com.verecloud.nimbus4.product.Ticket" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'ticket.label', default: 'Ticket')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-ticket" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.accountId.label" default="Account Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: ticketInstance, field: "accountId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.createDate.label" default="Create Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${ticketInstance?.createDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: ticketInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.productInstanceId.label" default="Product Instance Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: ticketInstance, field: "productInstanceId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${ticketInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.subject.label" default="Subject" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: ticketInstance, field: "subject")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="ticket.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${ticketInstance?.type?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
