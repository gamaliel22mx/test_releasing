<%@ page import="com.verecloud.nimbus4.product.Ticket" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'ticket.label', default: 'Ticket')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-ticket" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="accountId" title="${message(code: 'ticket.accountId.label', default: 'Account Id')}"/>
				
					<g:sortableColumn property="createDate" title="${message(code: 'ticket.createDate.label', default: 'Create Date')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'ticket.description.label', default: 'Description')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'ticket.status.label', default: 'Status')}"/>
				
					<g:sortableColumn property="subject" title="${message(code: 'ticket.subject.label', default: 'Subject')}"/>
				
					<g:sortableColumn property="type" title="${message(code: 'ticket.type.label', default: 'Type')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${ticketInstanceList}" status="i" var="ticketInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${ticketInstance.id}">${ticketInstance}</g:link></td>
					
					<td>${fieldValue(bean: ticketInstance, field: "accountId")}</td>
					
					<td><g:formatDate date="${ticketInstance.createDate}"/></td>
					
					<td>${fieldValue(bean: ticketInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: ticketInstance, field: "status")}</td>
					
					<td>${fieldValue(bean: ticketInstance, field: "subject")}</td>
					
					<td>${fieldValue(bean: ticketInstance, field: "type")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${ticketInstanceCount}"/>
	</div>
</section>

</body>

</html>
