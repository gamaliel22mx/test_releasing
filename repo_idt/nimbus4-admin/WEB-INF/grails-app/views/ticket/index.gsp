<%@ page import="com.verecloud.nimbus4.product.Ticket" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'ticket.label', default: 'Ticket')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-ticket" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="accountId" title="${message(code: 'ticket.accountId.label', default: 'Account Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="createDate" title="${message(code: 'ticket.createDate.label', default: 'Create Date')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="description" title="${message(code: 'ticket.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="status" title="${message(code: 'ticket.status.label', default: 'Status')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="subject" title="${message(code: 'ticket.subject.label', default: 'Subject')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="type" title="${message(code: 'ticket.type.label', default: 'Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${ticketInstanceList}" status="i" var="ticketInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${ticketInstance.id}">${ticketInstance}</g:link></td>
					
					<td>${fieldValue(bean: ticketInstance, field: "accountId")}</td>
					
					<td><g:formatDate date="${ticketInstance.createDate}"/></td>
					
					<td>${fieldValue(bean: ticketInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: ticketInstance, field: "status")}</td>
					
					<td>${fieldValue(bean: ticketInstance, field: "subject")}</td>
					
					<td>${fieldValue(bean: ticketInstance, field: "type")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${ticketInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
