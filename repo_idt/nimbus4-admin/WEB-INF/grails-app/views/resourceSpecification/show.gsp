<%@ page import="com.verecloud.nimbus4.resource.ResourceSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'resourceSpecification.label', default: 'ResourceSpecification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-resourceSpecification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${resourceSpecificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(resourceSpecificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceSpecificationInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceSpecificationInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.bindingDestination.label" default="Binding Destination" /></td>
			
			<td valign="top" class="value"><g:link controller="${resourceSpecificationInstance?.bindingDestination?.class?.simpleName}" action="show" id="${resourceSpecificationInstance?.bindingDestination?.id}">${resourceSpecificationInstance?.bindingDestination?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceSpecificationInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${resourceSpecificationInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceSpecificationInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.externalId.label" default="External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceSpecificationInstance, field: "externalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.parentProductSpecification.label" default="Parent Product Specification" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceSpecificationInstance, field: "parentProductSpecification")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.schema.label" default="Schema" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="schema" value="${(resourceSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(resourceSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${resourceSpecificationInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${resourceSpecificationInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceSpecification.resourceVisualizers.label" default="Resource Visualizers" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${resourceSpecificationInstance?.resourceVisualizers}" var="r">
	<g:if test="${r}">
		<li>
			<g:if test="${r?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.value?.class)}">
					<g:link
							controller="${r?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${r?.value?.id}">
						${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${r?.value?.class?.simpleName}"
							action="show"
							id="${r?.value?.id}">
						${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.class)}">
					<g:link
							controller="${r?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${r?.id}">
						${r?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${r?.class?.simpleName}"
							action="show"
							id="${r?.id}">
						${r?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(true)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
