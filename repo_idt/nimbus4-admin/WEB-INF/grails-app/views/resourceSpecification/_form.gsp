<%@ page import="com.verecloud.nimbus4.resource.ResourceSpecification" %>


<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'bindingDestination', 'error')} ">
	<label for="bindingDestination" class="control-label"><g:message code="resourceSpecification.bindingDestination.label" default="Binding Destination"/> </label>
	<div>

		<g:select class="form-control" id="bindingDestination" name="bindingDestination.id" noSelection="${['': '-----Select Group-----']}" from="${com.verecloud.nimbus4.util.Nimbus4CoreUtil.getParentAndSupplierTenants(nimbus.findTenantId().toLong())}" optionKey="id"  value="${resourceSpecificationInstance?.bindingDestination?.id}" class="many-to-one"/>

	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'name', 'error')} required">
	<label for="name" class="control-label"><g:message code="resourceSpecification.name.label" default="Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="name" required="" value="${resourceSpecificationInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="resourceSpecification.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.SpecificationStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.SpecificationStatus.values()*.name()}" required="" value="${resourceSpecificationInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="resourceSpecification.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${resourceSpecificationInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'externalId', 'error')} ">
	<label for="externalId" class="control-label"><g:message code="resourceSpecification.externalId.label" default="External Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="externalId" value="${resourceSpecificationInstance?.externalId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'parentProductSpecification', 'error')} ">
	<label for="parentProductSpecification" class="control-label"><g:message code="resourceSpecification.parentProductSpecification.label" default="Parent Product Specification"/> </label>
	<div>
		
			<g:select class="form-control" id="parentProductSpecification" name="parentProductSpecification.id" from="${com.verecloud.nimbus4.util.Nimbus4CoreUtil.getParentOrSupplierTenantData('com.verecloud.nimbus4.product.ProductSpecification')}" optionKey="id" value="${resourceSpecificationInstance?.parentProductSpecification?.id}" class="many-to-one" noSelection="['': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'schema', 'error')} required">
	<label for="schema" class="control-label"><g:message code="resourceSpecification.schema.label" default="Schema"/> <span class="required-indicator">*</span></label>
	<div>
		

		
           <g:textArea class="form-control" name="schema" value="${(resourceSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(resourceSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'resourceVisualizers', 'error')} ">
	<label for="resourceVisualizers" class="control-label"><g:message code="resourceSpecification.resourceVisualizers.label" default="Resource Visualizers"/> </label>
	<div>
		
			<g:if test="${resourceSpecificationInstance?.resourceVisualizers?.getClass() && Map.isAssignableFrom(resourceSpecificationInstance?.resourceVisualizers?.getClass())}"><g:select class="form-control" name="resourceVisualizers" from="${com.verecloud.nimbus4.resource.ResourceVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${resourceSpecificationInstance?.resourceVisualizers*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="resourceVisualizers" from="${com.verecloud.nimbus4.resource.ResourceVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${resourceSpecificationInstance?.resourceVisualizers*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceSpecificationInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="resourceSpecification.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(false)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>

