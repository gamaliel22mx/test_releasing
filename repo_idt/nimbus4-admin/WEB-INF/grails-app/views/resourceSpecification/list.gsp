<%@ page import="com.verecloud.nimbus4.resource.ResourceSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'resourceSpecification.label', default: 'ResourceSpecification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-resourceSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'resourceSpecification.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'resourceSpecification.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
				<th><g:message code="resourceSpecification.bindingDestination.label" default="Binding Destination"/></th>
				
					<g:sortableColumn property="name" title="${message(code: 'resourceSpecification.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'resourceSpecification.status.label', default: 'Status')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'resourceSpecification.description.label', default: 'Description')}"/>
				
					<g:sortableColumn property="externalId" title="${message(code: 'resourceSpecification.externalId.label', default: 'External Id')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'resourceSpecification.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'resourceSpecification.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="resourceSpecification.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${resourceSpecificationInstanceList}" status="i" var="resourceSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${resourceSpecificationInstance.id}">${resourceSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: resourceSpecificationInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: resourceSpecificationInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(resourceSpecificationInstance?.bindingDestination)?.class?.simpleName}" action="show" id="${resourceSpecificationInstance?.bindingDestination?.id}">${resourceSpecificationInstance?.bindingDestination?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: resourceSpecificationInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: resourceSpecificationInstance, field: "status")}</td>
					
					<td>${fieldValue(bean: resourceSpecificationInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: resourceSpecificationInstance, field: "externalId")}</td>
					
					<td><g:formatDate date="${resourceSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${resourceSpecificationInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${resourceSpecificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(resourceSpecificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${resourceSpecificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
