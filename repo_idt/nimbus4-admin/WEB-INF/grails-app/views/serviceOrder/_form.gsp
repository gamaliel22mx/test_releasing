<%@ page import="com.verecloud.nimbus4.service.ServiceOrder" %>


<div class="${hasErrors(bean: serviceOrderInstance, field: 'parent', 'error')} ">
	<label for="parent" class="control-label"><g:message code="serviceOrder.parent.label" default="Parent"/> </label>
	<div>
		
			<g:select class="form-control" id="parent" name="parent.id" from="${com.verecloud.nimbus4.product.ProductOrder.list()}" optionKey="id" value="${serviceOrderInstance?.parent?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'serviceInstance', 'error')} ">
	<label for="serviceInstance" class="control-label"><g:message code="serviceOrder.serviceInstance.label" default="Service Instance"/> </label>
	<div>
		
			<g:select class="form-control" id="serviceInstance" name="serviceInstance.id" from="${com.verecloud.nimbus4.service.ServiceInstance.list()}" optionKey="id" value="${serviceOrderInstance?.serviceInstance?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="serviceOrder.configuration.label" default="Configuration"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(serviceOrderInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(serviceOrderInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'serviceSpecificationVersion', 'error')} required">
	<label for="serviceSpecificationVersion" class="control-label"><g:message code="serviceOrder.serviceSpecificationVersion.label" default="Service Specification Version"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="serviceSpecificationVersion" name="serviceSpecificationVersion.id" from="${com.verecloud.nimbus4.service.ServiceSpecification.findAllByTenantId(nimbus.findTenantId().toLong())}" optionKey="id" required="" value="${serviceOrderInstance?.serviceSpecificationVersion?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'childrenCompleted', 'error')} ">
	<label for="childrenCompleted" class="control-label"><g:message code="serviceOrder.childrenCompleted.label" default="Children Completed"/> </label>
	<div>
		
			<nimbus:checkBox name="childrenCompleted"  disabled="disabled" value="${serviceOrderInstance?.childrenCompleted}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'children', 'error')} ">
	<label for="children" class="control-label"><g:message code="serviceOrder.children.label" default="Children"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${serviceOrderInstance?.children}" var="c">
			    <g:if test="${c}">
                    <li>
                        <g:if test="${c?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
                                <g:link
                                    controller="${c?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.value?.class?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
                                <g:link
                                    controller="${c?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.class?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'resourceOrder').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['serviceOrder.id': serviceOrderInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'orderType', 'error')} required">
	<label for="orderType" class="control-label"><g:message code="serviceOrder.orderType.label" default="Order Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="orderType" from="${com.verecloud.nimbus4.service.enums.OrderType?.values()}" keys="${com.verecloud.nimbus4.service.enums.OrderType.values()*.name()}" required="" value="${serviceOrderInstance?.orderType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'requestDate', 'error')} required">
	<label for="requestDate" class="control-label"><g:message code="serviceOrder.requestDate.label" default="Request Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="requestDate" precision="day"  value="${serviceOrderInstance?.requestDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="serviceOrder.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.OrderStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.OrderStatus.values()*.name()}" required="" value="${serviceOrderInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceOrderInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="serviceOrder.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>

