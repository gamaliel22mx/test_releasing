<%@ page import="com.verecloud.nimbus4.service.ServiceOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceOrder.label', default: 'ServiceOrder')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-serviceOrder" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceOrderInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceOrderInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceOrderInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceOrderInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.parent.label" default="Parent" /></td>
			
			<td valign="top" class="value"><g:link controller="${serviceOrderInstance?.parent?.class?.simpleName}" action="show" id="${serviceOrderInstance?.parent?.id}">${serviceOrderInstance?.parent?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.serviceInstance.label" default="Service Instance" /></td>
			
			<td valign="top" class="value"><g:link controller="${serviceOrderInstance?.serviceInstance?.class?.simpleName}" action="show" id="${serviceOrderInstance?.serviceInstance?.id}">${serviceOrderInstance?.serviceInstance?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(serviceOrderInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(serviceOrderInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.serviceSpecificationVersion.label" default="Service Specification Version" /></td>
			
			<td valign="top" class="value"><g:link controller="${serviceOrderInstance?.serviceSpecificationVersion?.class?.simpleName}" action="show" id="${serviceOrderInstance?.serviceSpecificationVersion?.id}">${serviceOrderInstance?.serviceSpecificationVersion?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.childrenCompleted.label" default="Children Completed" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${serviceOrderInstance?.childrenCompleted}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.children.label" default="Children" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${serviceOrderInstance?.children}" var="c">
	<g:if test="${c}">
		<li>
			<g:if test="${c?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
					<g:link
							controller="${c?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.value?.class?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
					<g:link
							controller="${c?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.class?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceOrderInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceOrderInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.orderType.label" default="Order Type" /></td>
			
			<td valign="top" class="value">${serviceOrderInstance?.orderType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.requestDate.label" default="Request Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceOrderInstance?.requestDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceOrder.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${serviceOrderInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
