<%@ page import="com.verecloud.nimbus4.service.ServiceOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceOrder.label', default: 'ServiceOrder')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-serviceOrder" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'serviceOrder.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'serviceOrder.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
				<th><g:message code="serviceOrder.parent.label" default="Parent"/></th>
				
				<th><g:message code="serviceOrder.serviceInstance.label" default="Service Instance"/></th>
				
				<th><g:message code="serviceOrder.serviceSpecificationVersion.label" default="Service Specification Version"/></th>
				
					<g:sortableColumn property="childrenCompleted" title="${message(code: 'serviceOrder.childrenCompleted.label', default: 'Children Completed')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'serviceOrder.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'serviceOrder.lastUpdated.label', default: 'Last Updated')}"/>
				
					<g:sortableColumn property="orderType" title="${message(code: 'serviceOrder.orderType.label', default: 'Order Type')}"/>
				
					<g:sortableColumn property="requestDate" title="${message(code: 'serviceOrder.requestDate.label', default: 'Request Date')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'serviceOrder.status.label', default: 'Status')}"/>
				
				<th><g:message code="serviceOrder.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${serviceOrderInstanceList}" status="i" var="serviceOrderInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${serviceOrderInstance.id}">${serviceOrderInstance}</g:link></td>
					
					<td>${fieldValue(bean: serviceOrderInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: serviceOrderInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(serviceOrderInstance?.parent)?.class?.simpleName}" action="show" id="${serviceOrderInstance?.parent?.id}">${serviceOrderInstance?.parent?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(serviceOrderInstance?.serviceInstance)?.class?.simpleName}" action="show" id="${serviceOrderInstance?.serviceInstance?.id}">${serviceOrderInstance?.serviceInstance?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(serviceOrderInstance?.serviceSpecificationVersion)?.class?.simpleName}" action="show" id="${serviceOrderInstance?.serviceSpecificationVersion?.id}">${serviceOrderInstance?.serviceSpecificationVersion?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatBoolean boolean="${serviceOrderInstance.childrenCompleted}"/></td>
							
					<td><g:formatDate date="${serviceOrderInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${serviceOrderInstance.lastUpdated}"/></td>
					
					<td>${fieldValue(bean: serviceOrderInstance, field: "orderType")}</td>
					
					<td><g:formatDate date="${serviceOrderInstance.requestDate}"/></td>
					
					<td>${fieldValue(bean: serviceOrderInstance, field: "status")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceOrderInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceOrderInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${serviceOrderInstanceCount}"/>
	</div>
</section>

</body>

</html>
