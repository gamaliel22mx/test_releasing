<%@ page import="com.verecloud.nimbus4.OrderAggregationChild" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'orderAggregationChild.label', default: 'OrderAggregationChild')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-orderAggregationChild" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="childId" title="${message(code: 'orderAggregationChild.childId.label', default: 'Child Id')}"/>
				
				<th><g:message code="orderAggregationChild.parent.label" default="Parent"/></th>
				
					<g:sortableColumn property="type" title="${message(code: 'orderAggregationChild.type.label', default: 'Type')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${orderAggregationChildInstanceList}" status="i" var="orderAggregationChildInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${orderAggregationChildInstance.id}">${orderAggregationChildInstance}</g:link></td>
					
					<td>${fieldValue(bean: orderAggregationChildInstance, field: "childId")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(orderAggregationChildInstance?.parent)?.class?.simpleName}" action="show" id="${orderAggregationChildInstance?.parent?.id}">${orderAggregationChildInstance?.parent?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: orderAggregationChildInstance, field: "type")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${orderAggregationChildInstanceCount}"/>
	</div>
</section>

</body>

</html>
