<%@ page import="com.verecloud.nimbus4.OrderAggregationChild" %>


<div class="${hasErrors(bean: orderAggregationChildInstance, field: 'childId', 'error')} required">
	<label for="childId" class="control-label"><g:message code="orderAggregationChild.childId.label" default="Child Id"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="childId" type="number" value="${orderAggregationChildInstance.childId}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: orderAggregationChildInstance, field: 'parent', 'error')} required">
	<label for="parent" class="control-label"><g:message code="orderAggregationChild.parent.label" default="Parent"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="parent" name="parent.id" from="${com.verecloud.nimbus4.OrderAggregation.list()}" optionKey="id" required="" value="${orderAggregationChildInstance?.parent?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: orderAggregationChildInstance, field: 'type', 'error')} ">
	<label for="type" class="control-label"><g:message code="orderAggregationChild.type.label" default="Type"/> </label>
	<div>
		
			<g:textField class="form-control" name="type" value="${orderAggregationChildInstance?.type}"/>
		
	</div>
</div>

