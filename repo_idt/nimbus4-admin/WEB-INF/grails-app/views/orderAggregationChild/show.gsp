<%@ page import="com.verecloud.nimbus4.OrderAggregationChild" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'orderAggregationChild.label', default: 'OrderAggregationChild')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-orderAggregationChild" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="orderAggregationChild.childId.label" default="Child Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: orderAggregationChildInstance, field: "childId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="orderAggregationChild.parent.label" default="Parent" /></td>
			
			<td valign="top" class="value"><g:link controller="${orderAggregationChildInstance?.parent?.class?.simpleName}" action="show" id="${orderAggregationChildInstance?.parent?.id}">${orderAggregationChildInstance?.parent?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="orderAggregationChild.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: orderAggregationChildInstance, field: "type")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
