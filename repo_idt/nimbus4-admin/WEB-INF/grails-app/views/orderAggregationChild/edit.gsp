<%@ page import="com.verecloud.nimbus4.OrderAggregationChild" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'orderAggregationChild.label', default: 'OrderAggregationChild')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

	<section id="edit-orderAggregationChild" class="first">

		<g:hasErrors bean="${orderAggregationChildInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${orderAggregationChildInstance}" as="list" />
		</div>
		</g:hasErrors>

		<g:form method="post" class="form-horizontal" role="form" >
			<g:hiddenField name="id" value="${orderAggregationChildInstance?.id}" />
			<g:hiddenField name="version" value="${orderAggregationChildInstance?.version}" />
			<g:hiddenField name="_method" value="PUT" />
		    

			<g:render template="form"/>

			<div class="form-actions margin-top-medium">
				<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
	            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
			</div>
		</g:form>

	</section>

</body>

</html>
