<%@ page import="com.verecloud.nimbus4.common.IntegrationXref" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'integrationXref.label', default: 'IntegrationXref')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-integrationXref" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${integrationXrefInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(integrationXrefInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: integrationXrefInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: integrationXrefInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.internalAssociatedClass.label" default="Internal Associated Class" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: integrationXrefInstance, field: "internalAssociatedClass")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.internalId.label" default="Internal Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: integrationXrefInstance, field: "internalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.associatedGroup.label" default="Associated Group" /></td>
			
			<td valign="top" class="value"><g:link controller="${integrationXrefInstance?.associatedGroup?.class?.simpleName}" action="show" id="${integrationXrefInstance?.associatedGroup?.id}">${integrationXrefInstance?.associatedGroup?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.lastUpdatedDate.label" default="Last Updated Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${integrationXrefInstance?.lastUpdatedDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.createdDate.label" default="Created Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${integrationXrefInstance?.createdDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${integrationXrefInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.externalId.label" default="External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: integrationXrefInstance, field: "externalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.externalObjectType.label" default="External Object Type" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: integrationXrefInstance, field: "externalObjectType")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="integrationXref.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${integrationXrefInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
