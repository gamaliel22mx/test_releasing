<%@ page import="com.verecloud.nimbus4.common.IntegrationXref" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'integrationXref.label', default: 'IntegrationXref')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-integrationXref" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'integrationXref.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'integrationXref.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="internalAssociatedClass" title="${message(code: 'integrationXref.internalAssociatedClass.label', default: 'Internal Associated Class')}"/>
				
					<g:sortableColumn property="internalId" title="${message(code: 'integrationXref.internalId.label', default: 'Internal Id')}"/>
				
				<th><g:message code="integrationXref.associatedGroup.label" default="Associated Group"/></th>
				
					<g:sortableColumn property="lastUpdatedDate" title="${message(code: 'integrationXref.lastUpdatedDate.label', default: 'Last Updated Date')}"/>
				
					<g:sortableColumn property="createdDate" title="${message(code: 'integrationXref.createdDate.label', default: 'Created Date')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'integrationXref.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="externalId" title="${message(code: 'integrationXref.externalId.label', default: 'External Id')}"/>
				
					<g:sortableColumn property="externalObjectType" title="${message(code: 'integrationXref.externalObjectType.label', default: 'External Object Type')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'integrationXref.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="integrationXref.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${integrationXrefInstanceList}" status="i" var="integrationXrefInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${integrationXrefInstance.id}">${integrationXrefInstance}</g:link></td>
					
					<td>${fieldValue(bean: integrationXrefInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: integrationXrefInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: integrationXrefInstance, field: "internalAssociatedClass")}</td>
					
					<td>${fieldValue(bean: integrationXrefInstance, field: "internalId")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(integrationXrefInstance?.associatedGroup)?.class?.simpleName}" action="show" id="${integrationXrefInstance?.associatedGroup?.id}">${integrationXrefInstance?.associatedGroup?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${integrationXrefInstance.lastUpdatedDate}"/></td>
					
					<td><g:formatDate date="${integrationXrefInstance.createdDate}"/></td>
					
					<td><g:formatDate date="${integrationXrefInstance.dateCreated}"/></td>
					
					<td>${fieldValue(bean: integrationXrefInstance, field: "externalId")}</td>
					
					<td>${fieldValue(bean: integrationXrefInstance, field: "externalObjectType")}</td>
					
					<td><g:formatDate date="${integrationXrefInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${integrationXrefInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(integrationXrefInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${integrationXrefInstanceCount}"/>
	</div>
</section>

</body>

</html>
