<%@ page import="com.verecloud.nimbus4.common.IntegrationXref" %>


<div class="${hasErrors(bean: integrationXrefInstance, field: 'internalAssociatedClass', 'error')} ">
	<label for="internalAssociatedClass" class="control-label"><g:message code="integrationXref.internalAssociatedClass.label" default="Internal Associated Class"/> </label>
	<div>
		
			<g:textField class="form-control" name="internalAssociatedClass" value="${integrationXrefInstance?.internalAssociatedClass}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'internalId', 'error')} ">
	<label for="internalId" class="control-label"><g:message code="integrationXref.internalId.label" default="Internal Id"/> </label>
	<div>
		
			<g:field class="form-control" name="internalId" type="number" value="${integrationXrefInstance.internalId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'associatedGroup', 'error')} ">
	<label for="associatedGroup" class="control-label"><g:message code="integrationXref.associatedGroup.label" default="Associated Group"/> </label>
	<div>
		
			<g:select class="form-control" id="associatedGroup" name="associatedGroup.id" from="${com.verecloud.nimbus4.party.Group.list()}" optionKey="id" value="${integrationXrefInstance?.associatedGroup?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'lastUpdatedDate', 'error')} ">
	<label for="lastUpdatedDate" class="control-label"><g:message code="integrationXref.lastUpdatedDate.label" default="Last Updated Date"/> </label>
	<div>
		
			<bs:datePicker name="lastUpdatedDate" precision="day"  value="${integrationXrefInstance?.lastUpdatedDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'createdDate', 'error')} required">
	<label for="createdDate" class="control-label"><g:message code="integrationXref.createdDate.label" default="Created Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="createdDate" precision="day"  value="${integrationXrefInstance?.createdDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'externalId', 'error')} ">
	<label for="externalId" class="control-label"><g:message code="integrationXref.externalId.label" default="External Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="externalId" value="${integrationXrefInstance?.externalId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'externalObjectType', 'error')} ">
	<label for="externalObjectType" class="control-label"><g:message code="integrationXref.externalObjectType.label" default="External Object Type"/> </label>
	<div>
		
			<g:textField class="form-control" name="externalObjectType" value="${integrationXrefInstance?.externalObjectType}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: integrationXrefInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="integrationXref.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

