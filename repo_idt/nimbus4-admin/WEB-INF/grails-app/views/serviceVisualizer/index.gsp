<%@ page import="com.verecloud.nimbus4.service.ServiceVisualizer" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceVisualizer.label', default: 'ServiceVisualizer')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-serviceVisualizer" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'serviceVisualizer.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'serviceVisualizer.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'serviceVisualizer.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="description" title="${message(code: 'serviceVisualizer.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'serviceVisualizer.groovyScript.label', default: 'Groovy Script')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'serviceVisualizer.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'serviceVisualizer.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="serviceVisualizer.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${serviceVisualizerInstanceList}" status="i" var="serviceVisualizerInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${serviceVisualizerInstance.id}">${serviceVisualizerInstance}</g:link></td>
					
					<td>${fieldValue(bean: serviceVisualizerInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: serviceVisualizerInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: serviceVisualizerInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: serviceVisualizerInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: serviceVisualizerInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${serviceVisualizerInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${serviceVisualizerInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceVisualizerInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceVisualizerInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${serviceVisualizerInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
