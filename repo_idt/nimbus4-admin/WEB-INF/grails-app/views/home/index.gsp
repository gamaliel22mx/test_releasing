<%@ page import="com.verecloud.nimbus4.util.Nimbus4Constants; com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants" %>
<html>

<head>
	<title><g:message code="default.welcome.title" args="[meta(name: 'app.name')]"/></title>
	<meta name="layout" content="kickstart"/>
</head>

<body>

<div class="container">
	<div class="row">
		<g:set var="sectionMap" value="[:]"/>
		<g:render template="/_menu/linkSection"/>
		<g:set var="index" value="${0}"/>
		<g:each var="cMap" in="${sectionMap.sort()}">
			<g:if test="${nimbus.hasAnyPermitted([label: cMap.key, domains: cMap.value]).toString().equals('true')}">
				<div class="col-md-3">
					<div class="center">
						<h3>${cMap.key}</h3>
					</div>
					<ul class="list-group">
						<g:each var="c"
						        in="${request.getAttribute(Nimbus4Constants.HAS_ANY_PERMITTED)?.get(cMap?.key)}">
							<li class="list-group-item">
								<g:link controller="${c.logicalPropertyName}">
									<g:if test="${c.fullName.contains('HomeController')}">
										<i class="glyphicon glyphicon-home"></i>
									</g:if>
									<g:elseif test="${c.fullName.contains('DemoPageController')}">
										<i class="glyphicon glyphicon-list-alt"></i>
									</g:elseif>
									<g:elseif test="${c.fullName.contains('DbdocController')}">
										<i class="glyphicon glyphicon-cloud"></i>
									</g:elseif>
									${c.name}
								</g:link>
							</li>
						</g:each>
					</ul>
				</div>
				<g:set var="index" value="${index + 1}"/>
				<g:if test="${index % 4 == 0}">
					<div class="clearfix visible-md-block"></div>
				</g:if>
			</g:if>
		</g:each>
	</div>
</div>
%{--<g:link class="btn btn-large btn-primary" controller="nextSteps">Next Steps</g:link>--}%

</body>

</html>