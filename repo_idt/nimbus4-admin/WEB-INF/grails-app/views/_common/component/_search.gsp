<%@ page import="com.verecloud.nimbus4.util.Nimbus4Constants" %>
<g:form class="form-horizontal" role="form" name="searchForm" action="search" method="post">
	<div class="row">
		<div class="form-group">
			<div class="col-md-6">
				<div class="input-group">
					<span class="input-group-addon" style="width: auto"><span class="glyphicon glyphicon-list-alt"></span></span>
					<g:select id="searchField" name="searchField" class="form-control" noSelection="${['': '-----Select Field-----']}" from=""/>
				</div>
			</div>

			<div class="col-md-6">
				<div class="input-group">
					<span class="input-group-addon" style="width: auto"><span class="glyphicon glyphicon-search"></span></span>
					<g:textField name="query" type="text" class="form-control" placeholder="Enter Search Query" required="required" value="${searchCO?.query}"/>
				</div>
			</div>
		</div>

		<div>
			<li class="dropdown controllerListMenu">
				<span class="dropdown-toggle text-primary" style="cursor: pointer">Advanced Search Options<b class="caret"></b></span>
				<ul class="dropdown-menu">
					<li class="controller" style="margin: 10px">
						<div class="form-group">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">Offset</span>
									<g:field class="form-control" name="offset" type="number" min="0" value="${searchCO?.offset}"/>
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">Max</span>
									<g:field class="form-control" name="max" type="number" min="1" max="100" value="${searchCO?.max}"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">Sort Field</span>
									<g:select id="sort" name="sort" class="form-control" noSelection="${['id': 'id']}" from=""/>
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">Order
										<span id="orderBySpan" class="glyphicon ${searchCO?.order?.equalsIgnoreCase('DESC') ? 'glyphicon-sort-by-order' : 'glyphicon-sort-by-order-alt'}"></span>
									</span>
									<g:select id="order" name="order" class="form-control" value="${searchCO?.order?.toUpperCase()}" from="['ASC', 'DESC']" onchange="toggleOrderClass()"/>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="input-group pull-right">
								<g:submitButton class="btn btn-success" name="Submit" value="Search"/>
							</div>
						</div>
					</li>
				</ul>
			</li>
		</div>
	</div>
</g:form>

<script type="text/javascript">
	function toggleOrderClass() {
		$('#orderBySpan').each(function (index, elm) {
			if ($(elm).hasClass('glyphicon-sort-by-order')) {
				$(elm).addClass('glyphicon-sort-by-order-alt');
				$(elm).removeClass('glyphicon-sort-by-order');
			} else {
				$(elm).addClass('glyphicon-sort-by-order');
				$(elm).removeClass('glyphicon-sort-by-order-alt');
			}
		});
	}

	function getParameterByName(uri, name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				results = regex.exec(uri);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	function toCamelCase(str) {
		return str
				.replace(/\s(.)/g, function ($1) {
					return $1.toUpperCase();
				})
				.replace(/\s/g, '')
				.replace(/^(.)/, function ($1) {
					return $1.toLowerCase();
				});
	}

	$(function () {
		var searchField = "${searchCO?.searchField}";
		var $el = $("#searchField");
		$('section.first th.sortable a').each(function (index, item) {
			var key = $(item).text();
			var value = getParameterByName($(item).attr('href'), 'sort');
			if (value === searchField) {
				$el.append($("<option selected></option>").attr("value", value).text(key));
			} else {
				$el.append($("<option></option>").attr("value", value).text(key));
			}
		});
		$('section.first th:not(.sortable)').slice(1).each(function (index, item) {
			var key = $(item).text();
			var value = toCamelCase($(item).text());
			if (value === searchField) {
				$el.append($("<option selected></option>").attr("value", value).text(key));
			} else {
				$el.append($("<option></option>").attr("value", value).text(key));
			}
		});

		var sortField = "${searchCO?.sort}";
		var $elm = $("#sort");
		$('th.sortable a').each(function (index, item) {
			var key = $(item).text();
			var value = getParameterByName($(item).attr('href'), 'sort');
			if (value === sortField) {
				$elm.append($("<option selected></option>").attr("value", value).text(key));
			} else {
				$elm.append($("<option></option>").attr("value", value).text(key));
			}
		});
	});
</script>