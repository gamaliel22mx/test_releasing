<div class="modal fade" id="SwitchTenantModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <g:form controller="auth" action="updateTenant" class="form-horizontal" method="post"
                    name="tenant_update_form"
                    onsubmit="return validateTenantValue();">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>

                    <h3><g:message code="default.tenant.label"/></h3>
                </div>

                <div class="modal-body">
                    <div id="errorDiv"></div>
                    <g:hiddenField name="targetUri" id="targetUri" value=""/>
                    <fieldset>
                        <input
                                type="text"
                                style="margin: 0 auto; width: 100%"
                                data-provide="typeahead"
                                data-items="8"
                                name="tenant"
                                id="tenant"
                                value="<nimbus:findTenant/>"
                                autocomplete="off"
                                data-source='<nimbus:fetchActAsTenant/>' onchange="fetchActAsRole($(this).val())"/>

                        <span id="actAsRole"></span>
                        <span id="loader"></span>
                    </fieldset>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="tenant_update_submit" class="btn btn-primary"><g:message
                            code="default.tenant.update"/></button>
                </div>
            </g:form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function fetchActAsRole(tenant) {
        jQuery("span#loader").addClass('loading');
        jQuery.ajax({
            url: "${createLink(controller: 'auth', action: 'fetchActAsRole')}" + "?tenant=" + tenant,
            success: function (data) {
                jQuery('#actAsRole').html(data);
                jQuery("span#loader").removeClass('loading')
            }
        });
        $("#actAsRole").html('')
    }
</script>