<%@ page import="com.verecloud.nimbus4.product.BillingAdapterOrder" %>


<div class="${hasErrors(bean: billingAdapterOrderInstance, field: 'action', 'error')} required">
	<label for="action" class="control-label"><g:message code="billingAdapterOrder.action.label" default="Action"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="action" from="${com.verecloud.nimbus4.product.enums.BillingAdapterAction?.values()}" keys="${com.verecloud.nimbus4.product.enums.BillingAdapterAction.values()*.name()}" required="" value="${billingAdapterOrderInstance?.action?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingAdapterOrderInstance, field: 'periodToProcess', 'error')} ">
	<label for="periodToProcess" class="control-label"><g:message code="billingAdapterOrder.periodToProcess.label" default="Period To Process"/> </label>
	<div>
		
			<g:textField class="form-control" name="periodToProcess" value="${billingAdapterOrderInstance?.periodToProcess}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingAdapterOrderInstance, field: 'processed', 'error')} ">
	<label for="processed" class="control-label"><g:message code="billingAdapterOrder.processed.label" default="Processed"/> </label>
	<div>
		
			<nimbus:checkBox name="processed" value="${billingAdapterOrderInstance?.processed}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingAdapterOrderInstance, field: 'successful', 'error')} ">
	<label for="successful" class="control-label"><g:message code="billingAdapterOrder.successful.label" default="Successful"/> </label>
	<div>
		
			<nimbus:checkBox name="successful" value="${billingAdapterOrderInstance?.successful}"/>
		
	</div>
</div>

