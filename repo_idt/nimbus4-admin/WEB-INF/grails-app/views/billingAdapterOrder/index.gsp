<%@ page import="com.verecloud.nimbus4.product.BillingAdapterOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'billingAdapterOrder.label', default: 'BillingAdapterOrder')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-billingAdapterOrder" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="action" title="${message(code: 'billingAdapterOrder.action.label', default: 'Action')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'billingAdapterOrder.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="periodToProcess" title="${message(code: 'billingAdapterOrder.periodToProcess.label', default: 'Period To Process')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="processed" title="${message(code: 'billingAdapterOrder.processed.label', default: 'Processed')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="successful" title="${message(code: 'billingAdapterOrder.successful.label', default: 'Successful')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${billingAdapterOrderInstanceList}" status="i" var="billingAdapterOrderInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${billingAdapterOrderInstance.id}">${billingAdapterOrderInstance}</g:link></td>
					
					<td>${fieldValue(bean: billingAdapterOrderInstance, field: "action")}</td>
					
					<td><g:formatDate date="${billingAdapterOrderInstance.dateCreated}"/></td>
					
					<td>${fieldValue(bean: billingAdapterOrderInstance, field: "periodToProcess")}</td>
					
					<td><g:formatBoolean boolean="${billingAdapterOrderInstance.processed}"/></td>
							
					<td><g:formatBoolean boolean="${billingAdapterOrderInstance.successful}"/></td>
							
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${billingAdapterOrderInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
