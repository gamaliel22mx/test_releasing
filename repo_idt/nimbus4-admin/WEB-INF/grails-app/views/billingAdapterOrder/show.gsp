<%@ page import="com.verecloud.nimbus4.product.BillingAdapterOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'billingAdapterOrder.label', default: 'BillingAdapterOrder')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-billingAdapterOrder" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingAdapterOrder.action.label" default="Action" /></td>
			
			<td valign="top" class="value">${billingAdapterOrderInstance?.action?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingAdapterOrder.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${billingAdapterOrderInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingAdapterOrder.periodToProcess.label" default="Period To Process" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: billingAdapterOrderInstance, field: "periodToProcess")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingAdapterOrder.processed.label" default="Processed" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${billingAdapterOrderInstance?.processed}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingAdapterOrder.successful.label" default="Successful" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${billingAdapterOrderInstance?.successful}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
