<%@ page import="com.verecloud.nimbus4.party.GroupSpecification" %>


<div class="${hasErrors(bean: groupSpecificationInstance, field: 'name', 'error')} required">
	<label for="name" class="control-label"><g:message code="groupSpecification.name.label" default="Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="name" required="" value="${groupSpecificationInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupSpecificationInstance, field: 'availableIntegrations', 'error')} ">
	<label for="availableIntegrations" class="control-label"><g:message code="groupSpecification.availableIntegrations.label" default="Available Integrations"/> </label>
	<div>
		
			<g:if test="${groupSpecificationInstance?.availableIntegrations?.getClass() && Map.isAssignableFrom(groupSpecificationInstance?.availableIntegrations?.getClass())}"><g:select class="form-control" name="availableIntegrations" from="${com.verecloud.nimbus4.party.GroupIntegrationSpecification.list()}" multiple="multiple" optionKey="id" size="5" value="${groupSpecificationInstance?.availableIntegrations*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="availableIntegrations" from="${com.verecloud.nimbus4.party.GroupIntegrationSpecification.list()}" multiple="multiple" optionKey="id" size="5" value="${groupSpecificationInstance?.availableIntegrations*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: groupSpecificationInstance, field: 'groupVisualizers', 'error')} ">
	<label for="groupVisualizers" class="control-label"><g:message code="groupSpecification.groupVisualizers.label" default="Group Visualizers"/> </label>
	<div>
		
			<g:if test="${groupSpecificationInstance?.groupVisualizers?.getClass() && Map.isAssignableFrom(groupSpecificationInstance?.groupVisualizers?.getClass())}"><g:select class="form-control" name="groupVisualizers" from="${com.verecloud.nimbus4.party.GroupVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${groupSpecificationInstance?.groupVisualizers*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="groupVisualizers" from="${com.verecloud.nimbus4.party.GroupVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${groupSpecificationInstance?.groupVisualizers*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

