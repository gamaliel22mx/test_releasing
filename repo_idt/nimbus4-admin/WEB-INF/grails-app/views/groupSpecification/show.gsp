<%@ page import="com.verecloud.nimbus4.party.GroupSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupSpecification.label', default: 'GroupSpecification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-groupSpecification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupSpecificationInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupSpecificationInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupSpecificationInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.availableIntegrations.label" default="Available Integrations" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupSpecificationInstance?.availableIntegrations}" var="a">
	<g:if test="${a}">
		<li>
			<g:if test="${a?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(a?.value?.class)}">
					<g:link
							controller="${a?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${a?.value?.id}">
						${a?.key?.encodeAsHTML()+" : "+a?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${a?.value?.class?.simpleName}"
							action="show"
							id="${a?.value?.id}">
						${a?.key?.encodeAsHTML()+" : "+a?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(a?.class)}">
					<g:link
							controller="${a?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${a?.id}">
						${a?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${a?.class?.simpleName}"
							action="show"
							id="${a?.id}">
						${a?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupSpecificationInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.groupVisualizers.label" default="Group Visualizers" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupSpecificationInstance?.groupVisualizers}" var="g">
	<g:if test="${g}">
		<li>
			<g:if test="${g?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(g?.value?.class)}">
					<g:link
							controller="${g?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${g?.value?.id}">
						${g?.key?.encodeAsHTML()+" : "+g?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${g?.value?.class?.simpleName}"
							action="show"
							id="${g?.value?.id}">
						${g?.key?.encodeAsHTML()+" : "+g?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(g?.class)}">
					<g:link
							controller="${g?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${g?.id}">
						${g?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${g?.class?.simpleName}"
							action="show"
							id="${g?.id}">
						${g?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupSpecification.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupSpecificationInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
