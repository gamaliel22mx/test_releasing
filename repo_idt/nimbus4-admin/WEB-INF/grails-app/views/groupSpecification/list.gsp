<%@ page import="com.verecloud.nimbus4.party.GroupSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupSpecification.label', default: 'GroupSpecification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-groupSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'groupSpecification.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'groupSpecification.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'groupSpecification.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'groupSpecification.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'groupSpecification.lastUpdated.label', default: 'Last Updated')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupSpecificationInstanceList}" status="i" var="groupSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupSpecificationInstance.id}">${groupSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupSpecificationInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupSpecificationInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: groupSpecificationInstance, field: "name")}</td>
					
					<td><g:formatDate date="${groupSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${groupSpecificationInstance.lastUpdated}"/></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupSpecificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
