<%@ page import="com.verecloud.nimbus4.product.Category" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-category" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'category.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'category.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'category.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'category.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'category.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="category.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${categoryInstance.id}">${categoryInstance}</g:link></td>
					
					<td>${fieldValue(bean: categoryInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: categoryInstance, field: "lastUpdatedBy")}</td>
					
					<td><g:formatDate date="${categoryInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${categoryInstance.lastUpdated}"/></td>
					
					<td>${fieldValue(bean: categoryInstance, field: "name")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${categoryInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(categoryInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${categoryInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
