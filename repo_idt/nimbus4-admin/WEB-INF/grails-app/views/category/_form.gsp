<%@ page import="com.verecloud.nimbus4.product.Category" %>


<div class="${hasErrors(bean: categoryInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="category.name.label" default="Name"/> </label>
	<div>
		
			<g:textField class="form-control" name="name" value="${categoryInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: categoryInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="category.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

