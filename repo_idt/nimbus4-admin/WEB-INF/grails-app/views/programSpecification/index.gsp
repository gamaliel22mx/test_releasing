<%@ page import="com.verecloud.nimbus4.program.ProgramSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'programSpecification.label', default: 'ProgramSpecification')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-programSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="notes" title="${message(code: 'programSpecification.notes.label', default: 'Notes')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="description" title="${message(code: 'programSpecification.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="logo" title="${message(code: 'programSpecification.logo.label', default: 'Logo')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="partner" title="${message(code: 'programSpecification.partner.label', default: 'Partner')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="programSpecification.activeFor.label" default="Active For"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'programSpecification.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'programSpecification.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'programSpecification.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${programSpecificationInstanceList}" status="i" var="programSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${programSpecificationInstance.id}">${programSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: programSpecificationInstance, field: "notes")}</td>
					
					<td>${fieldValue(bean: programSpecificationInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: programSpecificationInstance, field: "logo")}</td>
					
					<td>${fieldValue(bean: programSpecificationInstance, field: "partner")}</td>
					
					<td>${fieldValue(bean: programSpecificationInstance, field: "activeFor")}</td>
					
					<td><g:formatDate date="${programSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${programSpecificationInstance.lastUpdated}"/></td>
					
					<td>${fieldValue(bean: programSpecificationInstance, field: "name")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${programSpecificationInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
