<%@ page import="com.verecloud.nimbus4.program.ProgramSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'programSpecification.label', default: 'ProgramSpecification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-programSpecification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.schema.label" default="Schema" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="schema" value="${(programSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(programSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.notes.label" default="Notes" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: programSpecificationInstance, field: "notes")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: programSpecificationInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.logo.label" default="Logo" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: programSpecificationInstance, field: "logo")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.partner.label" default="Partner" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: programSpecificationInstance, field: "partner")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.activeFor.label" default="Active For" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: programSpecificationInstance, field: "activeFor")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${programSpecificationInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${programSpecificationInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="programSpecification.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: programSpecificationInstance, field: "name")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(true)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
