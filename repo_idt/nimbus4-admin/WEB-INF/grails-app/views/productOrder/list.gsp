<%@ page import="com.verecloud.nimbus4.product.ProductOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productOrder.label', default: 'ProductOrder')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-productOrder" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'productOrder.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'productOrder.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="parentId" title="${message(code: 'productOrder.parentId.label', default: 'Parent Id')}"/>
				
				<th><g:message code="productOrder.soldTo.label" default="Sold To"/></th>
				
				<th><g:message code="productOrder.productInstance.label" default="Product Instance"/></th>
				
					<g:sortableColumn property="rel" title="${message(code: 'productOrder.rel.label', default: 'Rel')}"/>
				
				<th><g:message code="productOrder.productSpecification.label" default="Product Specification"/></th>
				
					<g:sortableColumn property="orderType" title="${message(code: 'productOrder.orderType.label', default: 'Order Type')}"/>
				
					<g:sortableColumn property="opportunityValue" title="${message(code: 'productOrder.opportunityValue.label', default: 'Opportunity Value')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'productOrder.status.label', default: 'Status')}"/>
				
					<g:sortableColumn property="childrenCompleted" title="${message(code: 'productOrder.childrenCompleted.label', default: 'Children Completed')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'productOrder.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'productOrder.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="productOrder.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${productOrderInstanceList}" status="i" var="productOrderInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${productOrderInstance.id}">${productOrderInstance}</g:link></td>
					
					<td>${fieldValue(bean: productOrderInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: productOrderInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: productOrderInstance, field: "parentId")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(productOrderInstance?.soldTo)?.class?.simpleName}" action="show" id="${productOrderInstance?.soldTo?.id}">${productOrderInstance?.soldTo?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(productOrderInstance?.productInstance)?.class?.simpleName}" action="show" id="${productOrderInstance?.productInstance?.id}">${productOrderInstance?.productInstance?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: productOrderInstance, field: "rel")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(productOrderInstance?.productSpecification)?.class?.simpleName}" action="show" id="${productOrderInstance?.productSpecification?.id}">${productOrderInstance?.productSpecification?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: productOrderInstance, field: "orderType")}</td>
					
					<td>
						
						${fieldValue(bean: productOrderInstance, field: "opportunityValue")}
						
					</td>
					
					<td>${fieldValue(bean: productOrderInstance, field: "status")}</td>
					
					<td><g:formatBoolean boolean="${productOrderInstance.childrenCompleted}"/></td>
							
					<td><g:formatDate date="${productOrderInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${productOrderInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${productOrderInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productOrderInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${productOrderInstanceCount}"/>
	</div>
</section>

</body>

</html>
