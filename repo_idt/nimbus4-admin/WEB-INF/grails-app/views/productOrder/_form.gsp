<%@ page import="com.verecloud.nimbus4.product.ProductOrder" %>


<div class="${hasErrors(bean: productOrderInstance, field: 'parentId', 'error')} ">
	<label for="parentId" class="control-label"><g:message code="productOrder.parentId.label" default="Parent Id"/> </label>
	<div>
		
			<g:field class="form-control" name="parentId" type="number" value="${productOrderInstance.parentId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'soldTo', 'error')} required">
	<label for="soldTo" class="control-label"><g:message code="productOrder.soldTo.label" default="Sold To"/> <span class="required-indicator">*</span></label>
	<div>

		<g:select class="form-control" id="soldTo" name="soldTo.id" from="${((com.verecloud.nimbus4.party.Group.findAllChildrenByGroup(com.verecloud.nimbus4.party.Group.get(nimbus.findTenantId().toLong()))) + com.verecloud.nimbus4.party.Group.createCriteria().list {suppliers {eq('id', nimbus.findTenantId().toLong())}})}" optionKey="id" required="" value="${productOrderInstance?.soldTo?.id}" class="many-to-one"/>

	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'productInstance', 'error')} ">
	<label for="productInstance" class="control-label"><g:message code="productOrder.productInstance.label" default="Product Instance"/> </label>
	<div>
		
			<g:select class="form-control" id="productInstance" name="productInstance.id" from="${com.verecloud.nimbus4.product.ProductInstance.list()}" optionKey="id" value="${productOrderInstance?.productInstance?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'rel', 'error')} ">
	<label for="rel" class="control-label"><g:message code="productOrder.rel.label" default="Rel"/> </label>
	<div>
		
			<g:textField class="form-control" name="rel" value="${productOrderInstance?.rel}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'productSpecification', 'error')} required">
	<label for="productSpecification" class="control-label"><g:message code="productOrder.productSpecification.label" default="Product Specification"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="productSpecification" name="productSpecification.id" from="${com.verecloud.nimbus4.product.ProductSpecification.findAllByTenantId(nimbus.findTenantId().toLong())}" optionKey="id" required="" value="${productOrderInstance?.productSpecification?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'orderType', 'error')} required">
	<label for="orderType" class="control-label"><g:message code="productOrder.orderType.label" default="Order Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="orderType" from="${com.verecloud.nimbus4.service.enums.OrderType?.values()}" keys="${com.verecloud.nimbus4.service.enums.OrderType.values()*.name()}" required="" value="${productOrderInstance?.orderType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'opportunityValue', 'error')} ">
	<label for="opportunityValue" class="control-label"><g:message code="productOrder.opportunityValue.label" default="Opportunity Value"/> </label>
	<div>
		
			<g:field class="form-control" name="opportunityValue" value="${productOrderInstance.opportunityValue}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'status', 'error')} ">
	<label for="status" class="control-label"><g:message code="productOrder.status.label" default="Status"/> </label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.OrderStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.OrderStatus.values()*.name()}" value="${productOrderInstance?.status?.name()}" noSelection="['': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'configuration', 'error')} required">
	<label for="configuration" class="control-label"><g:message code="productOrder.configuration.label" default="Configuration"/> <span class="required-indicator">*</span></label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(productOrderInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(productOrderInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'price', 'error')} ">
	<label for="price" class="control-label"><g:message code="productOrder.price.label" default="Price"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="price" value="${(productOrderInstance?.price as grails.converters.JSON).toString(true)}" />
            <div id="priceResizable"><div id="priceEditor">${(productOrderInstance?.price as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'childrenCompleted', 'error')} ">
	<label for="childrenCompleted" class="control-label"><g:message code="productOrder.childrenCompleted.label" default="Children Completed"/> </label>
	<div>
		
			<nimbus:checkBox name="childrenCompleted"  disabled="disabled" value="${productOrderInstance?.childrenCompleted}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'children', 'error')} ">
	<label for="children" class="control-label"><g:message code="productOrder.children.label" default="Children"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${productOrderInstance?.children}" var="c">
			    <g:if test="${c}">
                    <li>
                        <g:if test="${c?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
                                <g:link
                                    controller="${c?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.value?.class?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
                                <g:link
                                    controller="${c?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.class?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'serviceOrder').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['productOrder.id': productOrderInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'notes', 'error')} ">
	<label for="notes" class="control-label"><g:message code="productOrder.notes.label" default="Notes"/> </label>
	<div>
		
			<g:if test="${productOrderInstance?.notes?.getClass() && Map.isAssignableFrom(productOrderInstance?.notes?.getClass())}"><g:select class="form-control" name="notes" from="${com.verecloud.nimbus4.Note.list()}" multiple="multiple" optionKey="id" size="5" value="${productOrderInstance?.notes*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="notes" from="${com.verecloud.nimbus4.Note.list()}" multiple="multiple" optionKey="id" size="5" value="${productOrderInstance?.notes*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: productOrderInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="productOrder.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #priceResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #priceResizable{position: relative}
                     #priceEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                priceEditor = ace.edit("priceEditor");
                                var price = $('textarea[name="price"]').hide();
                                priceEditor.session.setMode("ace/mode/json");
                                priceEditor.setTheme("ace/theme/tomorrow");
                                priceEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('priceEditor').style.fontSize='15px'
                                priceEditor.setReadOnly(false)
                                priceEditor.getSession().on('change', function(){
                                    price.val(priceEditor.getSession().getValue());
                                });
                                 jq("#priceResizable").resizable({
                                    resize: function( event, ui ) {
                                    priceEditor.resize();
                                  }
                                 });

            
                    });
                </script>

