<%@ page import="com.verecloud.nimbus4.product.ProductOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productOrder.label', default: 'ProductOrder')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-productOrder" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${productOrderInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productOrderInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productOrderInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productOrderInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.parentId.label" default="Parent Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productOrderInstance, field: "parentId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.soldTo.label" default="Sold To" /></td>
			
			<td valign="top" class="value"><g:link controller="${productOrderInstance?.soldTo?.class?.simpleName}" action="show" id="${productOrderInstance?.soldTo?.id}">${productOrderInstance?.soldTo?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.productInstance.label" default="Product Instance" /></td>
			
			<td valign="top" class="value"><g:link controller="${productOrderInstance?.productInstance?.class?.simpleName}" action="show" id="${productOrderInstance?.productInstance?.id}">${productOrderInstance?.productInstance?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.rel.label" default="Rel" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productOrderInstance, field: "rel")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.productSpecification.label" default="Product Specification" /></td>
			
			<td valign="top" class="value"><g:link controller="${productOrderInstance?.productSpecification?.class?.simpleName}" action="show" id="${productOrderInstance?.productSpecification?.id}">${productOrderInstance?.productSpecification?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.orderType.label" default="Order Type" /></td>
			
			<td valign="top" class="value">${productOrderInstance?.orderType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.opportunityValue.label" default="Opportunity Value" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: productOrderInstance, field: "opportunityValue")}
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${productOrderInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(productOrderInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(productOrderInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.price.label" default="Price" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="price" value="${(productOrderInstance?.price as grails.converters.JSON).toString(true)}" />
            <div id="priceResizable"><div id="priceEditor">${(productOrderInstance?.price as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.childrenCompleted.label" default="Children Completed" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${productOrderInstance?.childrenCompleted}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.children.label" default="Children" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productOrderInstance?.children}" var="c">
	<g:if test="${c}">
		<li>
			<g:if test="${c?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
					<g:link
							controller="${c?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.value?.class?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
					<g:link
							controller="${c?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.class?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productOrderInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productOrderInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productOrder.notes.label" default="Notes" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productOrderInstance?.notes}" var="n">
	<g:if test="${n}">
		<li>
			<g:if test="${n?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(n?.value?.class)}">
					<g:link
							controller="${n?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${n?.value?.id}">
						${n?.key?.encodeAsHTML()+" : "+n?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${n?.value?.class?.simpleName}"
							action="show"
							id="${n?.value?.id}">
						${n?.key?.encodeAsHTML()+" : "+n?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(n?.class)}">
					<g:link
							controller="${n?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${n?.id}">
						${n?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${n?.class?.simpleName}"
							action="show"
							id="${n?.id}">
						${n?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #priceResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #priceResizable{position: relative}
                     #priceEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                priceEditor = ace.edit("priceEditor");
                                var price = $('textarea[name="price"]').hide();
                                priceEditor.session.setMode("ace/mode/json");
                                priceEditor.setTheme("ace/theme/tomorrow");
                                priceEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('priceEditor').style.fontSize='15px'
                                priceEditor.setReadOnly(true)
                                priceEditor.getSession().on('change', function(){
                                    price.val(priceEditor.getSession().getValue());
                                });
                                 jq("#priceResizable").resizable({
                                    resize: function( event, ui ) {
                                    priceEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
