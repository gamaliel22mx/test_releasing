<%@ page import="com.verecloud.nimbus4.product.ProductInstance" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productInstance.label', default: 'ProductInstance')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-productInstance" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'productInstance.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'productInstance.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
				<th><g:message code="productInstance.group.label" default="Group"/></th>
				
				<th><g:message code="productInstance.productSpecification.label" default="Product Specification"/></th>
				
					<g:sortableColumn property="status" title="${message(code: 'productInstance.status.label', default: 'Status')}"/>
				
					<g:sortableColumn property="activationDate" title="${message(code: 'productInstance.activationDate.label', default: 'Activation Date')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'productInstance.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'productInstance.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="productInstance.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${productInstanceInstanceList}" status="i" var="productInstanceInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${productInstanceInstance.id}">${productInstanceInstance}</g:link></td>
					
					<td>${fieldValue(bean: productInstanceInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: productInstanceInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(productInstanceInstance?.group)?.class?.simpleName}" action="show" id="${productInstanceInstance?.group?.id}">${productInstanceInstance?.group?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(productInstanceInstance?.productSpecification)?.class?.simpleName}" action="show" id="${productInstanceInstance?.productSpecification?.id}">${productInstanceInstance?.productSpecification?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: productInstanceInstance, field: "status")}</td>
					
					<td><g:formatDate date="${productInstanceInstance.activationDate}"/></td>
					
					<td><g:formatDate date="${productInstanceInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${productInstanceInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${productInstanceInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productInstanceInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${productInstanceInstanceCount}"/>
	</div>
</section>

</body>

</html>
