<%@ page import="com.verecloud.nimbus4.product.ProductInstance" %>


<div class="${hasErrors(bean: productInstanceInstance, field: 'group', 'error')} required">
	<label for="group" class="control-label"><g:message code="productInstance.group.label" default="Group"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="group" name="group.id" from="${com.verecloud.nimbus4.party.Group.list()}" optionKey="id" required="" value="${productInstanceInstance?.group?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'productSpecification', 'error')} required">
	<label for="productSpecification" class="control-label"><g:message code="productInstance.productSpecification.label" default="Product Specification"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="productSpecification" name="productSpecification.id" from="${com.verecloud.nimbus4.product.ProductSpecification.list()}" optionKey="id" required="" value="${productInstanceInstance?.productSpecification?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="productInstance.configuration.label" default="Configuration"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(productInstanceInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(productInstanceInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'serviceInstances', 'error')} ">
	<label for="serviceInstances" class="control-label"><g:message code="productInstance.serviceInstances.label" default="Service Instances"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${productInstanceInstance?.serviceInstances}" var="s">
			    <g:if test="${s}">
                    <li>
                        <g:if test="${s?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.value?.class)}">
                                <g:link
                                    controller="${s?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${s?.value?.id}">
                                    ${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${s?.value?.class?.simpleName}"
                                    action="show"
                                    id="${s?.value?.id}">
                                    ${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.class)}">
                                <g:link
                                    controller="${s?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${s.id}">
                                    ${s?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${s?.class?.simpleName}"
                                    action="show"
                                    id="${s.id}">
                                    ${s?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'serviceInstance').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['productInstance.id': productInstanceInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'status', 'error')} ">
	<label for="status" class="control-label"><g:message code="productInstance.status.label" default="Status"/> </label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.InstanceStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.InstanceStatus.values()*.name()}" value="${productInstanceInstance?.status?.name()}" noSelection="['': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'activationDate', 'error')} ">
	<label for="activationDate" class="control-label"><g:message code="productInstance.activationDate.label" default="Activation Date"/> </label>
	<div>
		
			<bs:datePicker name="activationDate" precision="day"  value="${productInstanceInstance?.activationDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'chargingLineItemGroups', 'error')} ">
	<label for="chargingLineItemGroups" class="control-label"><g:message code="productInstance.chargingLineItemGroups.label" default="Charging Line Item Groups"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${productInstanceInstance?.chargingLineItemGroups}" var="c">
			    <g:if test="${c}">
                    <li>
                        <g:if test="${c?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
                                <g:link
                                    controller="${c?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.value?.class?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
                                <g:link
                                    controller="${c?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.class?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'chargingLineItemGroup').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['productInstance.id': productInstanceInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: productInstanceInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="productInstance.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>

