<%@ page import="com.verecloud.nimbus4.product.ProductInstance" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productInstance.label', default: 'ProductInstance')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-productInstance" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${productInstanceInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productInstanceInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstanceInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstanceInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.group.label" default="Group" /></td>
			
			<td valign="top" class="value"><g:link controller="${productInstanceInstance?.group?.class?.simpleName}" action="show" id="${productInstanceInstance?.group?.id}">${productInstanceInstance?.group?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.productSpecification.label" default="Product Specification" /></td>
			
			<td valign="top" class="value"><g:link controller="${productInstanceInstance?.productSpecification?.class?.simpleName}" action="show" id="${productInstanceInstance?.productSpecification?.id}">${productInstanceInstance?.productSpecification?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(productInstanceInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(productInstanceInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.serviceInstances.label" default="Service Instances" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productInstanceInstance?.serviceInstances}" var="s">
	<g:if test="${s}">
		<li>
			<g:if test="${s?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.value?.class)}">
					<g:link
							controller="${s?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${s?.value?.id}">
						${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${s?.value?.class?.simpleName}"
							action="show"
							id="${s?.value?.id}">
						${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.class)}">
					<g:link
							controller="${s?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${s?.id}">
						${s?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${s?.class?.simpleName}"
							action="show"
							id="${s?.id}">
						${s?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${productInstanceInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.activationDate.label" default="Activation Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productInstanceInstance?.activationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.chargingLineItemGroups.label" default="Charging Line Item Groups" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${productInstanceInstance?.chargingLineItemGroups}" var="c">
	<g:if test="${c}">
		<li>
			<g:if test="${c?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
					<g:link
							controller="${c?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.value?.class?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
					<g:link
							controller="${c?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.class?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productInstanceInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productInstance.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productInstanceInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
