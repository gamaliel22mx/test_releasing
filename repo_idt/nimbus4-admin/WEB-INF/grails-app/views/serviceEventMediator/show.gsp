<%@ page import="com.verecloud.nimbus4.service.ServiceEventMediator" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceEventMediator.label', default: 'ServiceEventMediator')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-serviceEventMediator" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceEventMediatorInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceEventMediatorInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceEventMediatorInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceEventMediatorInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceEventMediatorInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: serviceEventMediatorInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.groovyScript.label" default="Groovy Script" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="groovyScript" value="${serviceEventMediatorInstance?.groovyScript}" />
            <div id="groovyScriptResizable"><div id="groovyScriptEditor">${serviceEventMediatorInstance?.groovyScript}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceEventMediatorInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="serviceEventMediator.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${serviceEventMediatorInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #groovyScriptResizable { width: 1100px; height: 1000px; padding: 5px; border: 1px solid #aedeae}
                     #groovyScriptResizable{position: relative}
                     #groovyScriptEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                groovyScriptEditor = ace.edit("groovyScriptEditor");
                                var groovyScript = $('textarea[name="groovyScript"]').hide();
                                groovyScriptEditor.session.setMode("ace/mode/groovy");
                                groovyScriptEditor.setTheme("ace/theme/tomorrow");
                                groovyScriptEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('groovyScriptEditor').style.fontSize='15px'
                                groovyScriptEditor.setReadOnly(true)
                                groovyScriptEditor.getSession().on('change', function(){
                                    groovyScript.val(groovyScriptEditor.getSession().getValue());
                                });
                                 jq("#groovyScriptResizable").resizable({
                                    resize: function( event, ui ) {
                                    groovyScriptEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
