<%@ page import="com.verecloud.nimbus4.service.ServiceEventMediator" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceEventMediator.label', default: 'ServiceEventMediator')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-serviceEventMediator" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'serviceEventMediator.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'serviceEventMediator.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'serviceEventMediator.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="description" title="${message(code: 'serviceEventMediator.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'serviceEventMediator.groovyScript.label', default: 'Groovy Script')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'serviceEventMediator.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'serviceEventMediator.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="serviceEventMediator.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${serviceEventMediatorInstanceList}" status="i" var="serviceEventMediatorInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${serviceEventMediatorInstance.id}">${serviceEventMediatorInstance}</g:link></td>
					
					<td>${fieldValue(bean: serviceEventMediatorInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: serviceEventMediatorInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: serviceEventMediatorInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: serviceEventMediatorInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: serviceEventMediatorInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${serviceEventMediatorInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${serviceEventMediatorInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceEventMediatorInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceEventMediatorInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${serviceEventMediatorInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
