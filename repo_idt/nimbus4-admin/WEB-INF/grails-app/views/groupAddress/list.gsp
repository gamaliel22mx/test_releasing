<%@ page import="com.verecloud.nimbus4.party.GroupAddress" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupAddress.label', default: 'GroupAddress')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-groupAddress" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'groupAddress.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'groupAddress.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="addressLine1" title="${message(code: 'groupAddress.addressLine1.label', default: 'Address Line1')}"/>
				
					<g:sortableColumn property="addressLine2" title="${message(code: 'groupAddress.addressLine2.label', default: 'Address Line2')}"/>
				
					<g:sortableColumn property="city" title="${message(code: 'groupAddress.city.label', default: 'City')}"/>
				
					<g:sortableColumn property="state" title="${message(code: 'groupAddress.state.label', default: 'State')}"/>
				
					<g:sortableColumn property="zipCode" title="${message(code: 'groupAddress.zipCode.label', default: 'Zip Code')}"/>
				
					<g:sortableColumn property="country" title="${message(code: 'groupAddress.country.label', default: 'Country')}"/>
				
					<g:sortableColumn property="phoneNumber" title="${message(code: 'groupAddress.phoneNumber.label', default: 'Phone Number')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'groupAddress.dateCreated.label', default: 'Date Created')}"/>
				
				<th><g:message code="groupAddress.group.label" default="Group"/></th>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'groupAddress.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="groupAddress.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupAddressInstanceList}" status="i" var="groupAddressInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupAddressInstance.id}">${groupAddressInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "addressLine1")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "addressLine2")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "city")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "state")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "zipCode")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "country")}</td>
					
					<td>${fieldValue(bean: groupAddressInstance, field: "phoneNumber")}</td>
					
					<td><g:formatDate date="${groupAddressInstance.dateCreated}"/></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupAddressInstance?.group)?.class?.simpleName}" action="show" id="${groupAddressInstance?.group?.id}">${groupAddressInstance?.group?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${groupAddressInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${groupAddressInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(groupAddressInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupAddressInstanceCount}"/>
	</div>
</section>

</body>

</html>
