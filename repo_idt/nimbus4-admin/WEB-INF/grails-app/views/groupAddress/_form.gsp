<%@ page import="com.verecloud.nimbus4.party.GroupAddress" %>


<div class="${hasErrors(bean: groupAddressInstance, field: 'addressLine1', 'error')} required">
	<label for="addressLine1" class="control-label"><g:message code="groupAddress.addressLine1.label" default="Address Line1"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="addressLine1" required="" value="${groupAddressInstance?.addressLine1}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'addressLine2', 'error')} ">
	<label for="addressLine2" class="control-label"><g:message code="groupAddress.addressLine2.label" default="Address Line2"/> </label>
	<div>
		
			<g:textField class="form-control" name="addressLine2" value="${groupAddressInstance?.addressLine2}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'city', 'error')} required">
	<label for="city" class="control-label"><g:message code="groupAddress.city.label" default="City"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="city" required="" value="${groupAddressInstance?.city}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'state', 'error')} required">
	<label for="state" class="control-label"><g:message code="groupAddress.state.label" default="State"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="state" required="" value="${groupAddressInstance?.state}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'zipCode', 'error')} required">
	<label for="zipCode" class="control-label"><g:message code="groupAddress.zipCode.label" default="Zip Code"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="zipCode" required="" value="${groupAddressInstance?.zipCode}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'country', 'error')} required">
	<label for="country" class="control-label"><g:message code="groupAddress.country.label" default="Country"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="country" required="" value="${groupAddressInstance?.country}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'phoneNumber', 'error')} required">
	<label for="phoneNumber" class="control-label"><g:message code="groupAddress.phoneNumber.label" default="Phone Number"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="phoneNumber" required="" value="${groupAddressInstance?.phoneNumber}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'group', 'error')} required">
	<label for="group" class="control-label"><g:message code="groupAddress.group.label" default="Group"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="group" name="group.id" from="${com.verecloud.nimbus4.party.Group.list()}" optionKey="id" required="" value="${groupAddressInstance?.group?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupAddressInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="groupAddress.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

