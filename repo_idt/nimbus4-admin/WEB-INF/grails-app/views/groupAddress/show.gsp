<%@ page import="com.verecloud.nimbus4.party.GroupAddress" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupAddress.label', default: 'GroupAddress')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-groupAddress" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${groupAddressInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(groupAddressInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.addressLine1.label" default="Address Line1" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "addressLine1")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.addressLine2.label" default="Address Line2" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "addressLine2")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.city.label" default="City" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "city")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.state.label" default="State" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "state")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.zipCode.label" default="Zip Code" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "zipCode")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.country.label" default="Country" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "country")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.phoneNumber.label" default="Phone Number" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupAddressInstance, field: "phoneNumber")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupAddressInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.group.label" default="Group" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupAddressInstance?.group?.class?.simpleName}" action="show" id="${groupAddressInstance?.group?.id}">${groupAddressInstance?.group?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupAddress.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupAddressInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
