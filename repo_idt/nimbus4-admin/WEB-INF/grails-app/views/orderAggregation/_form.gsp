<%@ page import="com.verecloud.nimbus4.OrderAggregation" %>


<div class="${hasErrors(bean: orderAggregationInstance, field: 'children', 'error')} ">
	<label for="children" class="control-label"><g:message code="orderAggregation.children.label" default="Children"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${orderAggregationInstance?.children}" var="c">
			    <g:if test="${c}">
                    <li>
                        <g:if test="${c?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
                                <g:link
                                    controller="${c?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.value?.class?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
                                <g:link
                                    controller="${c?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.class?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'orderAggregationChild').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['orderAggregation.id': orderAggregationInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: orderAggregationInstance, field: 'parentId', 'error')} required">
	<label for="parentId" class="control-label"><g:message code="orderAggregation.parentId.label" default="Parent Id"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="parentId" type="number" value="${orderAggregationInstance.parentId}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: orderAggregationInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="orderAggregation.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<div class="${hasErrors(bean: orderAggregationInstance, field: 'type', 'error')} ">
	<label for="type" class="control-label"><g:message code="orderAggregation.type.label" default="Type"/> </label>
	<div>
		
			<g:textField class="form-control" name="type" value="${orderAggregationInstance?.type}"/>
		
	</div>
</div>

