<%@ page import="com.verecloud.nimbus4.OrderAggregation" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'orderAggregation.label', default: 'OrderAggregation')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-orderAggregation" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="parentId" title="${message(code: 'orderAggregation.parentId.label', default: 'Parent Id')}"/>
				
					<g:sortableColumn property="type" title="${message(code: 'orderAggregation.type.label', default: 'Type')}"/>
				
				<th><g:message code="orderAggregation.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${orderAggregationInstanceList}" status="i" var="orderAggregationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${orderAggregationInstance.id}">${orderAggregationInstance}</g:link></td>
					
					<td>${fieldValue(bean: orderAggregationInstance, field: "parentId")}</td>
					
					<td>${fieldValue(bean: orderAggregationInstance, field: "type")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${orderAggregationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(orderAggregationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${orderAggregationInstanceCount}"/>
	</div>
</section>

</body>

</html>
