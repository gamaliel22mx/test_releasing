<%@ page import="com.verecloud.nimbus4.service.ServiceSpecification" %>


<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'name', 'error')} required">
	<label for="name" class="control-label"><g:message code="serviceSpecification.name.label" default="Name"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="name" required="" value="${serviceSpecificationInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="serviceSpecification.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.SpecificationStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.SpecificationStatus.values()*.name()}" required="" value="${serviceSpecificationInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'externalId', 'error')} ">
	<label for="externalId" class="control-label"><g:message code="serviceSpecification.externalId.label" default="External Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="externalId" value="${serviceSpecificationInstance?.externalId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="serviceSpecification.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${serviceSpecificationInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'eventsMediator', 'error')} ">
	<label for="eventsMediator" class="control-label"><g:message code="serviceSpecification.eventsMediator.label" default="Events Mediator"/> </label>
	<div>
		
			<g:select class="form-control" id="eventsMediator" name="eventsMediator.id" from="${com.verecloud.nimbus4.service.ServiceEventMediator.list()}" optionKey="id" value="${serviceSpecificationInstance?.eventsMediator?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'schema', 'error')} required">
	<label for="schema" class="control-label"><g:message code="serviceSpecification.schema.label" default="Schema"/> <span class="required-indicator">*</span></label>
	<div>
		

		
           <g:textArea class="form-control" name="schema" value="${(serviceSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(serviceSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'resourceSpecifications', 'error')} ">
	<label for="resourceSpecifications" class="control-label"><g:message code="serviceSpecification.resourceSpecifications.label" default="Resource Specifications"/> </label>
	<div>
		
			<g:if test="${serviceSpecificationInstance?.resourceSpecifications?.getClass() && Map.isAssignableFrom(serviceSpecificationInstance?.resourceSpecifications?.getClass())}"><g:select class="form-control" name="resourceSpecifications" from="${com.verecloud.nimbus4.resource.ResourceSpecification.list()}" multiple="multiple" optionKey="id" size="5" value="${serviceSpecificationInstance?.resourceSpecifications*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="resourceSpecifications" from="${com.verecloud.nimbus4.resource.ResourceSpecification.list()}" multiple="multiple" optionKey="id" size="5" value="${serviceSpecificationInstance?.resourceSpecifications*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'serviceVisualizers', 'error')} ">
	<label for="serviceVisualizers" class="control-label"><g:message code="serviceSpecification.serviceVisualizers.label" default="Service Visualizers"/> </label>
	<div>
		
			<g:if test="${serviceSpecificationInstance?.serviceVisualizers?.getClass() && Map.isAssignableFrom(serviceSpecificationInstance?.serviceVisualizers?.getClass())}"><g:select class="form-control" name="serviceVisualizers" from="${com.verecloud.nimbus4.service.ServiceVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${serviceSpecificationInstance?.serviceVisualizers*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="serviceVisualizers" from="${com.verecloud.nimbus4.service.ServiceVisualizer.list()}" multiple="multiple" optionKey="id" size="5" value="${serviceSpecificationInstance?.serviceVisualizers*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="serviceSpecification.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(false)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>

