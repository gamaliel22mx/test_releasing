<%@ page import="com.verecloud.nimbus4.service.ServiceSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceSpecification.label', default: 'ServiceSpecification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-serviceSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'serviceSpecification.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'serviceSpecification.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'serviceSpecification.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'serviceSpecification.status.label', default: 'Status')}"/>
				
					<g:sortableColumn property="externalId" title="${message(code: 'serviceSpecification.externalId.label', default: 'External Id')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'serviceSpecification.description.label', default: 'Description')}"/>
				
				<th><g:message code="serviceSpecification.eventsMediator.label" default="Events Mediator"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'serviceSpecification.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'serviceSpecification.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="serviceSpecification.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${serviceSpecificationInstanceList}" status="i" var="serviceSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${serviceSpecificationInstance.id}">${serviceSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: serviceSpecificationInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: serviceSpecificationInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: serviceSpecificationInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: serviceSpecificationInstance, field: "status")}</td>
					
					<td>${fieldValue(bean: serviceSpecificationInstance, field: "externalId")}</td>
					
					<td>${fieldValue(bean: serviceSpecificationInstance, field: "description")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(serviceSpecificationInstance?.eventsMediator)?.class?.simpleName}" action="show" id="${serviceSpecificationInstance?.eventsMediator?.id}">${serviceSpecificationInstance?.eventsMediator?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${serviceSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${serviceSpecificationInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${serviceSpecificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(serviceSpecificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${serviceSpecificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
