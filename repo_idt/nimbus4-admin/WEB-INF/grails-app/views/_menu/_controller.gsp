<%@ page import="com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants; com.verecloud.nimbus4.util.Nimbus4Constants" %>
<%--
each menubar heading ifthe last part of the package name
ex.
com.verecloud.nimbus4.service
should become 'Service'
 --%>
<g:set var="sectionMap" value="[:]"/>
<g:render template="/_menu/linkSection"/>
<g:each var="cMap" in="${sectionMap.sort()}">
	<g:if test="${nimbus.hasAnyPermitted([label: cMap.key, domains: cMap.value]).toString().equals('true')}">
		<li class="dropdown controllerListMenu">
			<a class="dropdown-toggle"
			   data-toggle="dropdown"
			   href="#">${cMap.key}
				<b class="caret"></b>
			</a>
			<g:set var="minulScrollSize" value="0"/>
			<ul class="dropdown-menu">
				<g:each var="c" in="${request.getAttribute(Nimbus4Constants.HAS_ANY_PERMITTED)?.get(cMap?.key)}">
					<% minulScrollSize?.toInteger()++; %>
					<li class="controller">
						<g:link controller="${c.logicalPropertyName}">
							<g:if test="${c.fullName.contains('HomeController')}">
								<i class="glyphicon glyphicon-home"></i>
							</g:if>
							<g:elseif test="${c.fullName.contains('DemoPageController')}">
								<i class="glyphicon glyphicon-list-alt"></i>
							</g:elseif>
							<g:elseif test="${c.fullName.contains('DbdocController')}">
								<i class="glyphicon glyphicon-cloud"></i>
							</g:elseif>
							${c.name}
						</g:link>
					</li>
				</g:each>
			</ul>
		</li>
		<% if (minulScrollSize?.toInteger() > 10) { %>
		<script type="application/javascript">
			$("ul.dropdown-menu").addClass("ulScroll")
		</script>
		<% } %>
	</g:if>
</g:each>