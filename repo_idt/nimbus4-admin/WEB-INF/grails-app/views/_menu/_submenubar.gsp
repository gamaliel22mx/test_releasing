<%@ page import=" org.codehaus.groovy.grails.scaffolding.DomainClassPropertyComparator" %>
<!--
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->

<%-- Only show the "Pills" navigation menu if a controller exists (but not for home) --%>

<shiro:isLoggedIn>
    <g:if test="${params.controller != null && params.controller != '' && params.controller != 'home' && params.controller != 'audit'}">
        <ul id="Menu" class="nav nav-pills margin-top-small">

            <g:set var="entityName"
                   value="${message(code: params.controller + '.label', default: params.controller.substring(0, 1).toUpperCase() + params.controller.substring(1))}"/>

            <g:if test="${nimbus.isPermitted([domainClass: params.controller, action: 'list']).equals('true')}">
                <li class="${params.action == "list" ? 'active' : ''}">
                    <g:link controller="${params.controller}" action="list"><i
                            class="glyphicon glyphicon-th-list"></i> <g:message code="default.list.label"
                                                                                args="[entityName]"/></g:link>
                </li>
            </g:if>
            <g:set var="isValidTenant"
                   value="${nimbus.isValidTenant(domainClass: entityName, id: params.id)?.toString()?.equals('true')}"/>
            <g:set var="flag" value="${isValidTenant}"/>

            <g:if test="${nimbus.isPermitted([domainClass: params.controller, action: 'create']).equals('true')}">
                <li class="${params.action == "create" ? 'active' : ''} ${flag ? '' : 'disabled'}">
                    <g:link controller="${params.controller}" action="create" onclick="return ${flag};">
                        <i class="glyphicon glyphicon-plus"></i>
                        <g:message code="default.new.label" args="[entityName]"/>
                    </g:link>
                </li>
            </g:if>

            <g:if test="${params.action == 'show' || params.action == 'edit'}">
                <g:if test="${nimbus.isPermitted([domainClass: params.controller, action: 'edit']).equals('true')}">
                    <li class="${params.action == "edit" ? 'active' : ''} ${flag ? '' : 'disabled'}">
                        <g:link controller="${params.controller}" action="edit" id="${params.id}"
                                onclick="return ${flag};">
                            <i class="glyphicon glyphicon-pencil"></i>
                            <g:message code="default.edit.label" args="[entityName]"/>
                        </g:link>
                    </li>
                </g:if>
                <g:if test="${nimbus.isPermitted([domainClass: params.controller, action: 'delete']).equals('true')}">
                    <li class="${flag ? '' : 'disabled'}">
                        <g:render template="/_common/modals/deleteTextLink"/>
                    </li>
                </g:if>
            </g:if>

            <g:if test="${nimbus.isAuditable([domainClass: clazz]) == 'true'}">
                <g:if test="${nimbus.isPermitted([domainClass: params.controller, action: 'history']).equals('true')}">
                    <li class="${params.action == "history" ? 'active' : ''}">
                        <g:link controller="${params.controller}" action="history" id="${params.id}"><i
                                class="glyphicon glyphicon-time"></i>&nbsp;<g:message code="default.show.history.label"
                                                                                      args="[entityName]"/></g:link>
                    </li>
                </g:if>
            </g:if>
            <g:if test="${actionName in ['index', 'list', 'search'] && params.action != 'history'}">
                <li class="pull-right">
                    <g:render template="/_common/component/search"/>
                </li>
            </g:if>

        </ul>
    </g:if>
</shiro:isLoggedIn>