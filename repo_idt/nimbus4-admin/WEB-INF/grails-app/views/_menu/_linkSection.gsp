<%@ page import="com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants" %>
<g:set var="sectionValues" value="[]"/>
<g:each var="section" in="${
    grailsApplication.controllerClasses.collect { it.packageName }.toSet().sort { it }
}">
    <g:if test="${!(section in Nimbus4ScaffoldingConstants.EXCLUDED_PACKAGES_LIST)}">
        <g:set var="sectionKey"
               value="${section.lastIndexOf('.') < 0 ? section.capitalize() : section.substring(section.lastIndexOf('.') + 1).capitalize()}"/>
        <g:set var="sectionValue"
               value="${grailsApplication.controllerClasses.findAll {
                   it.packageName.equals(section) && !(it.fullName in Nimbus4ScaffoldingConstants.EXCLUDED_CONTROLLERS_LIST)
               }.sort {
                   it.name
               }}"/>
        <g:set var="svalues" value="[]"/>
        <r:script>
            <g:each var="svalue" in="${sectionValue}">
                <g:if test="${!(svalue in sectionValues.flatten())}">
                    ${svalues.add(svalue)}
                </g:if>
            </g:each>
            <g:if test="${!svalues.isEmpty()}">
                <g:if test="${sectionMap.containsKey(sectionKey)}">
                    ${sectionMap.put(sectionKey, (sectionMap.get(sectionKey) + svalues))}
                </g:if>
                <g:else>
                    ${sectionMap.put(sectionKey, svalues)}
                </g:else>
            </g:if>
            <g:if test="${sectionKey != "Nimbus4"}">
                ${sectionValues.add(svalues)}
            </g:if>
        </r:script>
    </g:if>
</g:each>