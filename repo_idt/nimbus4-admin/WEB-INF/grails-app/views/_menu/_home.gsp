<%@ page import="com.verecloud.nimbus4.util.Nimbus4CoreUtil" %>
<g:if test="${Nimbus4CoreUtil.securityPrincipal}">
	<li class="dropdown">
		<a href="#SwitchTenantModal" class="dropdown" role="button" data-toggle="modal"
		   title='<nimbus:findTenant/>'>
			<nimbus:findTenant truncate="true"/>
		</a>
	</li>


	<li class="dropdown">
		<a class="dropdown-toggle"
		   data-toggle="dropdown"
		   href="#"><nimbus:findUserName/>
			<b class="caret"></b>
		</a>
		<ul class="dropdown-menu">
			<li class="controller">
				<g:link controller="auth" action="signOut">
					<g:message code="security.signoff.label" default="Sign Out"/>
				</g:link>
			</li>
		</ul>
	</li>
</g:if>