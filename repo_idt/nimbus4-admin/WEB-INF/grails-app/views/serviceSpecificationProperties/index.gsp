<%@ page import="com.verecloud.nimbus4.service.ServiceSpecificationProperties" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'serviceSpecificationProperties.label', default: 'ServiceSpecificationProperties')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-serviceSpecificationProperties" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="serviceProperties" title="${message(code: 'serviceSpecificationProperties.serviceProperties.label', default: 'Service Properties')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="serviceSpecificationId" title="${message(code: 'serviceSpecificationProperties.serviceSpecificationId.label', default: 'Service Specification Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${serviceSpecificationPropertiesInstanceList}" status="i" var="serviceSpecificationPropertiesInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${serviceSpecificationPropertiesInstance.id}">${serviceSpecificationPropertiesInstance}</g:link></td>
					
					<td>${fieldValue(bean: serviceSpecificationPropertiesInstance, field: "serviceProperties")}</td>
					
					<td>${fieldValue(bean: serviceSpecificationPropertiesInstance, field: "serviceSpecificationId")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${serviceSpecificationPropertiesInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
