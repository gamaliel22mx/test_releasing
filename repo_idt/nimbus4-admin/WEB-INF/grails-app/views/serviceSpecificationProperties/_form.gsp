<%@ page import="com.verecloud.nimbus4.service.ServiceSpecificationProperties" %>


<div class="${hasErrors(bean: serviceSpecificationPropertiesInstance, field: 'serviceProperties', 'error')} ">
	<label for="serviceProperties" class="control-label"><g:message code="serviceSpecificationProperties.serviceProperties.label" default="Service Properties"/> </label>
	<div>
		
			<g:hiddenField name="serviceProperties"/>
          <ul id="Menu" class="nav nav-pills margin-top-small">
            <li>
              <a title="New" role="button" href="javascript:void(0)" onclick="addBlankRow()">
                <i class="glyphicon glyphicon-plus"></i> New
              </a>
            </li>
            <li>
              <a title="Delete" role="button" href="javascript:void(0)" onclick="deleteCheckedRows()">
                <i class="glyphicon glyphicon-trash"></i> Delete
              </a>
            </li>
          </ul>
          <table id="editableTable" class="table table-striped">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>Key</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <g:each in="${serviceSpecificationPropertiesInstance?.serviceProperties}" var="param">
              <tr>
                <td><g:checkBox name="editableTableCheckbox" checked="false"/></td>
                <td>${param.key}</td>
                <td>${param.value}</td>
              </tr>
              </g:each>
            </tbody>
          </table>
		
	</div>
</div>

<div class="${hasErrors(bean: serviceSpecificationPropertiesInstance, field: 'serviceSpecificationId', 'error')} ">
	<label for="serviceSpecificationId" class="control-label"><g:message code="serviceSpecificationProperties.serviceSpecificationId.label" default="Service Specification Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="serviceSpecificationId" value="${serviceSpecificationPropertiesInstance?.serviceSpecificationId}"/>
		
	</div>
</div>

<r:script>
	$(document).ready(function () {
		refreshEditableTableWidget();
	});

	function addBlankRow(){
		$('#editableTable  > tbody:last').append('<tr><td><g:checkBox name="editableTableCheckbox" checked="false"/></td><td></td><td></tr>');
		refreshEditableTableWidget();
	}

	function deleteCheckedRows(){
		$('#editableTable tr').filter(':has(:checkbox:checked)').each(function() {
			$(this).remove();
		});
	}

	function refreshEditableTableWidget(){
		$('#editableTable').editableTableWidget();
		$('#editableTable tr').filter(':has(:checkbox)').each(function() {
			$(this).find('td:first').removeAttr('tabindex');
		});
	}

	$("form[role='form']").submit(function(){updateMapField()});
	function updateMapField(){
		var json = ""
		$('#editableTable tr').filter(':has(:checkbox)').each(function() {
            var key = $(this).find("td[tabindex='1']").first().text();
            if(key){
				json+='"'+key+'":'
                var value = $(this).find("td[tabindex='1']").last().text();
                if(value){
                    //if(jQuery.isNumeric(value) || isBoolean(value)){
						//json+=value+','
                    //}else{
						json += value+','
                    //}
                }else{
                    json+='null,'
                }
            }
		});
		json = json.substr(0,json.length-1)
		json = "{" + json + "}"
		$('#serviceProperties').val(json)
	}
</r:script>

