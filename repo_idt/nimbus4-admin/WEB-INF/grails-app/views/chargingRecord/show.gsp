<%@ page import="com.verecloud.nimbus4.billing.ChargingRecord" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'chargingRecord.label', default: 'ChargingRecord')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-chargingRecord" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${chargingRecordInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(chargingRecordInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.amount.label" default="Amount" /></td>
			
			<td valign="top" class="value">
				
				<g:formatNumber number="${chargingRecordInstance?.amount}" type="number" maxFractionDigits="10" />
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.billing.label" default="Billing" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="billing" value="${(chargingRecordInstance?.billing as grails.converters.JSON).toString(true)}" />
            <div id="billingResizable"><div id="billingEditor">${(chargingRecordInstance?.billing as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.chargeCode.label" default="Charge Code" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingRecordInstance, field: "chargeCode")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.chargeDescription.label" default="Charge Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingRecordInstance, field: "chargeDescription")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.chargeMode.label" default="Charge Mode" /></td>
			
			<td valign="top" class="value">${chargingRecordInstance?.chargeMode?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.chargeType.label" default="Charge Type" /></td>
			
			<td valign="top" class="value">${chargingRecordInstance?.chargeType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.currency.label" default="Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingRecordInstance, field: "currency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingRecordInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.groupId.label" default="Group Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingRecordInstance, field: "groupId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingRecordInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.meteringDate.label" default="Metering Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingRecordInstance?.meteringDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.productCode.label" default="Product Code" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingRecordInstance, field: "productCode")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.rateType.label" default="Rate Type" /></td>
			
			<td valign="top" class="value">${chargingRecordInstance?.rateType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingRecord.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${chargingRecordInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #billingResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #billingResizable{position: relative}
                     #billingEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                billingEditor = ace.edit("billingEditor");
                                var billing = $('textarea[name="billing"]').hide();
                                billingEditor.session.setMode("ace/mode/json");
                                billingEditor.setTheme("ace/theme/tomorrow");
                                billingEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('billingEditor').style.fontSize='15px'
                                billingEditor.setReadOnly(true)
                                billingEditor.getSession().on('change', function(){
                                    billing.val(billingEditor.getSession().getValue());
                                });
                                 jq("#billingResizable").resizable({
                                    resize: function( event, ui ) {
                                    billingEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
