<%@ page import="com.verecloud.nimbus4.billing.ChargingRecord" %>


<div class="${hasErrors(bean: chargingRecordInstance, field: 'amount', 'error')} required">
	<label for="amount" class="control-label"><g:message code="chargingRecord.amount.label" default="Amount"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="amount" value="${chargingRecordInstance.amount}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'billing', 'error')} ">
	<label for="billing" class="control-label"><g:message code="chargingRecord.billing.label" default="Billing"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="billing" value="${(chargingRecordInstance?.billing as grails.converters.JSON).toString(true)}" />
            <div id="billingResizable"><div id="billingEditor">${(chargingRecordInstance?.billing as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'chargeCode', 'error')} ">
	<label for="chargeCode" class="control-label"><g:message code="chargingRecord.chargeCode.label" default="Charge Code"/> </label>
	<div>
		
			<g:textField class="form-control" name="chargeCode" value="${chargingRecordInstance?.chargeCode}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'chargeDescription', 'error')} ">
	<label for="chargeDescription" class="control-label"><g:message code="chargingRecord.chargeDescription.label" default="Charge Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="chargeDescription" value="${chargingRecordInstance?.chargeDescription}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'chargeMode', 'error')} required">
	<label for="chargeMode" class="control-label"><g:message code="chargingRecord.chargeMode.label" default="Charge Mode"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="chargeMode" from="${com.verecloud.nimbus4.service.enums.ChargeMode?.values()}" keys="${com.verecloud.nimbus4.service.enums.ChargeMode.values()*.name()}" required="" value="${chargingRecordInstance?.chargeMode?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'chargeType', 'error')} required">
	<label for="chargeType" class="control-label"><g:message code="chargingRecord.chargeType.label" default="Charge Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="chargeType" from="${com.verecloud.nimbus4.service.enums.ChargeType?.values()}" keys="${com.verecloud.nimbus4.service.enums.ChargeType.values()*.name()}" required="" value="${chargingRecordInstance?.chargeType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'currency', 'error')} required">
	<label for="currency" class="control-label"><g:message code="chargingRecord.currency.label" default="Currency"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:currencySelect class='form-control' name="currency" value="${chargingRecordInstance?.currency}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'groupId', 'error')} ">
	<label for="groupId" class="control-label"><g:message code="chargingRecord.groupId.label" default="Group Id"/> </label>
	<div>
		
			<g:textField class="form-control" name="groupId" value="${chargingRecordInstance?.groupId}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'meteringDate', 'error')} required">
	<label for="meteringDate" class="control-label"><g:message code="chargingRecord.meteringDate.label" default="Metering Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="meteringDate" precision="day"  value="${chargingRecordInstance?.meteringDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'productCode', 'error')} ">
	<label for="productCode" class="control-label"><g:message code="chargingRecord.productCode.label" default="Product Code"/> </label>
	<div>
		
			<g:textField class="form-control" name="productCode" value="${chargingRecordInstance?.productCode}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'rateType', 'error')} required">
	<label for="rateType" class="control-label"><g:message code="chargingRecord.rateType.label" default="Rate Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="rateType" from="${com.verecloud.nimbus4.service.enums.ChargeRateType?.values()}" keys="${com.verecloud.nimbus4.service.enums.ChargeRateType.values()*.name()}" required="" value="${chargingRecordInstance?.rateType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="chargingRecord.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.ChargingRecordStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.ChargingRecordStatus.values()*.name()}" required="" value="${chargingRecordInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingRecordInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="chargingRecord.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #billingResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #billingResizable{position: relative}
                     #billingEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                billingEditor = ace.edit("billingEditor");
                                var billing = $('textarea[name="billing"]').hide();
                                billingEditor.session.setMode("ace/mode/json");
                                billingEditor.setTheme("ace/theme/tomorrow");
                                billingEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('billingEditor').style.fontSize='15px'
                                billingEditor.setReadOnly(false)
                                billingEditor.getSession().on('change', function(){
                                    billing.val(billingEditor.getSession().getValue());
                                });
                                 jq("#billingResizable").resizable({
                                    resize: function( event, ui ) {
                                    billingEditor.resize();
                                  }
                                 });

            
                    });
                </script>

