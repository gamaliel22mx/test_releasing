<%@ page import="com.verecloud.nimbus4.billing.ChargingRecord" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'chargingRecord.label', default: 'ChargingRecord')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-chargingRecord" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="amount" title="${message(code: 'chargingRecord.amount.label', default: 'Amount')}"/>
				
					<g:sortableColumn property="chargeCode" title="${message(code: 'chargingRecord.chargeCode.label', default: 'Charge Code')}"/>
				
					<g:sortableColumn property="chargeDescription" title="${message(code: 'chargingRecord.chargeDescription.label', default: 'Charge Description')}"/>
				
					<g:sortableColumn property="chargeMode" title="${message(code: 'chargingRecord.chargeMode.label', default: 'Charge Mode')}"/>
				
					<g:sortableColumn property="chargeType" title="${message(code: 'chargingRecord.chargeType.label', default: 'Charge Type')}"/>
				
					<g:sortableColumn property="currency" title="${message(code: 'chargingRecord.currency.label', default: 'Currency')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'chargingRecord.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="groupId" title="${message(code: 'chargingRecord.groupId.label', default: 'Group Id')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'chargingRecord.lastUpdated.label', default: 'Last Updated')}"/>
				
					<g:sortableColumn property="meteringDate" title="${message(code: 'chargingRecord.meteringDate.label', default: 'Metering Date')}"/>
				
					<g:sortableColumn property="productCode" title="${message(code: 'chargingRecord.productCode.label', default: 'Product Code')}"/>
				
					<g:sortableColumn property="rateType" title="${message(code: 'chargingRecord.rateType.label', default: 'Rate Type')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'chargingRecord.status.label', default: 'Status')}"/>
				
				<th><g:message code="chargingRecord.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${chargingRecordInstanceList}" status="i" var="chargingRecordInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${chargingRecordInstance.id}">${chargingRecordInstance}</g:link></td>
					
					<td>
						
                        <g:formatNumber number="${chargingRecordInstance?.amount}" type="number" maxFractionDigits="10" />
						
					</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "chargeCode")}</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "chargeDescription")}</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "chargeMode")}</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "chargeType")}</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "currency")}</td>
					
					<td><g:formatDate date="${chargingRecordInstance.dateCreated}"/></td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "groupId")}</td>
					
					<td><g:formatDate date="${chargingRecordInstance.lastUpdated}"/></td>
					
					<td><g:formatDate date="${chargingRecordInstance.meteringDate}"/></td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "productCode")}</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "rateType")}</td>
					
					<td>${fieldValue(bean: chargingRecordInstance, field: "status")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${chargingRecordInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(chargingRecordInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${chargingRecordInstanceCount}"/>
	</div>
</section>

</body>

</html>
