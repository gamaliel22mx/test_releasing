<%@ page import="com.verecloud.nimbus4.billing.ChargingLineItemGroup" %>


<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'productInstance', 'error')} required">
	<label for="productInstance" class="control-label"><g:message code="chargingLineItemGroup.productInstance.label" default="Product Instance"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="productInstance" name="productInstance.id" from="${com.verecloud.nimbus4.product.ProductInstance.findAllByTenantId(nimbus.findTenantId().toLong())}" optionKey="id" required="" value="${chargingLineItemGroupInstance?.productInstance?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'billingFrequency', 'error')} ">
	<label for="billingFrequency" class="control-label"><g:message code="chargingLineItemGroup.billingFrequency.label" default="Billing Frequency"/> </label>
	<div>
		
			<g:textField class="form-control" name="billingFrequency" value="${chargingLineItemGroupInstance?.billingFrequency}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'chargeMode', 'error')} required">
	<label for="chargeMode" class="control-label"><g:message code="chargingLineItemGroup.chargeMode.label" default="Charge Mode"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="chargeMode" from="${com.verecloud.nimbus4.service.enums.ChargeMode?.values()}" keys="${com.verecloud.nimbus4.service.enums.ChargeMode.values()*.name()}" required="" value="${chargingLineItemGroupInstance?.chargeMode?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'chargingLineItems', 'error')} ">
	<label for="chargingLineItems" class="control-label"><g:message code="chargingLineItemGroup.chargingLineItems.label" default="Charging Line Items"/> </label>
	<div>
		
			
<ul class="one-to-many">
			<g:each in="${chargingLineItemGroupInstance?.chargingLineItems}" var="c">
			    <g:if test="${c}">
                    <li>
                        <g:if test="${c?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
                                <g:link
                                    controller="${c?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.value?.class?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
                                <g:link
                                    controller="${c?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.class?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'chargingLineItem').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['chargingLineItemGroup.id': chargingLineItemGroupInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'endDate', 'error')} required">
	<label for="endDate" class="control-label"><g:message code="chargingLineItemGroup.endDate.label" default="End Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="endDate" precision="day"  value="${chargingLineItemGroupInstance?.endDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'startDate', 'error')} required">
	<label for="startDate" class="control-label"><g:message code="chargingLineItemGroup.startDate.label" default="Start Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="startDate" precision="day"  value="${chargingLineItemGroupInstance?.startDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: chargingLineItemGroupInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="chargingLineItemGroup.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

