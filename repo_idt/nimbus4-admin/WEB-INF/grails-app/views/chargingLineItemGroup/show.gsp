<%@ page import="com.verecloud.nimbus4.billing.ChargingLineItemGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'chargingLineItemGroup.label', default: 'ChargingLineItemGroup')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-chargingLineItemGroup" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${chargingLineItemGroupInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(chargingLineItemGroupInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemGroupInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemGroupInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.productInstance.label" default="Product Instance" /></td>
			
			<td valign="top" class="value"><g:link controller="${chargingLineItemGroupInstance?.productInstance?.class?.simpleName}" action="show" id="${chargingLineItemGroupInstance?.productInstance?.id}">${chargingLineItemGroupInstance?.productInstance?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.billingFrequency.label" default="Billing Frequency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: chargingLineItemGroupInstance, field: "billingFrequency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.chargeMode.label" default="Charge Mode" /></td>
			
			<td valign="top" class="value">${chargingLineItemGroupInstance?.chargeMode?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.chargingLineItems.label" default="Charging Line Items" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${chargingLineItemGroupInstance?.chargingLineItems}" var="c">
	<g:if test="${c}">
		<li>
			<g:if test="${c?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
					<g:link
							controller="${c?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.value?.class?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
					<g:link
							controller="${c?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.class?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingLineItemGroupInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.endDate.label" default="End Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingLineItemGroupInstance?.endDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingLineItemGroupInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="chargingLineItemGroup.startDate.label" default="Start Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${chargingLineItemGroupInstance?.startDate}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
