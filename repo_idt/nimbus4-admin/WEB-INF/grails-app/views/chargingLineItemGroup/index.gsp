<%@ page import="com.verecloud.nimbus4.billing.ChargingLineItemGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'chargingLineItemGroup.label', default: 'ChargingLineItemGroup')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-chargingLineItemGroup" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'chargingLineItemGroup.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'chargingLineItemGroup.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="chargingLineItemGroup.productInstance.label" default="Product Instance"/></th>
				
					<g:sortableColumn property="billingFrequency" title="${message(code: 'chargingLineItemGroup.billingFrequency.label', default: 'Billing Frequency')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="chargeMode" title="${message(code: 'chargingLineItemGroup.chargeMode.label', default: 'Charge Mode')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'chargingLineItemGroup.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="endDate" title="${message(code: 'chargingLineItemGroup.endDate.label', default: 'End Date')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'chargingLineItemGroup.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="startDate" title="${message(code: 'chargingLineItemGroup.startDate.label', default: 'Start Date')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="chargingLineItemGroup.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${chargingLineItemGroupInstanceList}" status="i" var="chargingLineItemGroupInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${chargingLineItemGroupInstance.id}">${chargingLineItemGroupInstance}</g:link></td>
					
					<td>${fieldValue(bean: chargingLineItemGroupInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: chargingLineItemGroupInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(chargingLineItemGroupInstance?.productInstance)?.class?.simpleName}" action="show" id="${chargingLineItemGroupInstance?.productInstance?.id}">${chargingLineItemGroupInstance?.productInstance?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: chargingLineItemGroupInstance, field: "billingFrequency")}</td>
					
					<td>${fieldValue(bean: chargingLineItemGroupInstance, field: "chargeMode")}</td>
					
					<td><g:formatDate date="${chargingLineItemGroupInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${chargingLineItemGroupInstance.endDate}"/></td>
					
					<td><g:formatDate date="${chargingLineItemGroupInstance.lastUpdated}"/></td>
					
					<td><g:formatDate date="${chargingLineItemGroupInstance.startDate}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${chargingLineItemGroupInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(chargingLineItemGroupInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${chargingLineItemGroupInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
