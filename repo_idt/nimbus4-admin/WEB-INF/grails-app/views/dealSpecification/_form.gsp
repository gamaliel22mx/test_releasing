<%@ page import="com.verecloud.nimbus4.deal.DealSpecification" %>


<div class="${hasErrors(bean: dealSpecificationInstance, field: 'schema', 'error')} ">
	<label for="schema" class="control-label"><g:message code="dealSpecification.schema.label" default="Schema"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="schema" value="${(dealSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(dealSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: dealSpecificationInstance, field: 'emailNotes', 'error')} ">
	<label for="emailNotes" class="control-label"><g:message code="dealSpecification.emailNotes.label" default="Email Notes"/> </label>
	<div>
		
			<g:textField class="form-control" name="emailNotes" value="${dealSpecificationInstance?.emailNotes}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: dealSpecificationInstance, field: 'thirdPartyEmail', 'error')} ">
	<label for="thirdPartyEmail" class="control-label"><g:message code="dealSpecification.thirdPartyEmail.label" default="Third Party Email"/> </label>
	<div>
		
			<g:field class="form-control" type="email" name="thirdPartyEmail" value="${dealSpecificationInstance?.thirdPartyEmail}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: dealSpecificationInstance, field: 'distributorEmail', 'error')} ">
	<label for="distributorEmail" class="control-label"><g:message code="dealSpecification.distributorEmail.label" default="Distributor Email"/> </label>
	<div>
		
			<g:field class="form-control" type="email" name="distributorEmail" value="${dealSpecificationInstance?.distributorEmail}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: dealSpecificationInstance, field: 'logo', 'error')} ">
	<label for="logo" class="control-label"><g:message code="dealSpecification.logo.label" default="Logo"/> </label>
	<div>
		
			<g:textField class="form-control" name="logo" value="${dealSpecificationInstance?.logo}"/>
		
	</div>
</div>
<g:set var="timePeriodInstance" value="${dealSpecificationInstance?.activeFor}"/>
<fieldset class="embedded"><legend><g:message code="dealSpecification.activeFor.label" default="Active For"/></legend>
	
<div class="${hasErrors(bean: dealSpecificationInstance, field: 'activeFor.endDate', 'error')} ">
	<label for="activeFor.endDate" class="control-label"><g:message code="dealSpecification.activeFor.endDate.label" default="End Date"/> </label>
	<div>
		
			<bs:datePicker name="activeFor.endDate" precision="day"  value="${timePeriodInstance?.endDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: dealSpecificationInstance, field: 'activeFor.startDate', 'error')} required">
	<label for="activeFor.startDate" class="control-label"><g:message code="dealSpecification.activeFor.startDate.label" default="Start Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="activeFor.startDate" precision="day"  value="${timePeriodInstance?.startDate}"  />
		
	</div>
</div>
</fieldset>
<div class="${hasErrors(bean: dealSpecificationInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="dealSpecification.name.label" default="Name"/> </label>
	<div>
		
			<g:textField class="form-control" name="name" value="${dealSpecificationInstance?.name}"/>
		
	</div>
</div>

<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(false)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>

