<%@ page import="com.verecloud.nimbus4.deal.DealSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'dealSpecification.label', default: 'DealSpecification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-dealSpecification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="emailNotes" title="${message(code: 'dealSpecification.emailNotes.label', default: 'Email Notes')}"/>
				
					<g:sortableColumn property="thirdPartyEmail" title="${message(code: 'dealSpecification.thirdPartyEmail.label', default: 'Third Party Email')}"/>
				
					<g:sortableColumn property="distributorEmail" title="${message(code: 'dealSpecification.distributorEmail.label', default: 'Distributor Email')}"/>
				
					<g:sortableColumn property="logo" title="${message(code: 'dealSpecification.logo.label', default: 'Logo')}"/>
				
				<th><g:message code="dealSpecification.activeFor.label" default="Active For"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'dealSpecification.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'dealSpecification.lastUpdated.label', default: 'Last Updated')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'dealSpecification.name.label', default: 'Name')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${dealSpecificationInstanceList}" status="i" var="dealSpecificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${dealSpecificationInstance.id}">${dealSpecificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: dealSpecificationInstance, field: "emailNotes")}</td>
					
					<td>${fieldValue(bean: dealSpecificationInstance, field: "thirdPartyEmail")}</td>
					
					<td>${fieldValue(bean: dealSpecificationInstance, field: "distributorEmail")}</td>
					
					<td>${fieldValue(bean: dealSpecificationInstance, field: "logo")}</td>
					
					<td>${fieldValue(bean: dealSpecificationInstance, field: "activeFor")}</td>
					
					<td><g:formatDate date="${dealSpecificationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${dealSpecificationInstance.lastUpdated}"/></td>
					
					<td>${fieldValue(bean: dealSpecificationInstance, field: "name")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${dealSpecificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
