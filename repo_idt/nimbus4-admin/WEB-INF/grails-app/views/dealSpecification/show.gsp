<%@ page import="com.verecloud.nimbus4.deal.DealSpecification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'dealSpecification.label', default: 'DealSpecification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-dealSpecification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.schema.label" default="Schema" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="schema" value="${(dealSpecificationInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(dealSpecificationInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.emailNotes.label" default="Email Notes" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: dealSpecificationInstance, field: "emailNotes")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.thirdPartyEmail.label" default="Third Party Email" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: dealSpecificationInstance, field: "thirdPartyEmail")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.distributorEmail.label" default="Distributor Email" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: dealSpecificationInstance, field: "distributorEmail")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.logo.label" default="Logo" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: dealSpecificationInstance, field: "logo")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.activeFor.label" default="Active For" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: dealSpecificationInstance, field: "activeFor")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${dealSpecificationInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${dealSpecificationInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="dealSpecification.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: dealSpecificationInstance, field: "name")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(true)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
