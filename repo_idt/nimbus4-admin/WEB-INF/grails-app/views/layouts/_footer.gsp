<shiro:isLoggedIn>
    <footer class="footer">
        <div class="container">
            <h4>Disclaimer</h4>

            <p>This Web site may contain other proprietary notices and copyright information, the terms of which must be observed and followed.
            Information on this Web site may contain technical inaccuracies or typographical errors.
            Information may be changed or updated without notice.
            </p>

            <p>Verecloud &#169; 2014
            </p>

            <p class="pull-right"><a href="#">Back to top</a></p>
        </div>

    </footer>
</shiro:isLoggedIn>

<shiro:isNotLoggedIn>
    <footer class="footer" style="position: fixed;bottom: 0px; width: 100%; padding:22px 0px;">
        <div class="container">
            <span>Verecloud &#169; 2014
            </span>
            <span class="pull-right">@Powered by <a href="http://www.intelligrape.com" target="_blank">
                <img src="${g.resource(dir: "images", file: "intelligrapeLogo.png")}"
                     style="width: 130px !important;"/>
            </a>
            </span>

        </div>

    </footer>
</shiro:isNotLoggedIn>