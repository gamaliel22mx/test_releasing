<shiro:isLoggedIn>
    <header id="Header" class="page-header" style="
    margin-top: 4%;">
        <div class="container">
            <h1 class="title"><g:layoutTitle default="${meta(name: 'app.name')}"/></h1>
        </div>
    </header>
</shiro:isLoggedIn>
