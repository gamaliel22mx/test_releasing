<%@ page import="com.verecloud.nimbus4.party.Role" %>


<div class="${hasErrors(bean: roleInstance, field: 'displayName', 'error')} ">
	<label for="displayName" class="control-label"><g:message code="role.displayName.label" default="Display Name"/> </label>
	<div>
		
			<g:textField class="form-control" name="displayName" value="${roleInstance?.displayName}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: roleInstance, field: 'title', 'error')} required">
	<label for="title" class="control-label"><g:message code="role.title.label" default="Title"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="title" required="" value="${roleInstance?.title}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: roleInstance, field: 'permissions', 'error')} ">
	<label for="permissions" class="control-label"><g:message code="role.permissions.label" default="Permissions"/> </label>
	<div>
		
			<g:textArea class="form-control" name="permissions" cols="40" rows="5" value="${roleInstance?.permissions?.join("\n")}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: roleInstance, field: 'userGroupRoles', 'error')} ">
	<label for="userGroupRoles" class="control-label"><g:message code="role.userGroupRoles.label" default="User Group Roles"/> </label>
	<div>
		
			
		
	</div>
</div>

