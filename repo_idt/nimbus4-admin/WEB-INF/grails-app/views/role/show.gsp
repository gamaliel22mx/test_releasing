<%@ page import="com.verecloud.nimbus4.party.Role" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-role" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="role.displayName.label" default="Display Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: roleInstance, field: "displayName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="role.title.label" default="Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: roleInstance, field: "title")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="role.permissions.label" default="Permissions" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: roleInstance, field: "permissions")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="role.userGroupRoles.label" default="User Group Roles" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${roleInstance?.userGroupRoles}" var="u">
	<g:if test="${u}">
		<li>
			<g:if test="${u?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(u?.value?.class)}">
					<g:link
							controller="${u?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${u?.value?.id}">
						${u?.key?.encodeAsHTML()+" : "+u?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${u?.value?.class?.simpleName}"
							action="show"
							id="${u?.value?.id}">
						${u?.key?.encodeAsHTML()+" : "+u?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(u?.class)}">
					<g:link
							controller="${u?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${u?.id}">
						${u?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${u?.class?.simpleName}"
							action="show"
							id="${u?.id}">
						${u?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
