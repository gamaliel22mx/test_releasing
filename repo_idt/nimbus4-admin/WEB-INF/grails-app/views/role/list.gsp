<%@ page import="com.verecloud.nimbus4.party.Role" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-role" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="displayName" title="${message(code: 'role.displayName.label', default: 'Display Name')}"/>
				
					<g:sortableColumn property="title" title="${message(code: 'role.title.label', default: 'Title')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${roleInstanceList}" status="i" var="roleInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${roleInstance.id}">${roleInstance}</g:link></td>
					
					<td>${fieldValue(bean: roleInstance, field: "displayName")}</td>
					
					<td>${fieldValue(bean: roleInstance, field: "title")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${roleInstanceCount}"/>
	</div>
</section>

</body>

</html>
