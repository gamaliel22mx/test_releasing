<%@ page import="com.verecloud.nimbus4.party.GroupEventHandler" %>


<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="groupEventHandler.name.label" default="Name"/> </label>
	<div>
		
			<g:textField class="form-control" name="name" value="${groupEventHandlerInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'forDomainName', 'error')} ">
	<label for="forDomainName" class="control-label"><g:message code="groupEventHandler.forDomainName.label" default="For Domain Name"/> </label>
	<div>
		
			<g:select class="form-control" name="forDomainName" from="${groupEventHandlerInstance.constraints.forDomainName.inList}" value="${groupEventHandlerInstance?.forDomainName}" valueMessagePrefix="groupEventHandler.forDomainName" noSelection="['': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'eventAction', 'error')} ">
	<label for="eventAction" class="control-label"><g:message code="groupEventHandler.eventAction.label" default="Event Action"/> </label>
	<div>
		
			<g:select class="form-control" name="eventAction" from="${groupEventHandlerInstance.constraints.eventAction.inList}" value="${groupEventHandlerInstance?.eventAction}" valueMessagePrefix="groupEventHandler.eventAction" noSelection="['': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'onUpdateInspectValueInField', 'error')} ">
	<label for="onUpdateInspectValueInField" class="control-label"><g:message code="groupEventHandler.onUpdateInspectValueInField.label" default="On Update Inspect Value In Field"/> </label>
	<div>
		
			<g:textField class="form-control" name="onUpdateInspectValueInField" value="${groupEventHandlerInstance?.onUpdateInspectValueInField}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'beforeUpdateFieldValue', 'error')} ">
	<label for="beforeUpdateFieldValue" class="control-label"><g:message code="groupEventHandler.beforeUpdateFieldValue.label" default="Before Update Field Value"/> </label>
	<div>
		
			<g:textField class="form-control" name="beforeUpdateFieldValue" value="${groupEventHandlerInstance?.beforeUpdateFieldValue}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'afterUpdateFieldValue', 'error')} ">
	<label for="afterUpdateFieldValue" class="control-label"><g:message code="groupEventHandler.afterUpdateFieldValue.label" default="After Update Field Value"/> </label>
	<div>
		
			<g:textField class="form-control" name="afterUpdateFieldValue" value="${groupEventHandlerInstance?.afterUpdateFieldValue}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="groupEventHandler.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${groupEventHandlerInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'groovyScript', 'error')} ">
	<label for="groovyScript" class="control-label"><g:message code="groupEventHandler.groovyScript.label" default="Groovy Script"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="groovyScript" value="${groupEventHandlerInstance?.groovyScript}" />
            <div id="groovyScriptResizable"><div id="groovyScriptEditor">${groupEventHandlerInstance?.groovyScript}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: groupEventHandlerInstance, field: 'groupSpecification', 'error')} required">
	<label for="groupSpecification" class="control-label"><g:message code="groupEventHandler.groupSpecification.label" default="Group Specification"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="groupSpecification" name="groupSpecification.id" from="${com.verecloud.nimbus4.party.GroupIntegrationSpecification.list()}" optionKey="id" required="" value="${groupEventHandlerInstance?.groupSpecification?.id}" class="many-to-one"/>
		
	</div>
</div>

<style>
                     #groovyScriptResizable { width: 1100px; height: 800px; padding: 5px; border: 1px solid #aedeae}
                     #groovyScriptResizable{position: relative}
                     #groovyScriptEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                groovyScriptEditor = ace.edit("groovyScriptEditor");
                                var groovyScript = $('textarea[name="groovyScript"]').hide();
                                groovyScriptEditor.session.setMode("ace/mode/groovy");
                                groovyScriptEditor.setTheme("ace/theme/tomorrow");
                                groovyScriptEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('groovyScriptEditor').style.fontSize='15px'
                                groovyScriptEditor.setReadOnly(false)
                                groovyScriptEditor.getSession().on('change', function(){
                                    groovyScript.val(groovyScriptEditor.getSession().getValue());
                                });
                                 jq("#groovyScriptResizable").resizable({
                                    resize: function( event, ui ) {
                                    groovyScriptEditor.resize();
                                  }
                                 });

            
                    });
                </script>

