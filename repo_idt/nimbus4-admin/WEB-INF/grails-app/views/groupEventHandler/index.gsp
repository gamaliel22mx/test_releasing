<%@ page import="com.verecloud.nimbus4.party.GroupEventHandler" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupEventHandler.label', default: 'GroupEventHandler')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-groupEventHandler" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'groupEventHandler.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'groupEventHandler.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'groupEventHandler.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="forDomainName" title="${message(code: 'groupEventHandler.forDomainName.label', default: 'For Domain Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="eventAction" title="${message(code: 'groupEventHandler.eventAction.label', default: 'Event Action')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="onUpdateInspectValueInField" title="${message(code: 'groupEventHandler.onUpdateInspectValueInField.label', default: 'On Update Inspect Value In Field')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="beforeUpdateFieldValue" title="${message(code: 'groupEventHandler.beforeUpdateFieldValue.label', default: 'Before Update Field Value')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="afterUpdateFieldValue" title="${message(code: 'groupEventHandler.afterUpdateFieldValue.label', default: 'After Update Field Value')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="description" title="${message(code: 'groupEventHandler.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'groupEventHandler.groovyScript.label', default: 'Groovy Script')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'groupEventHandler.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="groupEventHandler.groupSpecification.label" default="Group Specification"/></th>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'groupEventHandler.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupEventHandlerInstanceList}" status="i" var="groupEventHandlerInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupEventHandlerInstance.id}">${groupEventHandlerInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "forDomainName")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "eventAction")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "onUpdateInspectValueInField")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "beforeUpdateFieldValue")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "afterUpdateFieldValue")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: groupEventHandlerInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${groupEventHandlerInstance.dateCreated}"/></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupEventHandlerInstance?.groupSpecification)?.class?.simpleName}" action="show" id="${groupEventHandlerInstance?.groupSpecification?.id}">${groupEventHandlerInstance?.groupSpecification?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${groupEventHandlerInstance.lastUpdated}"/></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupEventHandlerInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
