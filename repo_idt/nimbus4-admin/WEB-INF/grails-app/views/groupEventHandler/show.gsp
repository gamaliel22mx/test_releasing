<%@ page import="com.verecloud.nimbus4.party.GroupEventHandler" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupEventHandler.label', default: 'GroupEventHandler')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-groupEventHandler" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.forDomainName.label" default="For Domain Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "forDomainName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.eventAction.label" default="Event Action" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "eventAction")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.onUpdateInspectValueInField.label" default="On Update Inspect Value In Field" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "onUpdateInspectValueInField")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.beforeUpdateFieldValue.label" default="Before Update Field Value" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "beforeUpdateFieldValue")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.afterUpdateFieldValue.label" default="After Update Field Value" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "afterUpdateFieldValue")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupEventHandlerInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.groovyScript.label" default="Groovy Script" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="groovyScript" value="${groupEventHandlerInstance?.groovyScript}" />
            <div id="groovyScriptResizable"><div id="groovyScriptEditor">${groupEventHandlerInstance?.groovyScript}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupEventHandlerInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.groupSpecification.label" default="Group Specification" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupEventHandlerInstance?.groupSpecification?.class?.simpleName}" action="show" id="${groupEventHandlerInstance?.groupSpecification?.id}">${groupEventHandlerInstance?.groupSpecification?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupEventHandler.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupEventHandlerInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #groovyScriptResizable { width: 1100px; height: 800px; padding: 5px; border: 1px solid #aedeae}
                     #groovyScriptResizable{position: relative}
                     #groovyScriptEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                groovyScriptEditor = ace.edit("groovyScriptEditor");
                                var groovyScript = $('textarea[name="groovyScript"]').hide();
                                groovyScriptEditor.session.setMode("ace/mode/groovy");
                                groovyScriptEditor.setTheme("ace/theme/tomorrow");
                                groovyScriptEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('groovyScriptEditor').style.fontSize='15px'
                                groovyScriptEditor.setReadOnly(true)
                                groovyScriptEditor.getSession().on('change', function(){
                                    groovyScript.val(groovyScriptEditor.getSession().getValue());
                                });
                                 jq("#groovyScriptResizable").resizable({
                                    resize: function( event, ui ) {
                                    groovyScriptEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
