<%@ page import="com.verecloud.nimbus4.documentation.Documentation" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'documentation.label', default: 'Documentation')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-documentation" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${documentationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(documentationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: documentationInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.teaser.label" default="Teaser" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: documentationInstance, field: "teaser")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.url.label" default="Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: documentationInstance, field: "url")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.product.label" default="Product" /></td>
			
			<td valign="top" class="value"><g:link controller="${documentationInstance?.product?.class?.simpleName}" action="show" id="${documentationInstance?.product?.id}">${documentationInstance?.product?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.title.label" default="Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: documentationInstance, field: "title")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="documentation.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${documentationInstance?.type?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
