<%@ page import="com.verecloud.nimbus4.documentation.Documentation" %>


<div class="${hasErrors(bean: documentationInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="documentation.description.label" default="Description"/> </label>
	<div>
		
			<g:textArea class="form-control" name="description" cols="40" rows="5" value="${documentationInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: documentationInstance, field: 'teaser', 'error')} required">
	<label for="teaser" class="control-label"><g:message code="documentation.teaser.label" default="Teaser"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="teaser" required="" value="${documentationInstance?.teaser}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: documentationInstance, field: 'url', 'error')} required">
	<label for="url" class="control-label"><g:message code="documentation.url.label" default="Url"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" type="url" name="url" required="" value="${documentationInstance?.url}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: documentationInstance, field: 'product', 'error')} ">
	<label for="product" class="control-label"><g:message code="documentation.product.label" default="Product"/> </label>
	<div>
		
			<g:select class="form-control" id="product" name="product.id" from="${com.verecloud.nimbus4.product.ProductSpecification.list()}" optionKey="id" value="${documentationInstance?.product?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: documentationInstance, field: 'title', 'error')} required">
	<label for="title" class="control-label"><g:message code="documentation.title.label" default="Title"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="title" required="" value="${documentationInstance?.title}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: documentationInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="documentation.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<div class="${hasErrors(bean: documentationInstance, field: 'type', 'error')} required">
	<label for="type" class="control-label"><g:message code="documentation.type.label" default="Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="type" from="${com.verecloud.nimbus4.documentation.enums.DocumentationType?.values()}" keys="${com.verecloud.nimbus4.documentation.enums.DocumentationType.values()*.name()}" required="" value="${documentationInstance?.type?.name()}"/>
		
	</div>
</div>

