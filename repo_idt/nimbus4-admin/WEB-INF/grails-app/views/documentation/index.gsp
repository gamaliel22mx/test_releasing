<%@ page import="com.verecloud.nimbus4.documentation.Documentation" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'documentation.label', default: 'Documentation')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-documentation" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="description" title="${message(code: 'documentation.description.label', default: 'Description')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="teaser" title="${message(code: 'documentation.teaser.label', default: 'Teaser')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="url" title="${message(code: 'documentation.url.label', default: 'Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="documentation.product.label" default="Product"/></th>
				
					<g:sortableColumn property="title" title="${message(code: 'documentation.title.label', default: 'Title')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="type" title="${message(code: 'documentation.type.label', default: 'Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="documentation.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${documentationInstanceList}" status="i" var="documentationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${documentationInstance.id}">${documentationInstance}</g:link></td>
					
					<td>${fieldValue(bean: documentationInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: documentationInstance, field: "teaser")}</td>
					
					<td>${fieldValue(bean: documentationInstance, field: "url")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(documentationInstance?.product)?.class?.simpleName}" action="show" id="${documentationInstance?.product?.id}">${documentationInstance?.product?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: documentationInstance, field: "title")}</td>
					
					<td>${fieldValue(bean: documentationInstance, field: "type")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${documentationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(documentationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${documentationInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
