<%@ page import="com.verecloud.nimbus4.product.BillingOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'billingOrder.label', default: 'BillingOrder')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-billingOrder" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.activeSince.label" default="Active Since" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${billingOrderInstance?.activeSince}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.activeUntil.label" default="Active Until" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${billingOrderInstance?.activeUntil}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.billingType.label" default="Billing Type" /></td>
			
			<td valign="top" class="value">${billingOrderInstance?.billingType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.currencyId.label" default="Currency Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: billingOrderInstance, field: "currencyId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.cycleStarts.label" default="Cycle Starts" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${billingOrderInstance?.cycleStarts}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.nextBillableDay.label" default="Next Billable Day" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${billingOrderInstance?.nextBillableDay}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.notify.label" default="Notify" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${billingOrderInstance?.notify}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.periodType.label" default="Period Type" /></td>
			
			<td valign="top" class="value">${billingOrderInstance?.periodType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="billingOrder.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${billingOrderInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
