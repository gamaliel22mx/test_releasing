<%@ page import="com.verecloud.nimbus4.product.BillingOrder" %>


<div class="${hasErrors(bean: billingOrderInstance, field: 'activeSince', 'error')} required">
	<label for="activeSince" class="control-label"><g:message code="billingOrder.activeSince.label" default="Active Since"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="activeSince" precision="day"  value="${billingOrderInstance?.activeSince}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'activeUntil', 'error')} required">
	<label for="activeUntil" class="control-label"><g:message code="billingOrder.activeUntil.label" default="Active Until"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="activeUntil" precision="day"  value="${billingOrderInstance?.activeUntil}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'billingType', 'error')} required">
	<label for="billingType" class="control-label"><g:message code="billingOrder.billingType.label" default="Billing Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="billingType" from="${com.verecloud.nimbus4.product.BillingOrder$BillingType?.values()}" keys="${com.verecloud.nimbus4.product.BillingOrder$BillingType.values()*.name()}" required="" value="${billingOrderInstance?.billingType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'currencyId', 'error')} required">
	<label for="currencyId" class="control-label"><g:message code="billingOrder.currencyId.label" default="Currency Id"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="currencyId" type="number" value="${billingOrderInstance.currencyId}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'cycleStarts', 'error')} required">
	<label for="cycleStarts" class="control-label"><g:message code="billingOrder.cycleStarts.label" default="Cycle Starts"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="cycleStarts" precision="day"  value="${billingOrderInstance?.cycleStarts}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'nextBillableDay', 'error')} required">
	<label for="nextBillableDay" class="control-label"><g:message code="billingOrder.nextBillableDay.label" default="Next Billable Day"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="nextBillableDay" precision="day"  value="${billingOrderInstance?.nextBillableDay}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'notify', 'error')} ">
	<label for="notify" class="control-label"><g:message code="billingOrder.notify.label" default="Notify"/> </label>
	<div>
		
			<nimbus:checkBox name="notify" value="${billingOrderInstance?.notify}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'periodType', 'error')} required">
	<label for="periodType" class="control-label"><g:message code="billingOrder.periodType.label" default="Period Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="periodType" from="${com.verecloud.nimbus4.product.BillingOrder$OrderPeriodType?.values()}" keys="${com.verecloud.nimbus4.product.BillingOrder$OrderPeriodType.values()*.name()}" required="" value="${billingOrderInstance?.periodType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: billingOrderInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="billingOrder.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.product.BillingOrder$OrderStatus?.values()}" keys="${com.verecloud.nimbus4.product.BillingOrder$OrderStatus.values()*.name()}" required="" value="${billingOrderInstance?.status?.name()}"/>
		
	</div>
</div>

