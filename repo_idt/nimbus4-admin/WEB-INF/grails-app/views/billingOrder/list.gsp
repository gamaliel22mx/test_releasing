<%@ page import="com.verecloud.nimbus4.product.BillingOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'billingOrder.label', default: 'BillingOrder')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-billingOrder" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="activeSince" title="${message(code: 'billingOrder.activeSince.label', default: 'Active Since')}"/>
				
					<g:sortableColumn property="activeUntil" title="${message(code: 'billingOrder.activeUntil.label', default: 'Active Until')}"/>
				
					<g:sortableColumn property="billingType" title="${message(code: 'billingOrder.billingType.label', default: 'Billing Type')}"/>
				
					<g:sortableColumn property="currencyId" title="${message(code: 'billingOrder.currencyId.label', default: 'Currency Id')}"/>
				
					<g:sortableColumn property="cycleStarts" title="${message(code: 'billingOrder.cycleStarts.label', default: 'Cycle Starts')}"/>
				
					<g:sortableColumn property="nextBillableDay" title="${message(code: 'billingOrder.nextBillableDay.label', default: 'Next Billable Day')}"/>
				
					<g:sortableColumn property="notify" title="${message(code: 'billingOrder.notify.label', default: 'Notify')}"/>
				
					<g:sortableColumn property="periodType" title="${message(code: 'billingOrder.periodType.label', default: 'Period Type')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'billingOrder.status.label', default: 'Status')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${billingOrderInstanceList}" status="i" var="billingOrderInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${billingOrderInstance.id}">${billingOrderInstance}</g:link></td>
					
					<td><g:formatDate date="${billingOrderInstance.activeSince}"/></td>
					
					<td><g:formatDate date="${billingOrderInstance.activeUntil}"/></td>
					
					<td>${fieldValue(bean: billingOrderInstance, field: "billingType")}</td>
					
					<td>${fieldValue(bean: billingOrderInstance, field: "currencyId")}</td>
					
					<td><g:formatDate date="${billingOrderInstance.cycleStarts}"/></td>
					
					<td><g:formatDate date="${billingOrderInstance.nextBillableDay}"/></td>
					
					<td><g:formatBoolean boolean="${billingOrderInstance.notify}"/></td>
							
					<td>${fieldValue(bean: billingOrderInstance, field: "periodType")}</td>
					
					<td>${fieldValue(bean: billingOrderInstance, field: "status")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${billingOrderInstanceCount}"/>
	</div>
</section>

</body>

</html>
