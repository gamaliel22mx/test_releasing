<%@ page import="com.verecloud.nimbus4.product.BillingOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'billingOrder.label', default: 'BillingOrder')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-billingOrder" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="activeSince" title="${message(code: 'billingOrder.activeSince.label', default: 'Active Since')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="activeUntil" title="${message(code: 'billingOrder.activeUntil.label', default: 'Active Until')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="billingType" title="${message(code: 'billingOrder.billingType.label', default: 'Billing Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="currencyId" title="${message(code: 'billingOrder.currencyId.label', default: 'Currency Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="cycleStarts" title="${message(code: 'billingOrder.cycleStarts.label', default: 'Cycle Starts')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="nextBillableDay" title="${message(code: 'billingOrder.nextBillableDay.label', default: 'Next Billable Day')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="notify" title="${message(code: 'billingOrder.notify.label', default: 'Notify')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="periodType" title="${message(code: 'billingOrder.periodType.label', default: 'Period Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="status" title="${message(code: 'billingOrder.status.label', default: 'Status')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${billingOrderInstanceList}" status="i" var="billingOrderInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${billingOrderInstance.id}">${billingOrderInstance}</g:link></td>
					
					<td><g:formatDate date="${billingOrderInstance.activeSince}"/></td>
					
					<td><g:formatDate date="${billingOrderInstance.activeUntil}"/></td>
					
					<td>${fieldValue(bean: billingOrderInstance, field: "billingType")}</td>
					
					<td>${fieldValue(bean: billingOrderInstance, field: "currencyId")}</td>
					
					<td><g:formatDate date="${billingOrderInstance.cycleStarts}"/></td>
					
					<td><g:formatDate date="${billingOrderInstance.nextBillableDay}"/></td>
					
					<td><g:formatBoolean boolean="${billingOrderInstance.notify}"/></td>
							
					<td>${fieldValue(bean: billingOrderInstance, field: "periodType")}</td>
					
					<td>${fieldValue(bean: billingOrderInstance, field: "status")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${billingOrderInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
