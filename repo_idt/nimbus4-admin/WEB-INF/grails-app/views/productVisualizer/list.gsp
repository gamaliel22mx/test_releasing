<%@ page import="com.verecloud.nimbus4.product.ProductVisualizer" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'productVisualizer.label', default: 'ProductVisualizer')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-productVisualizer" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'productVisualizer.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'productVisualizer.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'productVisualizer.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'productVisualizer.description.label', default: 'Description')}"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'productVisualizer.groovyScript.label', default: 'Groovy Script')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'productVisualizer.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'productVisualizer.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="productVisualizer.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${productVisualizerInstanceList}" status="i" var="productVisualizerInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${productVisualizerInstance.id}">${productVisualizerInstance}</g:link></td>
					
					<td>${fieldValue(bean: productVisualizerInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: productVisualizerInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: productVisualizerInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: productVisualizerInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: productVisualizerInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${productVisualizerInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${productVisualizerInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${productVisualizerInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(productVisualizerInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${productVisualizerInstanceCount}"/>
	</div>
</section>

</body>

</html>
