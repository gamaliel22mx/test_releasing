<%@ page import="com.verecloud.nimbus4.notification.Notification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'notification.label', default: 'Notification')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-notification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="message" title="${message(code: 'notification.message.label', default: 'Message')}"/>
				
					<g:sortableColumn property="url" title="${message(code: 'notification.url.label', default: 'Url')}"/>
				
				<th><g:message code="notification.userGroupRole.label" default="User Group Role"/></th>
				
					<g:sortableColumn property="acknowledged" title="${message(code: 'notification.acknowledged.label', default: 'Acknowledged')}"/>
				
					<g:sortableColumn property="date" title="${message(code: 'notification.date.label', default: 'Date')}"/>
				
					<g:sortableColumn property="type" title="${message(code: 'notification.type.label', default: 'Type')}"/>
				
				<th><g:message code="notification.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${notificationInstanceList}" status="i" var="notificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${notificationInstance.id}">${notificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: notificationInstance, field: "message")}</td>
					
					<td>${fieldValue(bean: notificationInstance, field: "url")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(notificationInstance?.userGroupRole)?.class?.simpleName}" action="show" id="${notificationInstance?.userGroupRole?.id}">${notificationInstance?.userGroupRole?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatBoolean boolean="${notificationInstance.acknowledged}"/></td>
							
					<td><g:formatDate date="${notificationInstance.date}"/></td>
					
					<td>${fieldValue(bean: notificationInstance, field: "type")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${notificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(notificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${notificationInstanceCount}"/>
	</div>
</section>

</body>

</html>
