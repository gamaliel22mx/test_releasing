<%@ page import="com.verecloud.nimbus4.notification.Notification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'notification.label', default: 'Notification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-notification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${notificationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(notificationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.message.label" default="Message" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: notificationInstance, field: "message")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.url.label" default="Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: notificationInstance, field: "url")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.userGroupRole.label" default="User Group Role" /></td>
			
			<td valign="top" class="value"><g:link controller="${notificationInstance?.userGroupRole?.class?.simpleName}" action="show" id="${notificationInstance?.userGroupRole?.id}">${notificationInstance?.userGroupRole?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.acknowledged.label" default="Acknowledged" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${notificationInstance?.acknowledged}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.date.label" default="Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${notificationInstance?.date}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="notification.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${notificationInstance?.type?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
