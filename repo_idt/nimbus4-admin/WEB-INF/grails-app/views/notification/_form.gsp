<%@ page import="com.verecloud.nimbus4.notification.Notification" %>


<div class="${hasErrors(bean: notificationInstance, field: 'message', 'error')} required">
	<label for="message" class="control-label"><g:message code="notification.message.label" default="Message"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="message" required="" value="${notificationInstance?.message}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: notificationInstance, field: 'url', 'error')} ">
	<label for="url" class="control-label"><g:message code="notification.url.label" default="Url"/> </label>
	<div>
		
			<g:textField class="form-control" name="url" value="${notificationInstance?.url}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: notificationInstance, field: 'userGroupRole', 'error')} required">
	<label for="userGroupRole" class="control-label"><g:message code="notification.userGroupRole.label" default="User Group Role"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="userGroupRole" name="userGroupRole.id" from="${com.verecloud.nimbus4.party.UserGroupRole.list()}" optionKey="id" required="" value="${notificationInstance?.userGroupRole?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: notificationInstance, field: 'acknowledged', 'error')} ">
	<label for="acknowledged" class="control-label"><g:message code="notification.acknowledged.label" default="Acknowledged"/> </label>
	<div>
		
			<nimbus:checkBox name="acknowledged" value="${notificationInstance?.acknowledged}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: notificationInstance, field: 'date', 'error')} required">
	<label for="date" class="control-label"><g:message code="notification.date.label" default="Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="date" precision="day"  value="${notificationInstance?.date}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: notificationInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="notification.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<div class="${hasErrors(bean: notificationInstance, field: 'type', 'error')} required">
	<label for="type" class="control-label"><g:message code="notification.type.label" default="Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="type" from="${com.verecloud.nimbus4.notification.enums.NotificationType?.values()}" keys="${com.verecloud.nimbus4.notification.enums.NotificationType.values()*.name()}" required="" value="${notificationInstance?.type?.name()}"/>
		
	</div>
</div>

