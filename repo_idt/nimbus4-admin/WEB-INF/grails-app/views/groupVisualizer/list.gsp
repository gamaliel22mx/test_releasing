<%@ page import="com.verecloud.nimbus4.party.GroupVisualizer" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupVisualizer.label', default: 'GroupVisualizer')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-groupVisualizer" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'groupVisualizer.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'groupVisualizer.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'groupVisualizer.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'groupVisualizer.description.label', default: 'Description')}"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'groupVisualizer.groovyScript.label', default: 'Groovy Script')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'groupVisualizer.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'groupVisualizer.lastUpdated.label', default: 'Last Updated')}"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupVisualizerInstanceList}" status="i" var="groupVisualizerInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupVisualizerInstance.id}">${groupVisualizerInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupVisualizerInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupVisualizerInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: groupVisualizerInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: groupVisualizerInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: groupVisualizerInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${groupVisualizerInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${groupVisualizerInstance.lastUpdated}"/></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupVisualizerInstanceCount}"/>
	</div>
</section>

</body>

</html>
