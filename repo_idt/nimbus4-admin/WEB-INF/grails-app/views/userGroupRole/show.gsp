<%@ page import="com.verecloud.nimbus4.party.UserGroupRole" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'userGroupRole.label', default: 'UserGroupRole')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-userGroupRole" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${userGroupRoleInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(userGroupRoleInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userGroupRoleInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userGroupRoleInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.user.label" default="User" /></td>
			
			<td valign="top" class="value"><g:link controller="${userGroupRoleInstance?.user?.class?.simpleName}" action="show" id="${userGroupRoleInstance?.user?.id}">${userGroupRoleInstance?.user?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.roles.label" default="Roles" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${userGroupRoleInstance?.roles}" var="r">
	<g:if test="${r}">
		<li>
			<g:if test="${r?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.value?.class)}">
					<g:link
							controller="${r?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${r?.value?.id}">
						${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${r?.value?.class?.simpleName}"
							action="show"
							id="${r?.value?.id}">
						${r?.key?.encodeAsHTML()+" : "+r?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(r?.class)}">
					<g:link
							controller="${r?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${r?.id}">
						${r?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${r?.class?.simpleName}"
							action="show"
							id="${r?.id}">
						${r?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userGroupRoleInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userGroupRole.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userGroupRoleInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
