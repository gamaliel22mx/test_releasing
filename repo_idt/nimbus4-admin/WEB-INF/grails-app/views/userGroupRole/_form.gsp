<%@ page import="com.verecloud.nimbus4.party.UserGroupRole" %>


<div class="${hasErrors(bean: userGroupRoleInstance, field: 'user', 'error')} required">
	<label for="user" class="control-label"><g:message code="userGroupRole.user.label" default="User"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="user" name="user.id" from="${com.verecloud.nimbus4.party.User.list()}" optionKey="id" required="" value="${userGroupRoleInstance?.user?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: userGroupRoleInstance, field: 'roles', 'error')} ">
	<label for="roles" class="control-label"><g:message code="userGroupRole.roles.label" default="Roles"/> </label>
	<div>
		
			<g:if test="${userGroupRoleInstance?.roles?.getClass() && Map.isAssignableFrom(userGroupRoleInstance?.roles?.getClass())}"><g:select class="form-control" name="roles" from="${com.verecloud.nimbus4.party.Role.list()}" multiple="multiple" optionKey="id" size="5" value="${userGroupRoleInstance?.roles*.value?.id}" class="many-to-many"/></g:if><g:else><g:select class="form-control" name="roles" from="${com.verecloud.nimbus4.party.Role.list()}" multiple="multiple" optionKey="id" size="5" value="${userGroupRoleInstance?.roles*.id}" class="many-to-many"/></g:else>
		
	</div>
</div>

<div class="${hasErrors(bean: userGroupRoleInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="userGroupRole.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

