<%@ page import="com.verecloud.nimbus4.party.UserGroupRole" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'userGroupRole.label', default: 'UserGroupRole')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-userGroupRole" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'userGroupRole.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'userGroupRole.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="userGroupRole.user.label" default="User"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'userGroupRole.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'userGroupRole.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="userGroupRole.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${userGroupRoleInstanceList}" status="i" var="userGroupRoleInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${userGroupRoleInstance.id}">${userGroupRoleInstance}</g:link></td>
					
					<td>${fieldValue(bean: userGroupRoleInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: userGroupRoleInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(userGroupRoleInstance?.user)?.class?.simpleName}" action="show" id="${userGroupRoleInstance?.user?.id}">${userGroupRoleInstance?.user?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${userGroupRoleInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${userGroupRoleInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${userGroupRoleInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(userGroupRoleInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${userGroupRoleInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
