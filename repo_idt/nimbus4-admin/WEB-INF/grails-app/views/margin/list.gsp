<%@ page import="com.verecloud.nimbus4.billing.Margin" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'margin.label', default: 'Margin')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-margin" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="disableDate" title="${message(code: 'margin.disableDate.label', default: 'Disable Date')}"/>
				
					<g:sortableColumn property="distributorMargin" title="${message(code: 'margin.distributorMargin.label', default: 'Distributor Margin')}"/>
				
					<g:sortableColumn property="enableDate" title="${message(code: 'margin.enableDate.label', default: 'Enable Date')}"/>
				
					<g:sortableColumn property="externalId" title="${message(code: 'margin.externalId.label', default: 'External Id')}"/>
				
					<g:sortableColumn property="foreignKey" title="${message(code: 'margin.foreignKey.label', default: 'Foreign Key')}"/>
				
					<g:sortableColumn property="foreignKeyType" title="${message(code: 'margin.foreignKeyType.label', default: 'Foreign Key Type')}"/>
				
					<g:sortableColumn property="resellerMargin" title="${message(code: 'margin.resellerMargin.label', default: 'Reseller Margin')}"/>
				
					<g:sortableColumn property="subdomain" title="${message(code: 'margin.subdomain.label', default: 'Subdomain')}"/>
				
				<th><g:message code="margin.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${marginInstanceList}" status="i" var="marginInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${marginInstance.id}">${marginInstance}</g:link></td>
					
					<td><g:formatDate date="${marginInstance.disableDate}"/></td>
					
					<td>
						
						${fieldValue(bean: marginInstance, field: "distributorMargin")}
						
					</td>
					
					<td><g:formatDate date="${marginInstance.enableDate}"/></td>
					
					<td>${fieldValue(bean: marginInstance, field: "externalId")}</td>
					
					<td>${fieldValue(bean: marginInstance, field: "foreignKey")}</td>
					
					<td>${fieldValue(bean: marginInstance, field: "foreignKeyType")}</td>
					
					<td>
						
						${fieldValue(bean: marginInstance, field: "resellerMargin")}
						
					</td>
					
					<td>${fieldValue(bean: marginInstance, field: "subdomain")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${marginInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(marginInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${marginInstanceCount}"/>
	</div>
</section>

</body>

</html>
