<%@ page import="com.verecloud.nimbus4.billing.Margin" %>


<div class="${hasErrors(bean: marginInstance, field: 'disableDate', 'error')} ">
	<label for="disableDate" class="control-label"><g:message code="margin.disableDate.label" default="Disable Date"/> </label>
	<div>
		
			<bs:datePicker name="disableDate" precision="day"  value="${marginInstance?.disableDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'distributorMargin', 'error')} required">
	<label for="distributorMargin" class="control-label"><g:message code="margin.distributorMargin.label" default="Distributor Margin"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="distributorMargin" value="${marginInstance.distributorMargin}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'enableDate', 'error')} required">
	<label for="enableDate" class="control-label"><g:message code="margin.enableDate.label" default="Enable Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="enableDate" precision="day"  value="${marginInstance?.enableDate}"  />
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'externalId', 'error')} required">
	<label for="externalId" class="control-label"><g:message code="margin.externalId.label" default="External Id"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="externalId" type="number" value="${marginInstance.externalId}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'foreignKey', 'error')} ">
	<label for="foreignKey" class="control-label"><g:message code="margin.foreignKey.label" default="Foreign Key"/> </label>
	<div>
		
			<g:textField class="form-control" name="foreignKey" value="${marginInstance?.foreignKey}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'foreignKeyType', 'error')} required">
	<label for="foreignKeyType" class="control-label"><g:message code="margin.foreignKeyType.label" default="Foreign Key Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="foreignKeyType" from="${com.verecloud.nimbus4.product.enums.MarginFKType?.values()}" keys="${com.verecloud.nimbus4.product.enums.MarginFKType.values()*.name()}" required="" value="${marginInstance?.foreignKeyType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'resellerMargin', 'error')} required">
	<label for="resellerMargin" class="control-label"><g:message code="margin.resellerMargin.label" default="Reseller Margin"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:field class="form-control" name="resellerMargin" value="${marginInstance.resellerMargin}" required=""/>
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'subdomain', 'error')} ">
	<label for="subdomain" class="control-label"><g:message code="margin.subdomain.label" default="Subdomain"/> </label>
	<div>
		
			<g:textField class="form-control" name="subdomain" value="${marginInstance?.subdomain}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: marginInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="margin.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

