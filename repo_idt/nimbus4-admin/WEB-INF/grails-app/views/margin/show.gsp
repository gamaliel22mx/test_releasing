<%@ page import="com.verecloud.nimbus4.billing.Margin" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'margin.label', default: 'Margin')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-margin" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${marginInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(marginInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.disableDate.label" default="Disable Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${marginInstance?.disableDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.distributorMargin.label" default="Distributor Margin" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: marginInstance, field: "distributorMargin")}
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.enableDate.label" default="Enable Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${marginInstance?.enableDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.externalId.label" default="External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: marginInstance, field: "externalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.foreignKey.label" default="Foreign Key" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: marginInstance, field: "foreignKey")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.foreignKeyType.label" default="Foreign Key Type" /></td>
			
			<td valign="top" class="value">${marginInstance?.foreignKeyType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.resellerMargin.label" default="Reseller Margin" /></td>
			
			<td valign="top" class="value">
				
				${fieldValue(bean: marginInstance, field: "resellerMargin")}
				
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="margin.subdomain.label" default="Subdomain" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: marginInstance, field: "subdomain")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
