<%@ page import="com.verecloud.nimbus4.product.PricePlan" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'pricePlan.label', default: 'PricePlan')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-pricePlan" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'pricePlan.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'pricePlan.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'pricePlan.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'pricePlan.description.label', default: 'Description')}"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'pricePlan.groovyScript.label', default: 'Groovy Script')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'pricePlan.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'pricePlan.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="pricePlan.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${pricePlanInstanceList}" status="i" var="pricePlanInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${pricePlanInstance.id}">${pricePlanInstance}</g:link></td>
					
					<td>${fieldValue(bean: pricePlanInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: pricePlanInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: pricePlanInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: pricePlanInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: pricePlanInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${pricePlanInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${pricePlanInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${pricePlanInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(pricePlanInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${pricePlanInstanceCount}"/>
	</div>
</section>

</body>

</html>
