<%@ page import="com.verecloud.nimbus4.product.PricePlan" %>


<div class="${hasErrors(bean: pricePlanInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="pricePlan.name.label" default="Name"/> </label>
	<div>
		
			<g:textField class="form-control" name="name" value="${pricePlanInstance?.name}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: pricePlanInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="pricePlan.description.label" default="Description"/> </label>
	<div>
		
			<g:textField class="form-control" name="description" value="${pricePlanInstance?.description}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: pricePlanInstance, field: 'groovyScript', 'error')} ">
	<label for="groovyScript" class="control-label"><g:message code="pricePlan.groovyScript.label" default="Groovy Script"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="groovyScript" value="${pricePlanInstance?.groovyScript}" />
            <div id="groovyScriptResizable"><div id="groovyScriptEditor">${pricePlanInstance?.groovyScript}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: pricePlanInstance, field: 'schema', 'error')} ">
	<label for="schema" class="control-label"><g:message code="pricePlan.schema.label" default="Schema"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="schema" value="${(pricePlanInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(pricePlanInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: pricePlanInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="pricePlan.configuration.label" default="Configuration"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(pricePlanInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(pricePlanInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: pricePlanInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="pricePlan.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #groovyScriptResizable { width: 1000px; height: 900px; padding: 5px; border: 1px solid #aedeae}
                     #groovyScriptResizable{position: relative}
                     #groovyScriptEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                groovyScriptEditor = ace.edit("groovyScriptEditor");
                                var groovyScript = $('textarea[name="groovyScript"]').hide();
                                groovyScriptEditor.session.setMode("ace/mode/groovy");
                                groovyScriptEditor.setTheme("ace/theme/tomorrow");
                                groovyScriptEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('groovyScriptEditor').style.fontSize='15px'
                                groovyScriptEditor.setReadOnly(false)
                                groovyScriptEditor.getSession().on('change', function(){
                                    groovyScript.val(groovyScriptEditor.getSession().getValue());
                                });
                                 jq("#groovyScriptResizable").resizable({
                                    resize: function( event, ui ) {
                                    groovyScriptEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(false)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>

