<%@ page import="com.verecloud.nimbus4.product.PricePlan" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'pricePlan.label', default: 'PricePlan')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-pricePlan" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${pricePlanInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(pricePlanInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: pricePlanInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: pricePlanInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: pricePlanInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: pricePlanInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.groovyScript.label" default="Groovy Script" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="groovyScript" value="${pricePlanInstance?.groovyScript}" />
            <div id="groovyScriptResizable"><div id="groovyScriptEditor">${pricePlanInstance?.groovyScript}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.schema.label" default="Schema" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="schema" value="${(pricePlanInstance?.schema as grails.converters.JSON).toString(true)}" />
            <div id="schemaResizable"><div id="schemaEditor">${(pricePlanInstance?.schema as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(pricePlanInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(pricePlanInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${pricePlanInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="pricePlan.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${pricePlanInstance?.lastUpdated}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #groovyScriptResizable { width: 1000px; height: 900px; padding: 5px; border: 1px solid #aedeae}
                     #groovyScriptResizable{position: relative}
                     #groovyScriptEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #schemaResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #schemaResizable{position: relative}
                     #schemaEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                groovyScriptEditor = ace.edit("groovyScriptEditor");
                                var groovyScript = $('textarea[name="groovyScript"]').hide();
                                groovyScriptEditor.session.setMode("ace/mode/groovy");
                                groovyScriptEditor.setTheme("ace/theme/tomorrow");
                                groovyScriptEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('groovyScriptEditor').style.fontSize='15px'
                                groovyScriptEditor.setReadOnly(true)
                                groovyScriptEditor.getSession().on('change', function(){
                                    groovyScript.val(groovyScriptEditor.getSession().getValue());
                                });
                                 jq("#groovyScriptResizable").resizable({
                                    resize: function( event, ui ) {
                                    groovyScriptEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                schemaEditor = ace.edit("schemaEditor");
                                var schema = $('textarea[name="schema"]').hide();
                                schemaEditor.session.setMode("ace/mode/json");
                                schemaEditor.setTheme("ace/theme/tomorrow");
                                schemaEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('schemaEditor').style.fontSize='15px'
                                schemaEditor.setReadOnly(true)
                                schemaEditor.getSession().on('change', function(){
                                    schema.val(schemaEditor.getSession().getValue());
                                });
                                 jq("#schemaResizable").resizable({
                                    resize: function( event, ui ) {
                                    schemaEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
