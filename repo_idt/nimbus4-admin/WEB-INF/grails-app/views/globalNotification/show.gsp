<%@ page import="com.verecloud.nimbus4.notification.GlobalNotification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'globalNotification.label', default: 'GlobalNotification')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-globalNotification" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="globalNotification.message.label" default="Message" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: globalNotificationInstance, field: "message")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="globalNotification.url.label" default="Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: globalNotificationInstance, field: "url")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="globalNotification.duration.label" default="Duration" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: globalNotificationInstance, field: "duration")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="globalNotification.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${globalNotificationInstance?.type?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
