<%@ page import="com.verecloud.nimbus4.notification.GlobalNotification" %>


<div class="${hasErrors(bean: globalNotificationInstance, field: 'message', 'error')} required">
	<label for="message" class="control-label"><g:message code="globalNotification.message.label" default="Message"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:textField class="form-control" name="message" required="" value="${globalNotificationInstance?.message}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: globalNotificationInstance, field: 'url', 'error')} ">
	<label for="url" class="control-label"><g:message code="globalNotification.url.label" default="Url"/> </label>
	<div>
		
			<g:textField class="form-control" name="url" value="${globalNotificationInstance?.url}"/>
		
	</div>
</div>
<g:set var="timePeriodInstance" value="${globalNotificationInstance?.duration}"/>
<fieldset class="embedded"><legend><g:message code="globalNotification.duration.label" default="Duration"/></legend>
	
<div class="${hasErrors(bean: globalNotificationInstance, field: 'duration.endDate', 'error')} ">
	<label for="duration.endDate" class="control-label"><g:message code="globalNotification.duration.endDate.label" default="End Date"/> </label>
	<div>
		
			<bs:datePicker name="duration.endDate" precision="day"  value="${timePeriodInstance?.endDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: globalNotificationInstance, field: 'duration.startDate', 'error')} required">
	<label for="duration.startDate" class="control-label"><g:message code="globalNotification.duration.startDate.label" default="Start Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="duration.startDate" precision="day"  value="${timePeriodInstance?.startDate}"  />
		
	</div>
</div>
</fieldset>
<div class="${hasErrors(bean: globalNotificationInstance, field: 'type', 'error')} ">
	<label for="type" class="control-label"><g:message code="globalNotification.type.label" default="Type"/> </label>
	<div>
		
			<g:select class="form-control" name="type" from="${com.verecloud.nimbus4.notification.enums.NotificationType?.values()}" keys="${com.verecloud.nimbus4.notification.enums.NotificationType.values()*.name()}" value="${globalNotificationInstance?.type?.name()}" noSelection="['': '']"/>
		
	</div>
</div>

