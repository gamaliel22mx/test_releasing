<%@ page import="com.verecloud.nimbus4.notification.GlobalNotification" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'globalNotification.label', default: 'GlobalNotification')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-globalNotification" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="message" title="${message(code: 'globalNotification.message.label', default: 'Message')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="url" title="${message(code: 'globalNotification.url.label', default: 'Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="globalNotification.duration.label" default="Duration"/></th>
				
					<g:sortableColumn property="type" title="${message(code: 'globalNotification.type.label', default: 'Type')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${globalNotificationInstanceList}" status="i" var="globalNotificationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${globalNotificationInstance.id}">${globalNotificationInstance}</g:link></td>
					
					<td>${fieldValue(bean: globalNotificationInstance, field: "message")}</td>
					
					<td>${fieldValue(bean: globalNotificationInstance, field: "url")}</td>
					
					<td>${fieldValue(bean: globalNotificationInstance, field: "duration")}</td>
					
					<td>${fieldValue(bean: globalNotificationInstance, field: "type")}</td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${globalNotificationInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
