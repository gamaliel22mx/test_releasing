<%@ page import="com.verecloud.nimbus4.party.ConnectorParam" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'connectorParam.label', default: 'ConnectorParam')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-connectorParam" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="connectorParam.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${connectorParamInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(connectorParamInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="connectorParam.groupConfiguration.label" default="Group Configuration" /></td>
			
			<td valign="top" class="value"><g:link controller="${connectorParamInstance?.groupConfiguration?.class?.simpleName}" action="show" id="${connectorParamInstance?.groupConfiguration?.id}">${connectorParamInstance?.groupConfiguration?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="connectorParam.key.label" default="Key" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: connectorParamInstance, field: "key")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="connectorParam.type.label" default="Type" /></td>
			
			<td valign="top" class="value">${connectorParamInstance?.type?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="connectorParam.value.label" default="Value" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: connectorParamInstance, field: "value")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
