<%@ page import="com.verecloud.nimbus4.party.ConnectorParam" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'connectorParam.label', default: 'ConnectorParam')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-connectorParam" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
				<th><g:message code="connectorParam.groupConfiguration.label" default="Group Configuration"/></th>
				
					<g:sortableColumn property="key" title="${message(code: 'connectorParam.key.label', default: 'Key')}"/>
				
					<g:sortableColumn property="type" title="${message(code: 'connectorParam.type.label', default: 'Type')}"/>
				
					<g:sortableColumn property="value" title="${message(code: 'connectorParam.value.label', default: 'Value')}"/>
				
				<th><g:message code="connectorParam.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${connectorParamInstanceList}" status="i" var="connectorParamInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${connectorParamInstance.id}">${connectorParamInstance}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(connectorParamInstance?.groupConfiguration)?.class?.simpleName}" action="show" id="${connectorParamInstance?.groupConfiguration?.id}">${connectorParamInstance?.groupConfiguration?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: connectorParamInstance, field: "key")}</td>
					
					<td>${fieldValue(bean: connectorParamInstance, field: "type")}</td>
					
					<td>${fieldValue(bean: connectorParamInstance, field: "value")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${connectorParamInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(connectorParamInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${connectorParamInstanceCount}"/>
	</div>
</section>

</body>

</html>
