<%@ page import="com.verecloud.nimbus4.party.ConnectorParam" %>


<div class="${hasErrors(bean: connectorParamInstance, field: 'groupConfiguration', 'error')} required">
	<label for="groupConfiguration" class="control-label"><g:message code="connectorParam.groupConfiguration.label" default="Group Configuration"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="groupConfiguration" name="groupConfiguration.id" from="${com.verecloud.nimbus4.party.GroupConfiguration.list()}" optionKey="id" required="" value="${connectorParamInstance?.groupConfiguration?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: connectorParamInstance, field: 'key', 'error')} ">
	<label for="key" class="control-label"><g:message code="connectorParam.key.label" default="Key"/> </label>
	<div>
		
			<g:textField class="form-control" name="key" value="${connectorParamInstance?.key}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: connectorParamInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="connectorParam.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<div class="${hasErrors(bean: connectorParamInstance, field: 'type', 'error')} required">
	<label for="type" class="control-label"><g:message code="connectorParam.type.label" default="Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="type" from="${com.verecloud.nimbus4.party.enums.ConnectorParamType?.values()}" keys="${com.verecloud.nimbus4.party.enums.ConnectorParamType.values()*.name()}" required="" value="${connectorParamInstance?.type?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: connectorParamInstance, field: 'value', 'error')} ">
	<label for="value" class="control-label"><g:message code="connectorParam.value.label" default="Value"/> </label>
	<div>
		
			<g:textField class="form-control" name="value" value="${connectorParamInstance?.value}"/>
		
	</div>
</div>

