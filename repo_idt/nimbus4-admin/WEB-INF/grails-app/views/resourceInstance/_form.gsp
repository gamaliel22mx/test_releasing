<%@ page import="com.verecloud.nimbus4.resource.ResourceInstance" %>


<div class="${hasErrors(bean: resourceInstanceInstance, field: 'parent', 'error')} ">
	<label for="parent" class="control-label"><g:message code="resourceInstance.parent.label" default="Parent"/> </label>
	<div>
		
			<g:select class="form-control" id="parent" name="parent.id" from="${com.verecloud.nimbus4.service.ServiceInstance.list()}" optionKey="id" value="${resourceInstanceInstance?.parent?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceInstanceInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="resourceInstance.configuration.label" default="Configuration"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(resourceInstanceInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(resourceInstanceInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>
<g:set var="timePeriodInstance" value="${resourceInstanceInstance?.activeFor}"/>
<fieldset class="embedded"><legend><g:message code="resourceInstance.activeFor.label" default="Active For"/></legend>
	
<div class="${hasErrors(bean: resourceInstanceInstance, field: 'activeFor.endDate', 'error')} ">
	<label for="activeFor.endDate" class="control-label"><g:message code="resourceInstance.activeFor.endDate.label" default="End Date"/> </label>
	<div>
		
			<bs:datePicker name="activeFor.endDate" precision="day"  value="${timePeriodInstance?.endDate}" default="none" noSelection="['': '']" />
		
	</div>
</div>

<div class="${hasErrors(bean: resourceInstanceInstance, field: 'activeFor.startDate', 'error')} required">
	<label for="activeFor.startDate" class="control-label"><g:message code="resourceInstance.activeFor.startDate.label" default="Start Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			<bs:datePicker name="activeFor.startDate" precision="day"  value="${timePeriodInstance?.startDate}"  />
		
	</div>
</div>
</fieldset>
<div class="${hasErrors(bean: resourceInstanceInstance, field: 'resourceSpecification', 'error')} required">
	<label for="resourceSpecification" class="control-label"><g:message code="resourceInstance.resourceSpecification.label" default="Resource Specification"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="resourceSpecification" name="resourceSpecification.id" from="${com.verecloud.nimbus4.resource.ResourceSpecification.list()}" optionKey="id" required="" value="${resourceInstanceInstance?.resourceSpecification?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceInstanceInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="resourceInstance.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.InstanceStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.InstanceStatus.values()*.name()}" required="" value="${resourceInstanceInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceInstanceInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="resourceInstance.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>

