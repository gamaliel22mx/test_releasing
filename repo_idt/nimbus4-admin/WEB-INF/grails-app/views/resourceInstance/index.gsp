<%@ page import="com.verecloud.nimbus4.resource.ResourceInstance" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'resourceInstance.label', default: 'ResourceInstance')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-resourceInstance" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'resourceInstance.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'resourceInstance.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="resourceInstance.parent.label" default="Parent"/></th>
				
				<th><g:message code="resourceInstance.activeFor.label" default="Active For"/></th>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'resourceInstance.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'resourceInstance.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="resourceInstance.resourceSpecification.label" default="Resource Specification"/></th>
				
					<g:sortableColumn property="status" title="${message(code: 'resourceInstance.status.label', default: 'Status')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="resourceInstance.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${resourceInstanceInstanceList}" status="i" var="resourceInstanceInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${resourceInstanceInstance.id}">${resourceInstanceInstance}</g:link></td>
					
					<td>${fieldValue(bean: resourceInstanceInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: resourceInstanceInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(resourceInstanceInstance?.parent)?.class?.simpleName}" action="show" id="${resourceInstanceInstance?.parent?.id}">${resourceInstanceInstance?.parent?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: resourceInstanceInstance, field: "activeFor")}</td>
					
					<td><g:formatDate date="${resourceInstanceInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${resourceInstanceInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(resourceInstanceInstance?.resourceSpecification)?.class?.simpleName}" action="show" id="${resourceInstanceInstance?.resourceSpecification?.id}">${resourceInstanceInstance?.resourceSpecification?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: resourceInstanceInstance, field: "status")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${resourceInstanceInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(resourceInstanceInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${resourceInstanceInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
