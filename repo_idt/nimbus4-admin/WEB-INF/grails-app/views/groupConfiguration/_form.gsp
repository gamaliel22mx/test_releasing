<%@ page import="com.verecloud.nimbus4.party.GroupConfiguration" %>


<div class="${hasErrors(bean: groupConfigurationInstance, field: 'group', 'error')} ">
	<label for="group" class="control-label"><g:message code="groupConfiguration.group.label" default="Group"/> <span class="required-indicator">*</span></label>

	<div>
		<g:if test="${groupConfigurationInstance?.group}">
			<g:textField class="form-control" disabled="" name="group.name" value="${groupConfigurationInstance.group.collect{"${it.name}(${it.role.name()})"}.first()}"/>
			<g:hiddenField class="form-control" name="group.id" value="${groupConfigurationInstance.group.id}"/>
		</g:if>
		<g:else>
			<g:textField class="form-control" disabled="" name="group.name" value="${nimbus.findTenant()}"/>
			<g:hiddenField class="form-control" name="group.id" value="${nimbus.findTenantId()}"/>
		</g:else>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'defaultResellerId', 'error')} ">
	<label for="defaultResellerId" class="control-label"><g:message code="groupConfiguration.defaultResellerId.label" default="Default Reseller Id"/> <span class="required-indicator">*</span></label>
	<div>
		<g:textField class="form-control" name="defaultResellerId" value="${groupConfigurationInstance?.defaultResellerId}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'defaultAssignedRepId', 'error')} ">
	<label for="defaultAssignedRepId" class="control-label"><g:message code="groupConfiguration.defaultAssignedRepId.label" default="Default Assigned Rep Id"/> <span class="required-indicator">*</span></label>
	<div>
		<g:textField class="form-control" name="defaultAssignedRepId" value="${groupConfigurationInstance?.defaultAssignedRepId}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'subdomain', 'error')} ">
	<label for="subdomain" class="control-label"><g:message code="groupConfiguration.subdomain.label" default="Subdomain"/> <span class="required-indicator">*</span></label>
	<div>
		<g:textField class="form-control" name="subdomain" required="" value="${groupConfigurationInstance?.subdomain}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'crmConnector', 'error')} ">
	<label for="crmConnector" class="control-label"><g:message code="groupConfiguration.crmConnector.label" default="Crm Connector"/> <span class="required-indicator">*</span></label>
	<div>
		<g:textField class="form-control" name="crmConnector" value="${groupConfigurationInstance?.crmConnector}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'connectorParams', 'error')} ">
	<label for="connectorParams" class="control-label"><g:message code="groupConfiguration.connectorParams.label" default="Connector Params"/> </label>
	<div>
		
<ul class="one-to-many">
			<g:each in="${groupConfigurationInstance?.connectorParams}" var="c">
			    <g:if test="${c}">
                    <li>
                        <g:if test="${c?.class?.enclosingClass}">
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
                                <g:link
                                    controller="${c?.value?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.value?.class?.simpleName}"
                                    action="show"
                                    id="${c?.value?.id}">
                                    ${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:if>
                        <g:else>
                            <g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
                                <g:link
                                    controller="${c?.class?.genericSuperclass?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link
                                    controller="${c?.class?.simpleName}"
                                    action="show"
                                    id="${c.id}">
                                    ${c?.encodeAsHTML()}
                                </g:link>
                            </g:else>
                        </g:else>
                    </li>
                </g:if>
		</g:each>
		<g:each in="${nimbus.findSubClasses(domainClass: 'connectorParam').toString().split(",")}" var="instance">
			<li class="add">
				<g:link controller="${instance}" action="create"
						params="['groupConfiguration.id': groupConfigurationInstance?.id]">
				        ${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
				</g:link>
			</li>
		</g:each>
</ul>

	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'billingConnector', 'error')} ">
	<label for="billingConnector" class="control-label"><g:message code="groupConfiguration.billingConnector.label" default="Billing Connector"/> <span class="required-indicator">*</span></label>
	<div>
		<g:textField class="form-control" name="billingConnector" value="${groupConfigurationInstance?.billingConnector}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'retailBillingEnabled', 'error')} ">
	<label for="retailBillingEnabled" class="control-label"><g:message code="groupConfiguration.retailBillingEnabled.label" default="Retail Billing Enabled"/> </label>
	<div>
		<bs:checkBox name="retailBillingEnabled" value="${groupConfigurationInstance?.retailBillingEnabled}" />
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'requiresCreditCard', 'error')} ">
	<label for="requiresCreditCard" class="control-label"><g:message code="groupConfiguration.requiresCreditCard.label" default="Requires Credit Card"/> </label>
	<div>
		<bs:checkBox name="requiresCreditCard" value="${groupConfigurationInstance?.requiresCreditCard}" />
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'companyWebSiteUrl', 'error')} ">
	<label for="companyWebSiteUrl" class="control-label"><g:message code="groupConfiguration.companyWebSiteUrl.label" default="Company Web Site Url"/> </label>
	<div>
		<g:field class="form-control" type="url" name="companyWebSiteUrl" value="${groupConfigurationInstance?.companyWebSiteUrl}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'supportEmailAddress', 'error')} ">
	<label for="supportEmailAddress" class="control-label"><g:message code="groupConfiguration.supportEmailAddress.label" default="Support Email Address"/> </label>
	<div>
		<g:field class="form-control" type="email" name="supportEmailAddress" value="${groupConfigurationInstance?.supportEmailAddress}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'supportPhoneNumber', 'error')} ">
	<label for="supportPhoneNumber" class="control-label"><g:message code="groupConfiguration.supportPhoneNumber.label" default="Support Phone Number"/> </label>
	<div>
		<g:textField class="form-control" name="supportPhoneNumber" value="${groupConfigurationInstance?.supportPhoneNumber}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'salesEmailAddress', 'error')} ">
	<label for="salesEmailAddress" class="control-label"><g:message code="groupConfiguration.salesEmailAddress.label" default="Sales Email Address"/> </label>
	<div>
		<g:field class="form-control" type="email" name="salesEmailAddress" value="${groupConfigurationInstance?.salesEmailAddress}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'locale', 'error')} ">
	<label for="locale" class="control-label"><g:message code="groupConfiguration.locale.label" default="Locale"/> </label>
	<div>
		<g:textField class="form-control" name="locale" value="${groupConfigurationInstance?.locale}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'gatewayUrl', 'error')} ">
	<label for="gatewayUrl" class="control-label"><g:message code="groupConfiguration.gatewayUrl.label" default="Gateway Url"/> <span class="required-indicator">*</span></label>
	<div>
		<g:field class="form-control" type="url" name="gatewayUrl" value="${groupConfigurationInstance?.gatewayUrl}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'crmGatewayUrl', 'error')} ">
	<label for="crmGatewayUrl" class="control-label"><g:message code="groupConfiguration.crmGatewayUrl.label" default="Crm Gateway Url"/> <span class="required-indicator">*</span></label>
	<div>
		<g:field class="form-control" type="url" name="crmGatewayUrl" value="${groupConfigurationInstance?.crmGatewayUrl}"/>
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'distributorTargetCurrency', 'error')} ">
	<label for="distributorTargetCurrency" class="control-label"><g:message code="groupConfiguration.distributorTargetCurrency.label" default="Distributor Target Currency"/> <span class="required-indicator">*</span></label>
	<div>
		<g:currencySelect class='form-control' name="distributorTargetCurrency" value="${groupConfigurationInstance?.distributorTargetCurrency}"  />
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'resellerTargetCurrency', 'error')} ">
	<label for="resellerTargetCurrency" class="control-label"><g:message code="groupConfiguration.resellerTargetCurrency.label" default="Reseller Target Currency"/> <span class="required-indicator">*</span></label>
	<div>
		<g:currencySelect class='form-control' name="resellerTargetCurrency" value="${groupConfigurationInstance?.resellerTargetCurrency}"  />
	</div>
</div>

<div class="${hasErrors(bean: groupConfigurationInstance, field: 'jasperGatewayUrl', 'error')} ">
    <label for="jasperGatewayUrl" class="control-label"><g:message code="groupConfiguration.jasperGatewayUrl.label" default="Jasper Gateway Url"/> </label>
    <div>
        <g:field class="form-control" type="url" name="jasperGatewayUrl" value="${groupConfigurationInstance?.jasperGatewayUrl}"/>
    </div>
</div>

