<%@ page import="com.verecloud.nimbus4.party.GroupConfiguration" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupConfiguration.label', default: 'GroupConfiguration')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-groupConfiguration" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
				<th><g:message code="groupConfiguration.group.label" default="Group"/></th>
				
					<g:sortableColumn property="defaultResellerId" title="${message(code: 'groupConfiguration.defaultResellerId.label', default: 'Default Reseller Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="defaultAssignedRepId" title="${message(code: 'groupConfiguration.defaultAssignedRepId.label', default: 'Default Assigned Rep Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="subdomain" title="${message(code: 'groupConfiguration.subdomain.label', default: 'Subdomain')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="crmConnector" title="${message(code: 'groupConfiguration.crmConnector.label', default: 'Crm Connector')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="billingConnector" title="${message(code: 'groupConfiguration.billingConnector.label', default: 'Billing Connector')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="retailBillingEnabled" title="${message(code: 'groupConfiguration.retailBillingEnabled.label', default: 'Retail Billing Enabled')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="requiresCreditCard" title="${message(code: 'groupConfiguration.requiresCreditCard.label', default: 'Requires Credit Card')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="companyWebSiteUrl" title="${message(code: 'groupConfiguration.companyWebSiteUrl.label', default: 'Company Web Site Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="supportEmailAddress" title="${message(code: 'groupConfiguration.supportEmailAddress.label', default: 'Support Email Address')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="supportPhoneNumber" title="${message(code: 'groupConfiguration.supportPhoneNumber.label', default: 'Support Phone Number')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="salesEmailAddress" title="${message(code: 'groupConfiguration.salesEmailAddress.label', default: 'Sales Email Address')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="locale" title="${message(code: 'groupConfiguration.locale.label', default: 'Locale')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="gatewayUrl" title="${message(code: 'groupConfiguration.gatewayUrl.label', default: 'Gateway Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="crmGatewayUrl" title="${message(code: 'groupConfiguration.crmGatewayUrl.label', default: 'Crm Gateway Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="distributorTargetCurrency" title="${message(code: 'groupConfiguration.distributorTargetCurrency.label', default: 'Distributor Target Currency')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="resellerTargetCurrency" title="${message(code: 'groupConfiguration.resellerTargetCurrency.label', default: 'Reseller Target Currency')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="jasperGatewayUrl" title="${message(code: 'groupConfiguration.jasperGatewayUrl.label', default: 'Jasper Gateway Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="groupConfiguration.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupConfigurationInstanceList}" status="i" var="groupConfigurationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupConfigurationInstance.id}">${groupConfigurationInstance}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupConfigurationInstance?.group)?.class?.simpleName}" action="show" id="${groupConfigurationInstance?.group?.id}">${groupConfigurationInstance?.group?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "defaultResellerId")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "defaultAssignedRepId")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "subdomain")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "crmConnector")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "billingConnector")}</td>
					
					<td><g:formatBoolean boolean="${groupConfigurationInstance.retailBillingEnabled}"/></td>
							
					<td><g:formatBoolean boolean="${groupConfigurationInstance.requiresCreditCard}"/></td>
							
					<td>${fieldValue(bean: groupConfigurationInstance, field: "companyWebSiteUrl")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "supportEmailAddress")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "supportPhoneNumber")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "salesEmailAddress")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "locale")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "gatewayUrl")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "crmGatewayUrl")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "distributorTargetCurrency")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "resellerTargetCurrency")}</td>
					
					<td>${fieldValue(bean: groupConfigurationInstance, field: "jasperGatewayUrl")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${groupConfigurationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(groupConfigurationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupConfigurationInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
