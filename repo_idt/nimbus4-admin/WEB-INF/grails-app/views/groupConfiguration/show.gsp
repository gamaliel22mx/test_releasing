<%@ page import="com.verecloud.nimbus4.party.GroupConfiguration" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupConfiguration.label', default: 'GroupConfiguration')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-groupConfiguration" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${groupConfigurationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(groupConfigurationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.group.label" default="Group" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupConfigurationInstance?.group?.class?.simpleName}" action="show" id="${groupConfigurationInstance?.group?.id}">${groupConfigurationInstance?.group?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.defaultResellerId.label" default="Default Reseller Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "defaultResellerId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.defaultAssignedRepId.label" default="Default Assigned Rep Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "defaultAssignedRepId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.subdomain.label" default="Subdomain" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "subdomain")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.crmConnector.label" default="Crm Connector" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "crmConnector")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.connectorParams.label" default="Connector Params" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupConfigurationInstance?.connectorParams}" var="c">
	<g:if test="${c}">
		<li>
			<g:if test="${c?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.value?.class)}">
					<g:link
							controller="${c?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.value?.class?.simpleName}"
							action="show"
							id="${c?.value?.id}">
						${c?.key?.encodeAsHTML()+" : "+c?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(c?.class)}">
					<g:link
							controller="${c?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${c?.class?.simpleName}"
							action="show"
							id="${c?.id}">
						${c?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.billingConnector.label" default="Billing Connector" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "billingConnector")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.retailBillingEnabled.label" default="Retail Billing Enabled" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${groupConfigurationInstance?.retailBillingEnabled}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.requiresCreditCard.label" default="Requires Credit Card" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${groupConfigurationInstance?.requiresCreditCard}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.companyWebSiteUrl.label" default="Company Web Site Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "companyWebSiteUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.supportEmailAddress.label" default="Support Email Address" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "supportEmailAddress")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.supportPhoneNumber.label" default="Support Phone Number" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "supportPhoneNumber")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.salesEmailAddress.label" default="Sales Email Address" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "salesEmailAddress")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.locale.label" default="Locale" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "locale")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.gatewayUrl.label" default="Gateway Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "gatewayUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.crmGatewayUrl.label" default="Crm Gateway Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "crmGatewayUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.distributorTargetCurrency.label" default="Distributor Target Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "distributorTargetCurrency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.resellerTargetCurrency.label" default="Reseller Target Currency" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "resellerTargetCurrency")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="groupConfiguration.jasperGatewayUrl.label" default="Jasper Gateway Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupConfigurationInstance, field: "jasperGatewayUrl")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style></style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                    });
                </script>
</body>

</html>
