<%@ page import="com.verecloud.nimbus4.party.Group" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-group" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.language.label" default="Language" /></td>
			
			<td valign="top" class="value">${groupInstance?.language?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${groupInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.role.label" default="Role" /></td>
			
			<td valign="top" class="value">${groupInstance?.role?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.groupSpecification.label" default="Group Specification" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupInstance?.groupSpecification?.class?.simpleName}" action="show" id="${groupInstance?.groupSpecification?.id}">${groupInstance?.groupSpecification?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.activationDate.label" default="Activation Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupInstance?.activationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.deactivationDate.label" default="Deactivation Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupInstance?.deactivationDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.parent.label" default="Parent" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupInstance?.parent?.class?.simpleName}" action="show" id="${groupInstance?.parent?.id}">${groupInstance?.parent?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.assignedRepId.label" default="Assigned Rep Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "assignedRepId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.groupAddress.label" default="Group Address" /></td>
			
			<td valign="top" class="value"><g:link controller="${groupInstance?.groupAddress?.class?.simpleName}" action="show" id="${groupInstance?.groupAddress?.id}">${groupInstance?.groupAddress?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.externalId.label" default="External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "externalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.parentExternalId.label" default="Parent External Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "parentExternalId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.websiteURL.label" default="Website URL" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "websiteURL")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.csrAgreementUrl.label" default="Csr Agreement Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "csrAgreementUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.termsOfUseUrl.label" default="Terms Of Use Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: groupInstance, field: "termsOfUseUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(groupInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(groupInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.suppliers.label" default="Suppliers" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupInstance?.suppliers}" var="s">
	<g:if test="${s}">
		<li>
			<g:if test="${s?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.value?.class)}">
					<g:link
							controller="${s?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${s?.value?.id}">
						${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${s?.value?.class?.simpleName}"
							action="show"
							id="${s?.value?.id}">
						${s?.key?.encodeAsHTML()+" : "+s?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(s?.class)}">
					<g:link
							controller="${s?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${s?.id}">
						${s?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${s?.class?.simpleName}"
							action="show"
							id="${s?.id}">
						${s?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.activeIntegrations.label" default="Active Integrations" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupInstance?.activeIntegrations}" var="a">
	<g:if test="${a}">
		<li>
			<g:if test="${a?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(a?.value?.class)}">
					<g:link
							controller="${a?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${a?.value?.id}">
						${a?.key?.encodeAsHTML()+" : "+a?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${a?.value?.class?.simpleName}"
							action="show"
							id="${a?.value?.id}">
						${a?.key?.encodeAsHTML()+" : "+a?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(a?.class)}">
					<g:link
							controller="${a?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${a?.id}">
						${a?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${a?.class?.simpleName}"
							action="show"
							id="${a?.id}">
						${a?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.integrationKeys.label" default="Integration Keys" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupInstance?.integrationKeys}" var="i">
	<g:if test="${i}">
		<li>
			<g:if test="${i?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(i?.value?.class)}">
					<g:link
							controller="${i?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${i?.value?.id}">
						${i?.key?.encodeAsHTML()+" : "+i?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${i?.value?.class?.simpleName}"
							action="show"
							id="${i?.value?.id}">
						${i?.key?.encodeAsHTML()+" : "+i?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(i?.class)}">
					<g:link
							controller="${i?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${i?.id}">
						${i?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${i?.class?.simpleName}"
							action="show"
							id="${i?.id}">
						${i?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${groupInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.productSpecifications.label" default="Product Specifications" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
<g:each in="${groupInstance?.productSpecifications}" var="p">
	<g:if test="${p}">
		<li>
			<g:if test="${p?.class?.enclosingClass}">
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(p?.value?.class)}">
					<g:link
							controller="${p?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${p?.value?.id}">
						${p?.key?.encodeAsHTML()+" : "+p?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${p?.value?.class?.simpleName}"
							action="show"
							id="${p?.value?.id}">
						${p?.key?.encodeAsHTML()+" : "+p?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(p?.class)}">
					<g:link
							controller="${p?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="${p?.id}">
						${p?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="${p?.class?.simpleName}"
							action="show"
							id="${p?.id}">
						${p?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="group.termsAccepted.label" default="Terms Accepted" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${groupInstance?.termsAccepted}"/></td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
