<%@ page import="com.verecloud.nimbus4.party.Group" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-group" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'group.createdBy.label', default: 'Created By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'group.lastUpdatedBy.label', default: 'Last Updated By')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="name" title="${message(code: 'group.name.label', default: 'Name')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="language" title="${message(code: 'group.language.label', default: 'Language')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="status" title="${message(code: 'group.status.label', default: 'Status')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="role" title="${message(code: 'group.role.label', default: 'Role')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="group.groupSpecification.label" default="Group Specification"/></th>
				
					<g:sortableColumn property="activationDate" title="${message(code: 'group.activationDate.label', default: 'Activation Date')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="deactivationDate" title="${message(code: 'group.deactivationDate.label', default: 'Deactivation Date')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="group.parent.label" default="Parent"/></th>
				
					<g:sortableColumn property="assignedRepId" title="${message(code: 'group.assignedRepId.label', default: 'Assigned Rep Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
				<th><g:message code="group.groupAddress.label" default="Group Address"/></th>
				
					<g:sortableColumn property="externalId" title="${message(code: 'group.externalId.label', default: 'External Id')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="websiteURL" title="${message(code: 'group.websiteURL.label', default: 'Website URL')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="csrAgreementUrl" title="${message(code: 'group.csrAgreementUrl.label', default: 'Csr Agreement Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="termsOfUseUrl" title="${message(code: 'group.termsOfUseUrl.label', default: 'Terms Of Use Url')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'group.dateCreated.label', default: 'Date Created')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'group.lastUpdated.label', default: 'Last Updated')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
					<g:sortableColumn property="termsAccepted" title="${message(code: 'group.termsAccepted.label', default: 'Terms Accepted')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupInstanceList}" status="i" var="groupInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupInstance.id}">${groupInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "language")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "status")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "role")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupInstance?.groupSpecification)?.class?.simpleName}" action="show" id="${groupInstance?.groupSpecification?.id}">${groupInstance?.groupSpecification?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatDate date="${groupInstance.activationDate}"/></td>
					
					<td><g:formatDate date="${groupInstance.deactivationDate}"/></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupInstance?.parent)?.class?.simpleName}" action="show" id="${groupInstance?.parent?.id}">${groupInstance?.parent?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: groupInstance, field: "assignedRepId")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupInstance?.groupAddress)?.class?.simpleName}" action="show" id="${groupInstance?.groupAddress?.id}">${groupInstance?.groupAddress?.encodeAsHTML()}</g:link></td>
					
					<td>${fieldValue(bean: groupInstance, field: "externalId")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "websiteURL")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "csrAgreementUrl")}</td>
					
					<td>${fieldValue(bean: groupInstance, field: "termsOfUseUrl")}</td>
					
					<td><g:formatDate date="${groupInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${groupInstance.lastUpdated}"/></td>
					
					<td><g:formatBoolean boolean="${groupInstance.termsAccepted}"/></td>
							
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupInstanceCount}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="${searchCO?.max}" sort="${searchCO?.sort}" order="${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
