<%@ page import="com.verecloud.nimbus4.service.enums.ChannelRole; com.verecloud.nimbus4.party.Group" %>
<g:set var="tenantId" value="${session.getAttribute('tenantId')}"/>

<div class="${hasErrors(bean: groupInstance, field: 'name', 'error')} required">
	<label for="name" class="control-label"><g:message code="group.name.label" default="Name"/> <span
			class="required-indicator">*</span></label>
	<div>

		<g:textField class="form-control" name="name" required="" value="${groupInstance?.name}"/>

	</div>
</div>


<div class="${hasErrors(bean: groupInstance, field: 'language', 'error')} required">
	<label for="language" class="control-label"><g:message code="group.language.label" default="Language"/> <span
			class="required-indicator">*</span></label>

	<div>

		<g:select class="form-control" name="language" from="${com.verecloud.nimbus4.service.enums.Language?.values()}"
		          keys="${com.verecloud.nimbus4.service.enums.Language.values()*.name()}" required=""
		          value="${groupInstance?.language?.name()}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="group.status.label" default="Status"/> <span
			class="required-indicator">*</span></label>

	<div>

		<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.PartyStatus?.values()}"
		          keys="${com.verecloud.nimbus4.service.enums.PartyStatus.values()*.name()}" required=""
		          value="${groupInstance?.status?.name()}"/>

	</div>
</div>

<g:if test="${isEditingSelf}">

	<div class="${hasErrors(bean: groupInstance, field: 'role', 'error')} required">
		<label for="role" class="control-label"><g:message code="group.role.label" default="Role"/> <span
				class="required-indicator">*</span></label>

		<div>
			<g:select class="form-control" name="role" onchange="populateParentGroups()"
					  from="${Group.findValidRolesForEdit()}"
					  keys="${Group.findValidRolesForEdit()*.name()}" required=""
			          value="${groupInstance?.role?.name()}"/>

		</div>
	</div>
</g:if>
<g:else>
	<div class="${hasErrors(bean: groupInstance, field: 'role', 'error')} required">
		<label for="role" class="control-label"><g:message code="group.role.label" default="Role"/><span
				class="required-indicator">*</span></label>

		<div>
			<g:select class="form-control" name="role" onchange="populateParentGroups()"
			          from="${Group.findValidChildRoles(tenantId)}"
			          keys="${Group.findValidChildRoles(tenantId)*.name()}" required=""
			          value="${groupInstance?.role?.name()}"/>
		</div>
	</div>
</g:else>

<div class="${hasErrors(bean: groupInstance, field: 'groupSpecification', 'error')} ">
	<label for="groupSpecification" class="control-label"><g:message code="group.groupSpecification.label"
	                                                                 default="Group Specification"/></label>

	<div>

		<g:select class="form-control" id="groupSpecification" name="groupSpecification.id"
		          from="${com.verecloud.nimbus4.party.GroupSpecification.list()}" optionKey="id"
		          value="${groupInstance?.groupSpecification?.id}" class="many-to-one" noSelection="['null': '']"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'activationDate', 'error')} ">
	<label for="activationDate" class="control-label"><g:message code="group.activationDate.label"
	                                                             default="Activation Date"/></label>

	<div>

		<bs:datePicker name="activationDate" precision="day" value="${groupInstance?.activationDate}" default="none"
		               noSelection="['': '']"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'deactivationDate', 'error')} ">
	<label for="deactivationDate" class="control-label"><g:message code="group.deactivationDate.label"
	                                                               default="Deactivation Date"/></label>

	<div>

		<bs:datePicker name="deactivationDate" precision="day" value="${groupInstance?.deactivationDate}" default="none"
		               noSelection="['': '']"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'parent', 'error')} ">
	<label for="parent" class="control-label"><g:message code="group.parent.label" default="Parent"/></label>

	<div id="parentDiv">
		<g:select class="form-control" id="parent" name="parent.id" from="${[]}" optionKey="id"  class="many-to-one" noSelection="[null:'No Parent']" />
	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'assignedRepId', 'error')} ">
	<label for="assignedRepId" class="control-label"><g:message code="group.assignedRepId.label"
	                                                            default="Assigned Rep Id"/></label>

	<div>

		<g:textField class="form-control" name="assignedRepId" value="${groupInstance?.assignedRepId}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'groupAddress', 'error')} ">
	<label for="groupAddress" class="control-label"><g:message code="group.groupAddress.label"
	                                                           default="Group Address"/></label>

	<div>

		<g:select class="form-control" id="groupAddress" name="groupAddress.id"
		          from="${com.verecloud.nimbus4.party.GroupAddress.list()}" optionKey="id"
		          value="${groupInstance?.groupAddress?.id}" class="many-to-one" noSelection="['null': '']"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'externalId', 'error')} ">
	<label for="externalId" class="control-label"><g:message code="group.externalId.label"
	                                                         default="External Id"/></label>

	<div>

		<g:textField class="form-control" name="externalId" value="${groupInstance?.externalId}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'parentExternalId', 'error')} ">
	<label for="parentExternalId" class="control-label"><g:message code="group.parentExternalId.label"
	                                                               default="Parent External Id"/></label>

	<div>

		<g:textField class="form-control" name="parentExternalId" value="${groupInstance?.parentExternalId}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'websiteURL', 'error')} ">
	<label for="websiteURL" class="control-label"><g:message code="group.websiteURL.label"
	                                                         default="Website URL"/></label>

	<div>

		<g:textField class="form-control" name="websiteURL" value="${groupInstance?.websiteURL}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'csrAgreementUrl', 'error')} ">
	<label for="csrAgreementUrl" class="control-label"><g:message code="group.csrAgreementUrl.label"
	                                                              default="Csr Agreement Url"/></label>

	<div>

		<g:field class="form-control" type="url" name="csrAgreementUrl" value="${groupInstance?.csrAgreementUrl}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'termsOfUseUrl', 'error')} ">
	<label for="termsOfUseUrl" class="control-label"><g:message code="group.termsOfUseUrl.label"
	                                                            default="Terms Of Use Url"/></label>

	<div>

		<g:field class="form-control" type="url" name="termsOfUseUrl" value="${groupInstance?.termsOfUseUrl}"/>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="group.configuration.label"
	                                                            default="Configuration"/></label>

	<div>

		<g:textArea class="form-control" name="configuration"
		            value="${(groupInstance?.configuration as grails.converters.JSON).toString(true)}"/>
		<div id="configurationResizable"><div
				id="configurationEditor">${(groupInstance?.configuration as grails.converters.JSON).toString(true)}</div>
		</div>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'activeIntegrations', 'error')} ">
	<label for="activeIntegrations" class="control-label"><g:message code="group.activeIntegrations.label"
	                                                                 default="Active Integrations"/></label>

	<div>

		<g:if test="${groupInstance?.activeIntegrations?.getClass() && Map.isAssignableFrom(groupInstance?.activeIntegrations?.getClass())}"><g:select
				class="form-control" name="activeIntegrations"
				from="${com.verecloud.nimbus4.party.GroupIntegration.list()}" multiple="multiple" optionKey="id"
				size="5" value="${groupInstance?.activeIntegrations*.value?.id}"
				class="many-to-many"/></g:if><g:else><g:select class="form-control" name="activeIntegrations"
		                                                       from="${com.verecloud.nimbus4.party.GroupIntegration.list()}"
		                                                       multiple="multiple" optionKey="id" size="5"
		                                                       value="${groupInstance?.activeIntegrations*.id}"
		                                                       class="many-to-many"/></g:else>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'integrationKeys', 'error')} ">
	<label for="integrationKeys" class="control-label"><g:message code="group.integrationKeys.label"
	                                                              default="Integration Keys"/></label>

	<div>

		<ul class="one-to-many">
			<g:each in="${groupInstance?.integrationKeys}" var="i">
				<g:if test="${i}">
					<li>
						<g:if test="${i?.class?.enclosingClass}">
							<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(i?.value?.class)}">
								<g:link
										controller="${i?.value?.class?.genericSuperclass?.simpleName}"
										action="show"
										id="${i?.value?.id}">
									${i?.key?.encodeAsHTML() + " : " + i?.value?.encodeAsHTML()}
								</g:link>
							</g:if>
							<g:else>
								<g:link
										controller="${i?.value?.class?.simpleName}"
										action="show"
										id="${i?.value?.id}">
									${i?.key?.encodeAsHTML() + " : " + i?.value?.encodeAsHTML()}
								</g:link>
							</g:else>
						</g:if>
						<g:else>
							<g:if test="${javassist.util.proxy.ProxyFactory.isProxyClass(i?.class)}">
								<g:link
										controller="${i?.class?.genericSuperclass?.simpleName}"
										action="show"
										id="${i.id}">
									${i?.encodeAsHTML()}
								</g:link>
							</g:if>
							<g:else>
								<g:link
										controller="${i?.class?.simpleName}"
										action="show"
										id="${i.id}">
									${i?.encodeAsHTML()}
								</g:link>
							</g:else>
						</g:else>
					</li>
				</g:if>
			</g:each>
			<g:each in="${nimbus.findSubClasses(domainClass: 'groupIntegrationKey').toString().split(",")}" var="instance">
				<li class="add">
					<g:link controller="${instance}" action="create"
					        params="['group.id': groupInstance?.id]">
						${message(code: 'default.add.label', args: [message(code: "${instance}.label", default: "${instance}")])}
					</g:link>
				</li>
			</g:each>
		</ul>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'productSpecifications', 'error')} ">
	<label for="productSpecifications" class="control-label"><g:message code="group.productSpecifications.label"
	                                                                    default="Product Specifications"/></label>

	<div>

		<g:if test="${groupInstance?.productSpecifications?.getClass() && Map.isAssignableFrom(groupInstance?.productSpecifications?.getClass())}"><g:select
				class="form-control" name="productSpecifications"
				from="${com.verecloud.nimbus4.product.ProductSpecification.list()}" multiple="multiple"
				optionKey="id" size="5" value="${groupInstance?.productSpecifications*.value?.id}"
				class="many-to-many"/></g:if><g:else><g:select class="form-control" name="productSpecifications"
		                                                       from="${com.verecloud.nimbus4.product.ProductSpecification.list()}"
		                                                       multiple="multiple" optionKey="id" size="5"
		                                                       value="${groupInstance?.productSpecifications*.id}"
		                                                       class="many-to-many"/></g:else>

	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'suppliers', 'error')} ">
	<label for="suppliers" class="control-label"><g:message code="group.suppliers.label"
																		default="Suppliers"/></label>

<div>
		<g:if test="${groupInstance?.suppliers?.getClass() && Map.isAssignableFrom(groupInstance?.suppliers?.getClass())}"><g:select
				class="form-control" name="suppliers"
				from="${Group.findAllByRole(ChannelRole.SUPPLIER)}" multiple="multiple"
				optionKey="id" size="5" value="${groupInstance?.suppliers*.value?.id}"
				class="many-to-many"/></g:if><g:else><g:select class="form-control" name="suppliers"
															   from="${Group.findAllByRole(ChannelRole.SUPPLIER)}"
															   multiple="multiple" optionKey="id" size="5"
															   value="${groupInstance?.suppliers*.id}"
															   class="many-to-many"/></g:else>
	</div>
</div>

<div class="${hasErrors(bean: groupInstance, field: 'termsAccepted', 'error')} ">
	<label for="termsAccepted" class="control-label"><g:message code="group.termsAccepted.label"
	                                                            default="Terms Accepted"/></label>

	<div>

		<bs:checkBox name="termsAccepted" value="${groupInstance?.termsAccepted}"/>

	</div>
</div>

<style>
#configurationResizable {
	width: 1000px;
	height: 500px;
	padding: 5px;
	border: 1px solid #aedeae
}

#configurationResizable {
	position: relative
}

#configurationEditor {
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
}
</style>
<script src="${resource(dir: 'js/ace', file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
<script src="${resource(dir: 'js/ace', file: 'ext-language_tools.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css/jquery', file: 'jquery-ui.css')}">
<script src="${resource(dir: 'js/jquery', file: 'jquery-ui.js')}"></script>
<script>
	var jq = jq ? jq : $.noConflict();
	jq(document).ready(function () {

		ace.require("ace/ext/language_tools");
		configurationEditor = ace.edit("configurationEditor");
		var configuration = $('textarea[name="configuration"]').hide();
		configurationEditor.session.setMode("ace/mode/json");
		configurationEditor.setTheme("ace/theme/tomorrow");
		configurationEditor.setOptions({
			enableBasicAutocompletion: true,
			enableSnippets: true,
			enableLiveAutocompletion: false

		});
		document.getElementById('configurationEditor').style.fontSize = '15px'
		configurationEditor.setReadOnly(false)
		configurationEditor.getSession().on('change', function () {
			configuration.val(configurationEditor.getSession().getValue());
		});
		jq("#configurationResizable").resizable({
			resize: function (event, ui) {
				configurationEditor.resize();
			}
		});
	});
</script>
<script>
//repopulate the parents list according to the role selected
	function populateParentGroups(){
		console.info("===="+jQuery("#role").val())
		var role = jQuery("#role").val()
		if(role =="SUPPLIER"){
			jQuery('#parent').val("null")
			jQuery('#parent').prop('disabled', true);

		}else{
	jQuery.ajax({
				url :  "${createLink(controller: 'group', action: 'populateParentGroups')}"+"?role="+role+"&parent=${groupInstance?.parentId}",
			success :  function(data) {
						jQuery('#parentDiv').html(data);
	}
});
	}
	}

//loading the parent according to the role on page load
	jQuery(window).load(function(){
		populateParentGroups()
	});
</script>
