<%@ page import="com.verecloud.nimbus4.content.Content" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'content.label', default: 'Content')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-content" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="key" title="${message(code: 'content.key.label', default: 'Key')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'content.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'content.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="content.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${contentInstanceList}" status="i" var="contentInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${contentInstance.id}">${contentInstance}</g:link></td>
					
					<td>${fieldValue(bean: contentInstance, field: "key")}</td>
					
					<td><g:formatDate date="${contentInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${contentInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${contentInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(contentInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${contentInstanceCount}"/>
	</div>
</section>

</body>

</html>
