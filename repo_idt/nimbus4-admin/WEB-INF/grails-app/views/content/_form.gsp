<%@ page import="com.verecloud.nimbus4.content.Content" %>


<div class="${hasErrors(bean: contentInstance, field: 'key', 'error')} ">
	<label for="key" class="control-label"><g:message code="content.key.label" default="Key"/> </label>
	<div>
		
			<g:textField class="form-control" name="key" value="${contentInstance?.key}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: contentInstance, field: 'content', 'error')} required">
	<label for="content" class="control-label"><g:message code="content.content.label" default="Content"/> <span class="required-indicator">*</span></label>
	<div>
		

		
           <g:textArea class="form-control" name="content" value="${(contentInstance?.content as grails.converters.JSON).toString(true)}" />
            <div id="contentResizable"><div id="contentEditor">${(contentInstance?.content as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: contentInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="content.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #contentResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #contentResizable{position: relative}
                     #contentEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                contentEditor = ace.edit("contentEditor");
                                var content = $('textarea[name="content"]').hide();
                                contentEditor.session.setMode("ace/mode/json");
                                contentEditor.setTheme("ace/theme/tomorrow");
                                contentEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('contentEditor').style.fontSize='15px'
                                contentEditor.setReadOnly(false)
                                contentEditor.getSession().on('change', function(){
                                    content.val(contentEditor.getSession().getValue());
                                });
                                 jq("#contentResizable").resizable({
                                    resize: function( event, ui ) {
                                    contentEditor.resize();
                                  }
                                 });

            
                    });
                </script>

