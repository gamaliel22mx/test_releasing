<%@ page import="com.verecloud.nimbus4.resource.ResourceVisualizer" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'resourceVisualizer.label', default: 'ResourceVisualizer')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-resourceVisualizer" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'resourceVisualizer.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'resourceVisualizer.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="name" title="${message(code: 'resourceVisualizer.name.label', default: 'Name')}"/>
				
					<g:sortableColumn property="description" title="${message(code: 'resourceVisualizer.description.label', default: 'Description')}"/>
				
					<g:sortableColumn property="groovyScript" title="${message(code: 'resourceVisualizer.groovyScript.label', default: 'Groovy Script')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'resourceVisualizer.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'resourceVisualizer.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="resourceVisualizer.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${resourceVisualizerInstanceList}" status="i" var="resourceVisualizerInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${resourceVisualizerInstance.id}">${resourceVisualizerInstance}</g:link></td>
					
					<td>${fieldValue(bean: resourceVisualizerInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: resourceVisualizerInstance, field: "lastUpdatedBy")}</td>
					
					<td>${fieldValue(bean: resourceVisualizerInstance, field: "name")}</td>
					
					<td>${fieldValue(bean: resourceVisualizerInstance, field: "description")}</td>
					
					<td>${fieldValue(bean: resourceVisualizerInstance, field: "groovyScript")}</td>
					
					<td><g:formatDate date="${resourceVisualizerInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${resourceVisualizerInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${resourceVisualizerInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(resourceVisualizerInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${resourceVisualizerInstanceCount}"/>
	</div>
</section>

</body>

</html>
