<%@ page import="com.verecloud.nimbus4.resource.ResourceOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'resourceOrder.label', default: 'ResourceOrder')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-resourceOrder" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="${resourceOrderInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(resourceOrderInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.createdBy.label" default="Created By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceOrderInstance, field: "createdBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.lastUpdatedBy.label" default="Last Updated By" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceOrderInstance, field: "lastUpdatedBy")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.parent.label" default="Parent" /></td>
			
			<td valign="top" class="value"><g:link controller="${resourceOrderInstance?.parent?.class?.simpleName}" action="show" id="${resourceOrderInstance?.parent?.id}">${resourceOrderInstance?.parent?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.instance.label" default="Instance" /></td>
			
			<td valign="top" class="value"><g:link controller="${resourceOrderInstance?.instance?.class?.simpleName}" action="show" id="${resourceOrderInstance?.instance?.id}">${resourceOrderInstance?.instance?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.configuration.label" default="Configuration" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="configuration" value="${(resourceOrderInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(resourceOrderInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.cost.label" default="Cost" /></td>
			
			<td valign="top" class="value">
				
           <g:textArea class="form-control" name="cost" value="${(resourceOrderInstance?.cost as grails.converters.JSON).toString(true)}" />
            <div id="costResizable"><div id="costEditor">${(resourceOrderInstance?.cost as grails.converters.JSON).toString(true)}</div></div>
                    
			</td>

			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.specification.label" default="Specification" /></td>
			
			<td valign="top" class="value"><g:link controller="${resourceOrderInstance?.specification?.class?.simpleName}" action="show" id="${resourceOrderInstance?.specification?.id}">${resourceOrderInstance?.specification?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.childrenCompleted.label" default="Children Completed" /></td>
			
			<td valign="top" class="value"><g:formatBoolean boolean="${resourceOrderInstance?.childrenCompleted}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.requestDate.label" default="Request Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${resourceOrderInstance?.requestDate}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.children.label" default="Children" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: resourceOrderInstance, field: "children")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${resourceOrderInstance?.dateCreated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${resourceOrderInstance?.lastUpdated}"/></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.orderType.label" default="Order Type" /></td>
			
			<td valign="top" class="value">${resourceOrderInstance?.orderType?.encodeAsHTML()}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="resourceOrder.status.label" default="Status" /></td>
			
			<td valign="top" class="value">${resourceOrderInstance?.status?.encodeAsHTML()}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>
<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #costResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #costResizable{position: relative}
                     #costEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(true)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                costEditor = ace.edit("costEditor");
                                var cost = $('textarea[name="cost"]').hide();
                                costEditor.session.setMode("ace/mode/json");
                                costEditor.setTheme("ace/theme/tomorrow");
                                costEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('costEditor').style.fontSize='15px'
                                costEditor.setReadOnly(true)
                                costEditor.getSession().on('change', function(){
                                    cost.val(costEditor.getSession().getValue());
                                });
                                 jq("#costResizable").resizable({
                                    resize: function( event, ui ) {
                                    costEditor.resize();
                                  }
                                 });

            
                    });
                </script>
</body>

</html>
