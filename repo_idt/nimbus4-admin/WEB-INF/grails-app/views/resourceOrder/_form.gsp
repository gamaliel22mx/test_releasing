<%@ page import="com.verecloud.nimbus4.resource.ResourceOrder" %>


<div class="${hasErrors(bean: resourceOrderInstance, field: 'parent', 'error')} ">
	<label for="parent" class="control-label"><g:message code="resourceOrder.parent.label" default="Parent"/> </label>
	<div>
		
			<g:select class="form-control" id="parent" name="parent.id" from="${com.verecloud.nimbus4.service.ServiceOrder.list()}" optionKey="id" value="${resourceOrderInstance?.parent?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'instance', 'error')} ">
	<label for="instance" class="control-label"><g:message code="resourceOrder.instance.label" default="Instance"/> </label>
	<div>
		
			<g:select class="form-control" id="instance" name="instance.id" from="${com.verecloud.nimbus4.resource.ResourceInstance.list()}" optionKey="id" value="${resourceOrderInstance?.instance?.id}" class="many-to-one" noSelection="['null': '']"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'configuration', 'error')} ">
	<label for="configuration" class="control-label"><g:message code="resourceOrder.configuration.label" default="Configuration"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="configuration" value="${(resourceOrderInstance?.configuration as grails.converters.JSON).toString(true)}" />
            <div id="configurationResizable"><div id="configurationEditor">${(resourceOrderInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'cost', 'error')} ">
	<label for="cost" class="control-label"><g:message code="resourceOrder.cost.label" default="Cost"/> </label>
	<div>
		

		
           <g:textArea class="form-control" name="cost" value="${(resourceOrderInstance?.cost as grails.converters.JSON).toString(true)}" />
            <div id="costResizable"><div id="costEditor">${(resourceOrderInstance?.cost as grails.converters.JSON).toString(true)}</div></div>
                    
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'specification', 'error')} required">
	<label for="specification" class="control-label"><g:message code="resourceOrder.specification.label" default="Specification"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" id="specification" name="specification.id" from="${com.verecloud.nimbus4.resource.ResourceSpecification.findAllByTenantId(nimbus.findTenantId().toLong())}" optionKey="id" required="" value="${resourceOrderInstance?.specification?.id}" class="many-to-one"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'childrenCompleted', 'error')} ">
	<label for="childrenCompleted" class="control-label"><g:message code="resourceOrder.childrenCompleted.label" default="Children Completed"/> </label>
	<div>
		
			<nimbus:checkBox name="childrenCompleted"  disabled="disabled" value="${resourceOrderInstance?.childrenCompleted}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'requestDate', 'error')} required">
	<label for="requestDate" class="control-label"><g:message code="resourceOrder.requestDate.label" default="Request Date"/> <span class="required-indicator">*</span></label>
	<div>
		
			${resourceOrderInstance?.requestDate?.toString()}
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'children', 'error')} ">
	<label for="children" class="control-label"><g:message code="resourceOrder.children.label" default="Children"/> </label>
	<div>
		
			<g:textArea class="form-control" name="children" cols="40" rows="5" value="${resourceOrderInstance?.children?.join("\n")}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'orderType', 'error')} required">
	<label for="orderType" class="control-label"><g:message code="resourceOrder.orderType.label" default="Order Type"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="orderType" from="${com.verecloud.nimbus4.service.enums.OrderType?.values()}" keys="${com.verecloud.nimbus4.service.enums.OrderType.values()*.name()}" required="" value="${resourceOrderInstance?.orderType?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'status', 'error')} required">
	<label for="status" class="control-label"><g:message code="resourceOrder.status.label" default="Status"/> <span class="required-indicator">*</span></label>
	<div>
		
			<g:select class="form-control" name="status" from="${com.verecloud.nimbus4.service.enums.OrderStatus?.values()}" keys="${com.verecloud.nimbus4.service.enums.OrderStatus.values()*.name()}" required="" value="${resourceOrderInstance?.status?.name()}"/>
		
	</div>
</div>

<div class="${hasErrors(bean: resourceOrderInstance, field: 'tenantId', 'error')} }">
	<label for="tenantId" class="control-label"><g:message code="resourceOrder.tenantId.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>

<style>
                     #configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #configurationResizable{position: relative}
                     #configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     
                     #costResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
                     #costResizable{position: relative}
                     #costEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
                     </style>
            <script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
            <script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
            <link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
            <script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
            <script>
                    var jq = jq?jq: $.noConflict();
                    jq(document).ready(function (){
            
                                ace.require("ace/ext/language_tools");
                                configurationEditor = ace.edit("configurationEditor");
                                var configuration = $('textarea[name="configuration"]').hide();
                                configurationEditor.session.setMode("ace/mode/json");
                                configurationEditor.setTheme("ace/theme/tomorrow");
                                configurationEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('configurationEditor').style.fontSize='15px'
                                configurationEditor.setReadOnly(false)
                                configurationEditor.getSession().on('change', function(){
                                    configuration.val(configurationEditor.getSession().getValue());
                                });
                                 jq("#configurationResizable").resizable({
                                    resize: function( event, ui ) {
                                    configurationEditor.resize();
                                  }
                                 });

            
                                ace.require("ace/ext/language_tools");
                                costEditor = ace.edit("costEditor");
                                var cost = $('textarea[name="cost"]').hide();
                                costEditor.session.setMode("ace/mode/json");
                                costEditor.setTheme("ace/theme/tomorrow");
                                costEditor.setOptions({
                                    enableBasicAutocompletion: true,
                                    enableSnippets: true,
                                    enableLiveAutocompletion: false

                                });
                                document.getElementById('costEditor').style.fontSize='15px'
                                costEditor.setReadOnly(false)
                                costEditor.getSession().on('change', function(){
                                    cost.val(costEditor.getSession().getValue());
                                });
                                 jq("#costResizable").resizable({
                                    resize: function( event, ui ) {
                                    costEditor.resize();
                                  }
                                 });

            
                    });
                </script>

