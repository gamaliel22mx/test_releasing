<%@ page import="com.verecloud.nimbus4.resource.ResourceOrder" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'resourceOrder.label', default: 'ResourceOrder')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-resourceOrder" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'resourceOrder.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'resourceOrder.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
				<th><g:message code="resourceOrder.parent.label" default="Parent"/></th>
				
				<th><g:message code="resourceOrder.instance.label" default="Instance"/></th>
				
				<th><g:message code="resourceOrder.specification.label" default="Specification"/></th>
				
					<g:sortableColumn property="childrenCompleted" title="${message(code: 'resourceOrder.childrenCompleted.label', default: 'Children Completed')}"/>
				
					<g:sortableColumn property="requestDate" title="${message(code: 'resourceOrder.requestDate.label', default: 'Request Date')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'resourceOrder.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'resourceOrder.lastUpdated.label', default: 'Last Updated')}"/>
				
					<g:sortableColumn property="orderType" title="${message(code: 'resourceOrder.orderType.label', default: 'Order Type')}"/>
				
					<g:sortableColumn property="status" title="${message(code: 'resourceOrder.status.label', default: 'Status')}"/>
				
				<th><g:message code="resourceOrder.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${resourceOrderInstanceList}" status="i" var="resourceOrderInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${resourceOrderInstance.id}">${resourceOrderInstance}</g:link></td>
					
					<td>${fieldValue(bean: resourceOrderInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: resourceOrderInstance, field: "lastUpdatedBy")}</td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(resourceOrderInstance?.parent)?.class?.simpleName}" action="show" id="${resourceOrderInstance?.parent?.id}">${resourceOrderInstance?.parent?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(resourceOrderInstance?.instance)?.class?.simpleName}" action="show" id="${resourceOrderInstance?.instance?.id}">${resourceOrderInstance?.instance?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(resourceOrderInstance?.specification)?.class?.simpleName}" action="show" id="${resourceOrderInstance?.specification?.id}">${resourceOrderInstance?.specification?.encodeAsHTML()}</g:link></td>
					
					<td><g:formatBoolean boolean="${resourceOrderInstance.childrenCompleted}"/></td>
							
					<td><g:formatDate date="${resourceOrderInstance.requestDate}"/></td>
					
					<td><g:formatDate date="${resourceOrderInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${resourceOrderInstance.lastUpdated}"/></td>
					
					<td>${fieldValue(bean: resourceOrderInstance, field: "orderType")}</td>
					
					<td>${fieldValue(bean: resourceOrderInstance, field: "status")}</td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${resourceOrderInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(resourceOrderInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${resourceOrderInstanceCount}"/>
	</div>
</section>

</body>

</html>
