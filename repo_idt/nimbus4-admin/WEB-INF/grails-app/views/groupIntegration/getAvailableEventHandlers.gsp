<g:if test="${groupIntegrationInstance?.eventHandlers?.getClass() && Map.isAssignableFrom(groupIntegrationInstance?.eventHandlers?.getClass())}">
    <g:select class="form-control" name="activeHandlers" from="${groupIntegrationInstance.eventHandlers}" multiple="multiple" optionKey="id" size="5" value="${groupIntegrationInstance?.eventHandlers*.value?.id}" class="many-to-many"/>
</g:if>
<g:else>
    <g:select class="form-control" name="activeHandlers" from="${groupIntegrationInstance.eventHandlers}" multiple="multiple" optionKey="id" size="5" value="${groupIntegrationInstance?.eventHandlers*.id}" class="many-to-many"/>
</g:else>