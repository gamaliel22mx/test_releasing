<%@ page import="com.verecloud.nimbus4.party.GroupIntegration" %>


<div class="${hasErrors(bean: groupIntegrationInstance, field: 'configuration', 'error')} ">
    <label for="configuration" class="control-label"><g:message code="groupIntegration.configuration.label" default="Configuration"/> </label>
    <div>
        <g:textArea class="form-control" name="configuration" value="${(groupIntegrationInstance?.configuration as grails.converters.JSON).toString(true)}" />
        <div id="configurationResizable"><div id="configurationEditor">${(groupIntegrationInstance?.configuration as grails.converters.JSON).toString(true)}</div></div>
    </div>
</div>

<div class="${hasErrors(bean: groupIntegrationInstance, field: 'specification', 'error')} required">
    <label for="specification" class="control-label"><g:message code="groupIntegration.specification.label" default="Specification"/> <span class="required-indicator">*</span></label>
    <div>
        <g:select class="form-control" id="specification" name="specification.id" from="${com.verecloud.nimbus4.party.GroupIntegrationSpecification.list()}" optionKey="id" required="" value="${groupIntegrationInstance?.specification?.id}" class="many-to-one" noSelection="['-1':'Select']"  onchange="${remoteFunction(
                action:'getAvailableEventHandlers',
                params:"'eventIntegrationSpecId='+this.value",
                update : 'divId'
        )}"/>

    </div>
</div>

<div class="${hasErrors(bean: groupIntegrationInstance, field: 'activeHandlers', 'error')} ">
    <label for="activeHandlers" class="control-label"><g:message code="groupIntegration.activeHandlers.label" default="Active Handlers"/> </label>
    <div id="divId">
        <g:if test="${groupIntegrationInstance?.activeHandlers?.getClass() && Map.isAssignableFrom(groupIntegrationInstance?.activeHandlers?.getClass())}">
            <g:select class="form-control" name="activeHandlers" from="${com.verecloud.nimbus4.party.GroupIntegrationSpecification.get(groupIntegrationInstance?.specification?.id)?.eventHandlers}" multiple="multiple" optionKey="id" size="5" value="${groupIntegrationInstance?.activeHandlers*.value?.id}" class="many-to-many"/>
        </g:if>
        <g:else>
            <g:select class="form-control" name="activeHandlers" from="${com.verecloud.nimbus4.party.GroupIntegrationSpecification.get(groupIntegrationInstance?.specification?.id)?.eventHandlers}" multiple="multiple" optionKey="id" size="5" value="${groupIntegrationInstance?.activeHandlers*.id}" class="many-to-many"/>
        </g:else>
    </div>
</div>

<div class="${hasErrors(bean: groupIntegrationInstance, field: 'tenantId', 'error')} }">
    <label for="tenantId" class="control-label"><g:message code="groupIntegration.tenantId.label" default="Tenant"/></label>

    <div>
        <g:textField class="form-control" disabled="" name="tenantId.name" value="${nimbus.findCurrentTenantGroup()}"/>
    </div>
</div>

<style>
#configurationResizable { width: 1000px; height: 500px; padding: 5px; border: 1px solid #aedeae}
#configurationResizable{position: relative}
#configurationEditor{position: absolute; top:0;left:0;right:0;bottom:0;}
</style>
<script src="${resource(dir: 'js/ace',file: 'ace.js')}" type="text/javascript" charset="utf-8"></script>
<script src="${resource(dir: 'js/ace',file: 'ext-language_tools.js')}"></script>
<link rel="stylesheet" href="${resource(dir: 'css/jquery',file:'jquery-ui.css')}">
<script src="${resource(dir:'js/jquery',file: 'jquery-ui.js')}"></script>
<script>
    var jq = jq?jq: $.noConflict();
    jq(document).ready(function (){

        ace.require("ace/ext/language_tools");
        configurationEditor = ace.edit("configurationEditor");
        var configuration = $('textarea[name="configuration"]').hide();
        configurationEditor.session.setMode("ace/mode/json");
        configurationEditor.setTheme("ace/theme/tomorrow");
        configurationEditor.setOptions({
            enableBasicAutocompletion: true,
            enableSnippets: true,
            enableLiveAutocompletion: false

        });
        document.getElementById('configurationEditor').style.fontSize='15px'
        configurationEditor.setReadOnly(false)
        configurationEditor.getSession().on('change', function(){
            configuration.val(configurationEditor.getSession().getValue());
        });
        jq("#configurationResizable").resizable({
            resize: function( event, ui ) {
                configurationEditor.resize();
            }
        });

    });

</script>

