<%@ page import="com.verecloud.nimbus4.party.GroupIntegration" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="${message(code: 'groupIntegration.label', default: 'GroupIntegration')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<section id="list-groupIntegration" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				
				<th>${entityName}</th>
				
					<g:sortableColumn property="createdBy" title="${message(code: 'groupIntegration.createdBy.label', default: 'Created By')}"/>
				
					<g:sortableColumn property="lastUpdatedBy" title="${message(code: 'groupIntegration.lastUpdatedBy.label', default: 'Last Updated By')}"/>
				
					<g:sortableColumn property="dateCreated" title="${message(code: 'groupIntegration.dateCreated.label', default: 'Date Created')}"/>
				
					<g:sortableColumn property="lastUpdated" title="${message(code: 'groupIntegration.lastUpdated.label', default: 'Last Updated')}"/>
				
				<th><g:message code="groupIntegration.specification.label" default="Specification"/></th>
				
				<th><g:message code="groupIntegration.tenantId.label" default="Tenant"/></th>
				
			</tr>
			</thead>
			<tbody>
			<g:each in="${groupIntegrationInstanceList}" status="i" var="groupIntegrationInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="${groupIntegrationInstance.id}">${groupIntegrationInstance}</g:link></td>
					
					<td>${fieldValue(bean: groupIntegrationInstance, field: "createdBy")}</td>
					
					<td>${fieldValue(bean: groupIntegrationInstance, field: "lastUpdatedBy")}</td>
					
					<td><g:formatDate date="${groupIntegrationInstance.dateCreated}"/></td>
					
					<td><g:formatDate date="${groupIntegrationInstance.lastUpdated}"/></td>
					
					<td valign="top" class="value"><g:link controller="${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(groupIntegrationInstance?.specification)?.class?.simpleName}" action="show" id="${groupIntegrationInstance?.specification?.id}">${groupIntegrationInstance?.specification?.encodeAsHTML()}</g:link></td>
					
					<td valign="top" class="value"><g:link controller="group" action="show" id="${groupIntegrationInstance?.tenantId}">${com.verecloud.nimbus4.party.Group.findById(groupIntegrationInstance?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
				
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="${groupIntegrationInstanceCount}"/>
	</div>
</section>

</body>

</html>
