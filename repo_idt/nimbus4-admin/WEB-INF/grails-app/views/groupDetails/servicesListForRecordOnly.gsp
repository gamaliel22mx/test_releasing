<%@ page import="com.verecloud.nimbus4.party.User"%>
<h3>New Group's Service For Record Only</h3>
	<%--	<div class="actionsNewGroupServiceInstanceRO">--%>
	<div id="newSsProfile">

		<div id="servicesListBox" class="listBox">
			<ul id="listOfAvServices" class="listOfServices">
				<!-- start list users -->
				<g:each in="${servicesListForRecordOnly}" status="i"
					var="serviceSpec">
					<li><g:remoteLink id="${serviceSpec?.id}" action="loadAttributes" params="[flag:'create',groupId:group?.id]"
							oncomplete="showSpinner(false);" onloading="showSpinner(true);"
							update="serviceSettings" class="displayServiceSettings">
							${serviceSpec?.name}
						</g:remoteLink></li>
				</g:each>
				<!-- end list users -->
			</ul>
		</div>
	</div>
	<div id="serviceSettings">
		Select a service from the list!
	</div>