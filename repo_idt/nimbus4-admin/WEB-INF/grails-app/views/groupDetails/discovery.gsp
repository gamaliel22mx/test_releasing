<%--<%@ page import="com.verecloud.nimbus4.service.specification.AttributePossibleValues; com.verecloud.nimbus4.service.ServiceSpecificationGroupAttribute;com.verecloud.nimbus4.service.enums.AttributeType; com.verecloud.nimbus4.service.enums.AttributeMultiplicity;com.verecloud.nimbus4.service.enums.AttributeVisibility" %>
<html>
<body>
<h4>${serviceSpec?.name} Settings</h4>
<g:formRemote class='pure-form pure-form-aligned' update="commonServiceSelected" name="discoveryForm" id="discoveryForm" method='post'
              url="[controller: 'groupDetails', action: 'processDiscovery']">
	<input class="data" type="hidden" id="serviceSpecificationId" name="serviceSpecificationId" value="${serviceSpec.id}"/>
	<input class="data" type="hidden" id="groupId" name="groupId" value="${params.groupId}"/>
	<g:hiddenField name="formData"/>
	<fieldset>
		<g:each in="${treeMap.keySet()}" var="serviceSpecGrpAttrOrder">
			<%	  def serviceSpecGrpAttr = treeMap.get(serviceSpecGrpAttrOrder)
			def serviceAttribute = serviceSpecGrpAttr?.serviceAttribute 
                String attributeType = serviceAttribute?.type
                String attributeMultiplicity = serviceAttribute?.multiplicity
		   		AttributeVisibility attributeVisibility = AttributeVisibility.EDITABLE; //For discovery & record only we show all the fields
			    String hasValidators = serviceAttribute?.validators
			    String validatorStr = ' '
			    def validatorType = serviceAttribute?.type
			    boolean required = true
			    int itr = 0
			   	 def defaultValue = attributeValues?.get(serviceSpecGrpAttr?.key) ?:''
			 //  def defaultValue = attributeValues?.get(serviceSpecGrpAttr?.key) ?:serviceAttribute?.defaultValue?.expression  //calculate value from expression
			   def defaultValueSplit = defaultValue.toString().split('group')
           %>
		<%	validatorStr = com.nimbus4.group.util.CommonUtils.getFieldType(validatorType,attributeVisibility,serviceAttribute?.validators) %>

			<g:if test="${serviceSpecGrpAttr?.serviceAttribute?.discovery && serviceSpecGrpAttr?.key != 'accountId'}">
				<g:if test="${attributeVisibility == AttributeVisibility.HIDDEN}">
						<g:hiddenField class="data" name="${serviceAttribute?.name}" type="${validatorStr}" value="${defaultValue}" />
						${serviceAttribute?.name} hidden
					</g:if>
					<g:else>  orderEntryVisibility = EDITABLE or Read-only
							<div class='pure-control-group ${attributeType} ${attributeMultiplicity}'>
							<div class='formLabel'>
							<label for='${serviceSpecGrpAttr?.key}'>
								${serviceSpecGrpAttr?.key}
							</label>
							</div>
							<% AttributePossibleValues possibleValues = serviceSpecGrpAttr?.serviceAttribute?.possibleValues %>
							<g:if test="${possibleValues}">  Display a dropdown 
								<div class='formValue'>
									<g:if test="${attributeVisibility == AttributeVisibility.READ_ONLY}">
									 TODO set default Value 
										<g:select class="data" name="${serviceSpecGrpAttr?.key}" from="${possibleValues?.staticValues.entrySet()}" optionKey="key"
										          optionValue="value"  disabled=""/>
									</g:if>
									<g:elseif test="${serviceSpecGrpAttr?.serviceAttribute?.orderEntryVisibility == AttributeVisibility.EDITABLE}">
										<g:select class="data" name="${serviceSpecGrpAttr?.key}" from="${possibleValues?.staticValues.entrySet()}" optionKey="key"
										          optionValue="value" />
									</g:elseif>
								</div>
							</g:if>
							<g:else>
							 READ-ONLY or EDITABLE 
							<g:if test="${serviceAttribute?.type == AttributeType.COMPLEX}">
		                    	<g:if test="${serviceAttribute?.multiplicity == AttributeMultiplicity.ONE}">
		                    			<g:render contextPath="/groupDetails" template="complexOne" model="['serviceAttribute':serviceAttribute,'required':required,'validatorStr':validatorStr,'defaultValue':defaultValue]" />
		                    	</g:if>
                    			<g:elseif test="${serviceAttribute?.multiplicity == AttributeMultiplicity.MANY}">
                    				<g:render contextPath="/groupDetails" template="complexMany" model="['serviceAttribute':serviceAttribute,'required':required,'validatorStr':validatorStr,'defaultValue':defaultValue]" />
                    			</g:elseif>
                    	
		                    </g:if>
		                    <g:else>  SIMPLE OBJECT BEGIN
								<g:if test="${serviceAttribute?.multiplicity == AttributeMultiplicity.ONE}">
				                    	    <g:if test="${required && serviceAttribute?.type != AttributeType.BOOLEAN}">
	                        	    			<div class='formValue required'>
	        			                    		<g:field class="data" name="${serviceAttribute?.name}" type="${validatorStr}" 
			            			               	value="${defaultValue}" required="true"   />
	        	                    			</div>
				            	            </g:if>
					                        <g:else>
					                            <div class='formValue' id="${serviceAttribute?.name}Div">
					                            <g:field class="data" type="${validatorStr}" 
					                            	name="${serviceAttribute?.name}" value="${defaultValue}"  />
					                            </div>
					                        </g:else>   
					                                         		
	                    		</g:if>
	                    		<g:elseif test="${serviceAttribute?.multiplicity == AttributeMultiplicity.MANY}">
	                    				  <div class='formValue' id="${serviceAttribute?.name}Div">
												<g:render contextPath="/groupDetails" template="simpleMany" model="['serviceAttribute':serviceAttribute,'required':required,'validatorStr':validatorStr,'defaultValue':defaultValue]" />
					                       </div>
	                    		</g:elseif>
	                    	</g:else> SIMPLE OBJECT 
							</g:else>
							</div>			
					</g:else>	
					</g:if>	
					</g:each>
		<div class="formValue"><br/>
			<input type="submit" value="Submit Discovery" class="btn btn-primary" onclick="getDataAsJson('#discoveryForm');"/></div>
	</fieldset>
</g:formRemote>
*check on accountReference validation and domain
</body>
</html>
--%>