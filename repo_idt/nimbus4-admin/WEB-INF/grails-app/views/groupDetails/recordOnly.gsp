<%--<%@page import="com.google.gson.GsonBuilder"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="java.lang.Object"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.verecloud.nimbus4.service.enums.AttributeMultiplicity"%>
<%@page import="com.verecloud.nimbus4.service.enums.AttributeType"%>
<%@page import="com.verecloud.nimbus4.service.enums.AttributeVisibility"%>
<%@ page import="com.verecloud.nimbus4.service.specification.AttributePossibleValues; com.verecloud.nimbus4.service.ServiceSpecificationGroupAttribute;com.verecloud.nimbus4.party.Group" %>
<html>
<body>
<g:if test="${afterSubmit == true}">
	${message}
</g:if>
<g:else>
	<h4>${serviceSpec?.name} Settings</h4>
<g:form class='pure-form pure-form-aligned'       controller="groupDetails" action="processRecordOnly" method='post' id="recordOnlyForm" >
	<g:formRemote class='pure-form pure-form-aligned' update="serviceSettings" name="recordOnlyForm" id="recordOnlyForm" method='post'
	              url="[controller: 'groupDetails', action: 'processRecordOnly']">
		<input type="hidden" id="serviceSpecificationId" name="serviceSpecificationId" value="${serviceSpec?.id}"/>
		<input type="hidden" id="groupId" name="groupId" value="${params?.groupId}"/>
		<input type="hidden" id="flag" name="flag" value="${params?.flag}"/>
		<input type="hidden" id="gsi" name="gsi" value="${params?.gsi}"/>
		<input type="hidden" id="sicId" name="sicId" value="${params?.sicId}"/>
		<g:hiddenField name="formData"/>
		<fieldset>
			<g:each in="${treeMap.keySet()}" var="serviceSpecGrpAttrOrder">
					<%	  def serviceSpecGrpAttr = treeMap.get(serviceSpecGrpAttrOrder)
					def serviceAttribute = serviceSpecGrpAttr?.serviceAttribute 
                   String attributeType = serviceAttribute?.type
                   String attributeMultiplicity = serviceAttribute?.multiplicity
				   AttributeVisibility attributeVisibility = serviceAttribute?.orderEntryVisibility
				   attributeVisibility = AttributeVisibility.EDITABLE
				   String hasValidators = serviceAttribute?.validators
				   String validatorStr = ' '
				   def validatorType = serviceAttribute?.type
				   boolean required = serviceAttribute?.required ==true
				   int itr = 0
				   	 def defaultValue = attributeValues?.get(serviceSpecGrpAttr?.key) ?:''
				 //  def defaultValue = attributeValues?.get(serviceSpecGrpAttr?.key) ?:serviceAttribute?.defaultValue?.expression  //calculate value from expression
				   
				   def defaultValueSplit = defaultValue.toString().split('group')
             	  %>
					<%	validatorStr = com.nimbus4.group.util.CommonUtils.getFieldType(validatorType,attributeVisibility,serviceAttribute?.validators,defaultValue) %>
					<g:if test="${attributeVisibility == AttributeVisibility.HIDDEN && serviceSpecGrpAttr?.key != 'accountId'}">
						<g:hiddenField class="data" name="${serviceAttribute?.name}" type="${validatorStr}" value="${defaultValue}" />
					</g:if>
					<g:else>  orderEntryVisibility = EDITABLE or Read-only
						<div class='pure-control-group ${attributeType} ${attributeMultiplicity}'>
							<div class='formLabel'>
							<label for='${serviceSpecGrpAttr?.key}'>
								${serviceSpecGrpAttr?.key}
							</label>
							</div>				
						 <% AttributePossibleValues possibleValues = serviceSpecGrpAttr?.serviceAttribute?.possibleValues %>
							<g:if test="${possibleValues}">  Display a dropdown 
								<div class='formValue'>
									<g:if test="${attributeVisibility == AttributeVisibility.READ_ONLY}">
									 TODO set default Value 
										<g:select class="data" name="${serviceSpecGrpAttr?.key}" from="${possibleValues?.staticValues.entrySet()}" optionKey="key"
										          optionValue="value"  disabled=""/>
									</g:if>
									<g:else>
										<g:select class="data" name="${serviceSpecGrpAttr?.key}" from="${possibleValues?.staticValues.entrySet()}" optionKey="key"
										          optionValue="value" />
									</g:else>
								</div>
							</g:if>
							<g:else>
							<g:if test="${serviceAttribute?.type == AttributeType.COMPLEX}">
		                    	<g:if test="${serviceAttribute?.multiplicity == AttributeMultiplicity.ONE}">
		                    			<g:render contextPath="/groupDetails" template="complexOne" model="['serviceAttribute':serviceAttribute,'required':required,'validatorStr':validatorStr,'defaultValue':defaultValue]" />
		                    	</g:if>
                    			<g:elseif test="${serviceAttribute?.multiplicity == AttributeMultiplicity.MANY}">
                    				<g:render contextPath="/groupDetails" template="complexMany" model="['serviceAttribute':serviceAttribute,'required':required,'validatorStr':validatorStr,'defaultValue':defaultValue]" />
                    			</g:elseif>
                    	
		                    </g:if>
		                    <g:else>  SIMPLE OBJECT 
								<g:if test="${serviceAttribute?.multiplicity == AttributeMultiplicity.ONE}">
				                    	    <g:if test="${required && serviceAttribute?.type != AttributeType.BOOLEAN}">
	                        	    			<div class='formValue required'>
	        			                    		<g:field class="data" name="${serviceAttribute?.name}" type="${validatorStr}" 
			            			               	value="${defaultValue}" required="true"   />
	        	                    			</div>
				            	            </g:if>
					                        <g:else>
					                            <div class='formValue' id="${serviceAttribute?.name}Div">
					                            <g:field class="data" type="${validatorStr}" 
					                            	name="${serviceAttribute?.name}" value="${defaultValue}"  />
					                            </div>
					                        </g:else>   
					                                         		
	                    		</g:if>
	                    		<g:elseif test="${serviceAttribute?.multiplicity == AttributeMultiplicity.MANY}">
	                    				  <div class='formValue' id="${serviceAttribute?.name}Div">
												<g:render contextPath="/groupDetails" template="simpleMany" model="['serviceAttribute':serviceAttribute,'required':required,'validatorStr':validatorStr,'defaultValue':defaultValue]" />
					                       </div>
	                    		</g:elseif>
	                    	</g:else> SIMPLE OBJECT 
	                       </g:else> POSSIBLE VALUES - ELSE 	
                    	</div>
                    	</g:else>
			</g:each>
			<div class="formValue">
				<br/>
				<input type="submit" value="Submit" class="btn btn-primary" onclick="getDataAsJson('#recordOnlyForm');"/>

			</div>
		</fieldset>
	</g:formRemote>
</g:else>
</body>
</html>

--%>