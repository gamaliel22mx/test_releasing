<%@ page import="com.verecloud.nimbus4.party.User"%>

<h3>Record Only GSI services</h3>
        <%--    <div class="actionsNewGroupServiceInstanceRO">--%>
        <div id="newSsProfile">

                <div id="servicesListBox" class="listBox">
                        <ul id="listOfAvServices" class="listOfServices">
                                <!-- start list users -->
                               <g:each in="${existingServiceInstanceList}" status="i"  var="existingServiceInstance">
                                        <li><g:remoteLink id="${existingServiceInstance?.serviceSpecificationVersion?.id}" action="loadAttributes" params="[sicId:gsiSicMap?.get(existingServiceInstance?.id)?.get('id'),groupId:group?.id,flag:'edit',gsi:existingServiceInstance?.id]"
                                                        oncomplete="showSpinner(false);" onloading="showSpinner(true);"
                                                        update="serviceSettings" class="displayServiceSettings">
                                                        GSI id: ${existingServiceInstance?.id}
                                                        <%-- - ${existingServiceInstance?.serviceInstanceConfiguration} --%>
                                                </g:remoteLink>
                                                ---- EDIT <g:link controller="serviceInstance" action="edit" params="[id:existingServiceInstance?.id]">GSI</g:link>
                                                </li>
                                </g:each>
                        </ul>
                </div>
        </div>
        <div id="serviceSettings">
                Select a service from the list!
        </div>

