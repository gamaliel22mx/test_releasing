<%@page import="com.mongodb.util.JSONParser"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.JsonArray"%>
<ul class="data featuresList simplemany" id="${serviceAttribute?.name}" >
   		
   		<% 
		boolean inputExists = attributeValues?.get(serviceAttribute?.name) != null 
		int index = 0;
		%>
   		
   		<g:if test="${inputExists}">
   		<a onclick="addNewItem('${serviceAttribute?.name}','wdiv','${validatorStr}');">Add ${serviceAttribute?.name} </a>
   			<g:each in="${attributeValues?.get('secondaryDomain').toMap()?.keySet()}" var="currKey">
		    <% String value = attributeValues?.get('secondaryDomain').toMap().get(currKey); %>
		       		<li id='${serviceAttribute?.name+index} wdiv childData  ${required?'required':''}'>
				   <g:if test="${required}">
			   			<g:field class="required" type="${validatorStr}" name="${serviceAttribute?.name+index}" value="${value}" required='true'/>
			   		</g:if>
			   		<g:else>
			   			<g:field type="${validatorStr}" name="${serviceAttribute?.name+index}" value="${value}" />
			   		</g:else>
			   		<g:if test="${index != 0}">
			   		<a id="${serviceAttribute?.name+index}" onclick='removeLi();'>Remove</a>
			   		</g:if>
			   		<% index++; %>
			   		</li>
			</g:each>
			
   		</g:if>
   		<g:else>
   		
   		<li id='${serviceAttribute?.name}0 wdiv childData  ${required?'required':''}'>
			   		<g:if test="${required}">
	   			<g:field class="required" type="${validatorStr}" name="${serviceAttribute?.name}0" value="${serviceAttribute?.defaultValue?.expression}" required='true'/>
	   		</g:if>
	   		<g:else>
	   			<g:field type="${validatorStr}" name="${serviceAttribute?.name}0" value="${serviceAttribute?.defaultValue?.expression}" />
	   		</g:else>
   			<a onclick="addNewItem('${serviceAttribute?.name}','wdiv','${validatorStr}');">Add ${serviceAttribute?.name} </a>
   		</li>
   		</g:else>	
</ul>


