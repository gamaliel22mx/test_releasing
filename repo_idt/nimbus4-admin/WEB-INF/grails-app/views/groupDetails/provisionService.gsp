<%--<%@ page import="com.verecloud.nimbus4.service.specification.AttributePossibleValues; com.verecloud.nimbus4.service.ServiceSpecificationGroupAttribute"%>
<%@page import="com.verecloud.nimbus4.service.enums.PartyStatus"%>
<%@ page import="com.verecloud.nimbus4.party.Group" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
    <title><g:message code="group.details.title" default="Group Details - ${group?.name}"  /></title>
    <style type="text/css">

    .bodyDiv{
        padding:10px;
        border-radius:5px;
        box-shadow:3px 2px 2px 5px #eee inset;
        background:#fff;
        margin: 5px;
    }
	#Menu{
		visibility:hidden;
	}
    .pure-control-group {
        padding: 5px;
    }

    .formLabel {
        color: #666666;
        text-align: right;
        width: 35%;
        display: inline-block;
    }

    .formValue {
        color: #666666;
        text-align: left;
        width: 60%;
        float: right;
    }

    .required {
        color: red;
    }

    #side-content {
        padding: 10px;
    }

    #main-content {
        text-align: left;
        background-color: #eee;
        box-shadow:3px 2px 2px 5px #eee inset;
        width: 75%;
        padding: 10px;
    }

    div.actionsNewGroupServiceInstanceDI {
        display: inline-block;
        width: 25%;
        padding:10px
    }
    div#commonServiceSelected {
        border-style: groove;
        text-align: left;
    }

    div#newSsProfile {
        width: 25%;
        display: inline-block;
        padding: 10px;
    }

    div#serviceSettings {
        border-style: groove;
        text-align: center;
        float: right;
        width: 75%;
    }
    </style>

</head>
<body style="vertical-align: middle;">
<div class="bodyDiv" >
	<h4 style="text-align: center;">Add a Service to Provision</h4>
    <div id="side-content">
        <p>
            <span style="font-weight:bold;">GroupID:</span> 
            <span><g:link action="groupDetails" id="${group?.id}">${group?.externalId}</g:link></span>
        </p>
        <p>
            <span style="font-weight:bold;">Language:</span> <span id="groupRole">${group?.language.encodeAsHTML()}</span>
        </p>
        <p>
            <span style="font-weight:bold;">Status:</span>
            <span id="status">
                <g:if test="${group?.parentId && !group?.parentId}">${group?.status}
                </g:if>
            </span>
        </p>
        	<g:select name="serviceSpec" from="${allActiveServicesList}" optionValue="name" optionKey="id" noSelection="['':'Select a service']" onchange="changeSpec();" />
	</div>
    <div id="main-content">
	<div id="commonServiceSelected">
		Select a service for Provision
    </div>
    </div>
  
</div>
<script type="text/javascript">
    function changeSpec(){
		var c = document.getElementById("serviceSpec");
    	var id= c.options[c.selectedIndex].value;
		var groupId = '${group?.id}'; 
        var root="${resource()}";
        var url = root+'/groupDetails/loadProvisionAttributes?groupId='+groupId+"&id="+id;
        jQuery('#commonServiceSelected').load(url);
        return;
    }
    
</script>
</body>
</html>
--%>