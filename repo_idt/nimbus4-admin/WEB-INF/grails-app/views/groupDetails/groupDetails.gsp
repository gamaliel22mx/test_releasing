<%@page import="com.verecloud.nimbus4.service.enums.ChannelRole"%>
<%@page import="com.verecloud.nimbus4.service.enums.PartyStatus"%>
<%@ page import="com.verecloud.nimbus4.party.Group" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
    <title><g:message code="group.details.title" default="Group Details - ${group?.name}"  /></title>
    <style type="text/css">

    .bodyDiv{
        padding:10px;
        border-radius:5px;
        box-shadow:3px 2px 2px 5px #eee inset;
        background:#fff;
        margin: 5px;
    }
	#Menu{
	visibility:hidden;
	}
    .pure-control-group {
        padding: 5px;
    }

    .formLabel {
        color: #666666;
        text-align: right;
        width: 35%;
        display: inline-block;
    }

    .formValue {
        color: #666666;
        text-align: left;
        width: 60%;
        float: right;
    }

    .required {
        color: red;
    }

    .side-content {
        width: 25%;
        display: inline-block;
        padding: 10px;
    }

    .main-content {
        text-align: left;
        background-color: #eee;
    <%--	border: 1px solid inactiveborder;--%>
        box-shadow:3px 2px 2px 5px #eee inset;
        width: 75%;
        float: right;
        padding: 10px;
        height: auto;
    }

    div.actionsNewGroupServiceInstanceDI {
        display: inline-block;
        width: 25%;
        padding:10px
    }
    div#commonServiceSelected {
        border-style: groove;
        text-align: left;
        float:right;
        width: 70%;
    }

    div#newSsProfile {
        width: 25%;
        display: inline-block;
        padding: 10px;
    }

    div#serviceSettings {
        border-style: groove;
        text-align: center;
        float: right;
        width: 75%;
    }
    </style>

</head>
<body style="vertical-align: middle;">
<div class="bodyDiv" >
    <div class="col-md-3" style="align:left;width:70%;padding-left: 10%;padding-top:40px;padding-right:10%;display:inline-block;" >
        <p>
            <span style="font-weight:bold;">Group ID:</span> <span>${group?.externalId}</span>
        </p>
        <p>
            <span style="font-weight:bold;">Channel Role:</span> <span id="groupRole">${group?.role}</span>
        </p>
        <p>
            <span style="font-weight:bold;">Parent Id:</span>
            <%-- TODO check if multiple parents exists or not? If yes, below code needs to be modified --%>
            <span id="groupName">
             <g:if test="${group?.role == ChannelRole.VAR || group?.role == ChannelRole.DISTRIBUTOR}"> - </g:if>
              <g:else> ${group?.parent?.name} </g:else>
            </span>
        </p>
        <p>
            <span style="font-weight:bold;">Language:</span> <span id="groupLanguage">${group?.language?.encodeAsHTML()}</span>
        </p>
    </div>

    <%--<div class="groupServices">       --%>
    <div  class="col-md-6" style="width:30%;align:right;border:solid 1px;text-align:center;display:inline-block;background-color:#E8E8E8 ;">
        <g:if test="${group?.status==PartyStatus.ACTIVE}">
            <h5 style="background-color:#E8E8E8 ;padding-top: 5px;padding-bottom: 5px;">${group?.status}</h5>
            <p>
                <g:link class="btn btn-primary btn-xs" controller="group" action="edit" id="${group?.id}"><g:message code="group.edit.label" default="Edit Group"  /></g:link>
            </p>
            <p>
                <g:link class="btn btn-primary btn-xs" controller="group" action="edit" id="${group?.id}"><g:message code="group.edit.label" default="Deactivate Group"  />
                <%--TODO handle deactivate group - change status of group from active to inactive--%>
                </g:link>
            </p>
        </g:if>
    </div>
    <div class="col-md-6" style="width:30%;align:right;border:solid 1px;text-align: center;background-color:#E8E8E8;">
        <g:if test="${group?.status==PartyStatus.ACTIVE}">
        <%--TODO verify if thse options should be displayed only for ATICV GROUPs- theoretically it make sense only for active status but doublcheck again legacy code --%>
            <h5 style="background-color:#E8E8E8;padding-top: 5px;padding-bottom: 5px;border-top: 10px;">Order New Services</h5>
            <p>
                <%--		<g:link class="btn btn-primary btn-xs" controller="group" action="test" id="${group?.id}"><g:message code="group.recordOnly.label" default="Record Only" />--%>
                <a class="btn btn-primary btn-xs" onclick="showServices();">
                    <g:message code="group.recordOnly.label" default="Record Only" />
                </a>
                <%-- TODO handle Record Only similar to legacy; form a message and add it to the queue to send it to bacnkend and handle to message response accordingly
Start with Record only service first
--%>
            </p>
            <p>
                <g:link class="btn btn-primary btn-xs" controller="groupDetails" action="provisionService" id="${group?.id}"><g:message code="group.provision.label" default="Provision"  />
                </g:link>
            </p>
            <%--
            DO NOT DELETE THIS SECTION -- THIS NEEDS TO BE UNCOMMENTED ONCE PROVISION AND DISCOVERY LOGIC IS FULLY IMPLEMENTED IN THE BACKEND
            <p>
                <g:link class="btn btn-primary btn-xs" controller="groupDetails" action="provisionService" id="${group?.id}"><g:message code="group.provision.label" default="Provision"  />
                </g:link>
            </p>
            <p>

                <a class="btn btn-primary btn-xs" onclick="showCommonDiscServices();">
                    <g:message code="group.discovery.label" default="Discovery" />
                     TODO handle Discovery service Only similar to legacy; form a message and add it to the queue to send it to bacnkend and handle to message response accordingly  
                </a>
            </p>
        	--%>
        </g:if>
    </div>
    <%--</div>--%>

    <div id="tabs">
        <%--<ul class="nav navbar-nav" id="tabs" style="border-bottom: 1px solid #CCCCCC;clear: both;display:block;height: 40px;margin-bottom:10px;margin-top: 10px;border: 1px solid;background-color: #E8E8E8;">--%>
        <ul  class="nav navbar-nav" id="tabs" style="border-bottom: 1px solid #87e2f4;clear: both;display:block;height: 40px;margin-bottom:10px;margin-top: 10px;background-color: #E8E8E8;width:100%">
            <%--TODO below urls should be changed according to the logic --%>
            <li><a id="tabUsers" onclick="populateUsers();">Users</a></li>
            <li><a id="tabServices" href="#servicesDiv" onclick="populateServices();showCommonServices();">Services</a></li>
<%--            <li><a id="tabServices" onclick="populateServices();">Services</a></li> --%>
            <li><a id="orderHistoryNav" href="#ordersDiv" onclick="showOrders();">Order History</a></li>
            <%--    <li><a id="previewNav" href="#tabs-4">Preview</a></li> TODO this is not reuired for nimbus35--%>
        </ul>
    </div>

    <div id="usersDiv" class="side-content">
    
    </div>

    <div id="servicesDiv" class="side-content">
        %{--//TODO:GSI is removed in latest pull.Need to think about alternate--}%
    %{--    <table class="table table-bordered margin-top-medium">
            <thead>
                <g:if test="${ssvSet}">
                    <th class="center">Click on the link to edit a service!!!</th>
                </g:if>
                <g:else>
                <th class="center">No Service exists!!</th>
                </g:else>
            </thead>
            <tbody>
               		<g:each in="${ssvSet}" status="i" var="gsiSpec">
                    <tr class="${(i % 2) == 0 ? 'odd' : 'even'}" >
                        <td class="center">
                        fds
						<a id="${gsiSpec?.id}" onclick="populateGsiServices('${gsiSpec?.id}');">
							*${gsiSpec?.name}
						</a>
                        </td>
                    </tr>
                </g:each>
            </tbody>
        </table>
--}%        
    </div>

    <div id="ordersDiv" class="side-content">
        <g:set var="orders" value="${com.verecloud.nimbus4.product.ProductOrder.findAllByTenantId(group?.id?.toInteger)}"/>
        <table class="table table-bordered margin-top-medium">
            <thead>
            <g:if test="${orders}">
                <th class="center">Click on the link to edit a Product</th>
            </g:if>
            <g:else><th class="center">No Product Exists</th></g:else>
            </thead>
            <tbody>
            <g:each in="${orders}" status="i" var="productOrder">
                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}" >
                    <td class="center"><g:link>${productOrder?.productSpecification?.name}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
        <p  style="text-align:center;">
            <g:link class="btn btn-primary btn-xs" controller="productOrder" action="create">Add New Order</g:link>
        </p>
    </div>

    <div class="main-content">
        <g:render template="commonServices" />
        <div id="gsiServices">..</div>
        Serve your customers by managing their users on their behalf.
    </div>
    <br class="clear"/>
</div>
<script type="text/javascript">
    function test(){
        console.log("inside test");
        return confirm('At this point we should to message queue which will be processed by backend. Hit cancel now');
    }
    $("#testAj").click(function(){
        alert('working');
    });
    $(document).ready(function() {
        $(".commonServices").hide();
        $(".commonDiscoveryServices").hide();
        $("#servicesDiv").hide();
        $("#ordersDiv").hide();
        $("#usersDiv").show();
        $("ul[id^='Menu']").hide();
        $("div[class^='commonSelected']").hide();
        populateUsers();
    });


    $("#tabServices").click(function () {
        //not working in chrome probably jquery plugin is missing in kickstart
        console.log('showServices jquery');
        $("#usersDiv").hide();
        $("#ordersDiv").hide();
        $("#servicesDiv").show();
    });
    $('#tabUsers').click(function () {
        //not working in chrome
        console.log('showUsers jquery');
        $("#servicesDiv").hide();
        $("#ordersDiv").hide();
        $("#usersDiv").show();
    });
    function showServices(){
        var root="${resource()}";
        var groupId = '${group?.id}';
    	var url = root+'/groupDetails/getAllActiveServiceSpecsList?groupId='+groupId;
    	$(".commonDiscoveryServices").hide();
    	$(".commonServices").show();
        jQuery('.commonServices').load(url);
        return;
    }
    function showUsers(){
        console.log('showUsers');
        $(".commonServices").hide();
        $(".commonServiceSelected").hide();
        $("#servicesDiv").hide();
        $("#ordersDiv").hide();
        $(".commonDiscoveryServices").hide();
        $("#usersDiv").show();
    }
    function showOrders(){
        console.log('showOrders');
        $(".commonServices").hide();
        
        $("#usersDiv").hide();
        $("#servicesDiv").hide();
        $(".commonDiscoveryServices").hide();
        $("#ordersDiv").show();
    }
    function showCommonServices(){
        $(".commonDiscoveryServices").hide();
        $(".commonServices").empty();
        $(".commonServices").show();
    }
    function showCommonDiscServices(){
        $(".commonServices").hide();
        $(".commonDiscoveryServices").show();
    }

    function showSettings(inputClass){
        alert("showsettings inputClass ="+inputClass.replace(/\s+/g, ''));
        <%--		$(":input[attribute^='commonSelected']").hide();--%>
        $("div[class^='commonSelected']").hide();
        $(".commonSelected"+inputClass.replace(/\s+/g, '')).show();

    }
	function populateUsers(groupId){
		 $("#gsiServices").hide();
		var groupId = '${group?.id}'; 
        var root="${resource()}";
        var url = root+'/groupDetails/usersList?id='+groupId;
        jQuery('#usersDiv').load(url);
        return;
    }
	function populateServices(groupId){
		console.log('showServices');
        $(".commonServices").hide();
        $(".commonServiceSelected").hide();
        $(".commonDiscoveryServices").hide();
        $("#usersDiv").hide();
        $("#ordersDiv").hide();
        $("#servicesDiv").show();
        $("#gsiServices").empty();
        $("#gsiServices").show();
		var groupId = '${group?.id}'; 
        var root="${resource()}";
        var url = root+'/groupDetails/servicesList?id='+groupId;
        jQuery('#servicesDiv').load(url);
        return;
    }
	function populateGsiServices(id){
		console.log('showServices');
        $(".commonServices").hide();
        $(".commonServiceSelected").hide();
        $(".commonDiscoveryServices").hide();
        $("#usersDiv").hide();
        $("#ordersDiv").hide();
        $("#servicesDiv").show();
        $("#gsiServices").show();
		var groupId = '${group?.id}';
		var specId = id;
		alert('test id = '+specId); 
        var root="${resource()}";

        var url = root+'/groupDetails/getAllGSIsForSSV?id='+specId+'&groupId='+groupId;
        jQuery('#gsiServices').load(url);
        return;
    }
<%--    function addDiscovery(){
        var results
  		var data = JSON.stringify($('#discoveryForm').serializeArray());
  		alert(data);
        
        $.post("${createLink(controller: 'groupDetails',action: 'processDiscovery')}",$('#discoveryForm').serialize(),function(data){
            $('#commonServiceSelected').html(data)
        });
        
    }
    function addNewGSIForRecordOnly(ssvId, groupId){
        $.post("${createLink(controller: 'groupDetails',action: 'processRecordOnly')}",$('#recordOnlyForm').serialize(),function(data){
           $('#serviceSettings').html(data)
        });
    }
--%>
//add simpleMany items
function addNewItem(ulName,liNameInput,type){
	var divId = ulName+"Div";
	var liIndex = $("#"+ulName+" li").length;
	var liName = ulName +liIndex;
	$("ul#"+ulName).append("<li id="+liName+"><input type="+type+"  name="+ulName+" value='' id="+liName+"> <a id="+liName+" onclick='removeLi();'>Remove</a></li>");
	return;
}

function addNewComplexManyItem(ulName){
	var inputUlName = '#'+ulName;
	var liCount = $(inputUlName+" li").length;
	var newId = ulName+liCount;
	var $gallery = $(inputUlName);
	//clone the first li 
	$gallery.find('li:first').clone(true).appendTo($gallery);
	
	//remane id of recently added li - append count so it will be unique
	$("ul."+ulName+" > li:last-child").attr("id",newId);
	$("ul."+ulName+" > li:last-child a").attr("id",newId);
	
	//remane id of 'remove' link for recently added li - so it matches with enclosed li.
	$("a#"+newId).removeAttr('hidden');
	
}

function removeLi(){
	$(document).on('click', 'a', function () {
		var id=this.id;
	    $("#"+id).remove();    
	});	
}


</script>
</body>
</html>
