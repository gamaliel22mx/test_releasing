<div class="formValue">
<ul class='data childData' id="${serviceAttribute?.name}">
	<g:each in="${serviceAttribute?.childAttributes?.entrySet()}" var="child" status="i">
     				<g:if test="${child?.getValue()?.orderEntryVisibility == 'hidden' || serviceAttribute?.orderEntryVisibility == 'hidden'}">
	        			<li class="childData">
	                    		<g:hiddenField class="data formValue readOnly" type="${validatorStr}" name="${child?.getValue()?.name}" />
	                    </li>		
	                </g:if>
	                <g:else>
	                <li class="childData ${required?'required':''}">
	                ${child?.getValue()?.name}:
     					<g:if test="${child?.getValue()?.possibleValues=='true'}">
	                       <select class="data" name="${child?.getValue()?.name}">
	                        	<g:each in="${child?.getValue()?.possibleValues?.staticValues}" var="staticList" status="j">
					             <option label="${staticList.getKey()}" value="${staticList.getValue()}"></option>
					             </g:each>
	                          </select>
	                      </g:if>
	                     <g:else>
	                   		<g:if test="${required}">
	                    				<g:field class="data formValue required" type="${validatorStr}" name="${child?.getValue()?.name}" required="true"/>
	                   		</g:if>
	                   		<g:else>
	                    			<g:field class="data formValue " type="${validatorStr}" name="${child?.getValue()?.name}" />
                   			</g:else>
	                    </g:else>
     					</li>
	      			</g:else>
    </g:each>
</ul>
</div>