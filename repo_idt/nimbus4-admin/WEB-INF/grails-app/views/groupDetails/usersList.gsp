<%@ page import="com.verecloud.nimbus4.party.User"%>
<html>
<body>
    <table class="table table-bordered margin-top-medium">
        <thead>
            <g:if test="${group?.users}">
                <th class="center">Click to edit a User</th>
            </g:if>
            <g:else><th>No Users exists</th></g:else>
            <tbody>
            <g:each in="${group?.users}" status="i" var="user">
                <tr class="${(i % 2) == 0 ? 'odd' : 'even'}" >
                    <td class="center">
                        <g:link controller="user" action="edit" params="[id:user?.id]">${user.firstName},${user.lastName} - <g:if test="${user?.admin==true}">Admin</g:if><g:else> Not and admin</g:else>
                        </g:link></td></tr>
            </g:each>
            </tbody>
        </table>
        <p  style="text-align:center;">
            <g:link class="btn btn-primary btn-xs" controller="user" action="create">Add New User</g:link>
        </p>
    <script>
        function addNewGSIForRecordOnly(ssvId, groupId){
            $.post("${createLink(controller: 'groupDetails',action: 'processRecordOnly')}",$('#recordOnlyForm').serialize(),function(data){
               $('#serviceSettings').html(data)
            });
        }
    </script>
    </body>
    </html>
