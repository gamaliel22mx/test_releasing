<%@ page import="com.verecloud.nimbus4.party.Group"%>

<div class="commonServices" id="commonServices">
	
</div>
<div class="commonDiscoveryServices">
	<!-- start create Group For DISCOVERY -->
	<h5>Discover a new instance</h5>
	<div class="actionsNewGroupServiceInstanceDI">
		<div id="servicesListBox" class="listBox">
		<ul>
			<g:each in="${servicesWithDiscovery}" status="i" var="serviceSpec">

				<li><g:remoteLink id="${serviceSpec?.id}" params="[groupId:group?.id]" action="loadDiscoverAttributes"
							oncomplete="showSpinner(false);" onloading="showSpinner(true);"
							update="commonServiceSelected" class="displayServiceSettings">
							${serviceSpec?.name}
						</g:remoteLink>
				</li>
			</g:each>
		</ul>

	</div>
	</div>
	<div id="commonServiceSelected">
	Select a Service!
	</div>
</div>
<div class="gsiServices">
	
</div>
