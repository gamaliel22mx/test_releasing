<%@ page import="com.verecloud.nimbus4.party.User"%>

    <table class="table table-bordered margin-top-medium">
        <thead>
            <g:if test="${ssvSet}">
                <th class="center">Click to edit a Service</th>
            </g:if>
            <g:else><th>No GSIs exists</th></g:else>
            <tbody>
            <g:each in="${ssvSet}" status="i" var="gsiSpec">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}" >
                    <td class="center">
                        <g:remoteLink id="${gsiSpec?.id}" controller="groupDetails" action="getAllGSIsForSSV" params="[groupId:groupId]"
                                                        oncomplete="showSpinner(false);" onloading="showSpinner(true);"
                                                        update="commonServices" class="displayServiceSettings">
                                                        ${gsiSpec?.name}
                                                </g:remoteLink>
                        <%--
                                <g:link controller="groupDetails" action="getAllGSIsForSSV" params="[id:gsiSpec?.id,groupId:groupId]" onclick="showCommonServices();">${gsiSpec?.name}</g:link>
                        --%>

                     </td>
            </tr>
            </g:each>

            </tbody>
        </table>