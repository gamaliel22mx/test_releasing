package com.verecloud.nimbus4

import com.verecloud.nimbus4.common.ApplicationContextHolder
import com.verecloud.nimbus4.party.Group
import com.verecloud.nimbus4.party.GroupAddress
import com.verecloud.nimbus4.party.User
import com.verecloud.nimbus4.party.UserGroupRole
import com.verecloud.nimbus4.util.Nimbus4Constants
import grails.plugin.multitenant.core.ChildTenant
import grails.plugin.multitenant.core.CurrentTenant
import org.springframework.context.ApplicationContext
import org.springframework.context.i18n.LocaleContextHolder as LCH

class ApplicationFilters {

	def filters = {

		ApplicationContext context = ApplicationContextHolder.applicationContext
		CurrentTenant currentTenant = ApplicationContextHolder.currentTenant
		ChildTenant childTenant = ApplicationContextHolder.childTenant
		ApplicationTagLib nimbus = ApplicationContextHolder.getBean(ApplicationTagLib)

		multiTenantCreateEdit(controller: '*', action: 'create|edit') {
			before = {
				boolean isMultiTenant = nimbus.isMultiTenant([domainClass: controllerName])?.toString()?.equals('true')
				boolean isTenantNotSet = nimbus.isTenantNotSet([domainClass: controllerName])?.toString()?.equals('true')

				if (isMultiTenant && isTenantNotSet) {
					flash.message = context.getMessage("select.tenant", new Object[0], "default message", LCH.getLocale())
					redirect action: 'index'
					return false
				}
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}

		multiTenantRestriction(controller: '*', action: 'edit|update') {
			before = {
				boolean isMultiTenant = nimbus.isMultiTenant([domainClass: controllerName])?.toString()?.equals('true')
				if (isMultiTenant) {
					Class aClass = context.grailsApplication.domainClasses.find {
						it.name.equalsIgnoreCase(controllerName)
					}.clazz
					Integer currentTenantId = currentTenant.get()
					Group.withoutTenantRestriction {
						def instance = aClass.get(params.id as long)
						if (instance?.tenantId != currentTenantId) {
							flash.message = context.getMessage("tenant.mismatch", new Object[0], "default message", LCH.getLocale())
							redirect(controller: controllerName)
							return false
						}
					}
				}
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}

		customGroupHandling(controller: 'group', action: 'edit|update') {
			before = {
				Integer currentTenantId = currentTenant.get()
				if (params.id?.toInteger() != currentTenantId) {
					flash.message = context.getMessage("tenant.mismatch", new Object[0], "default message", LCH.getLocale())
					redirect(controller: controllerName, action: 'show', params: [id: params.id])
					return false
				}
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}

		customUserHandling(controller: 'user|userGroupRole', action: 'edit|update') {
			before = {
				Integer currentTenantId = currentTenant.get()
				UserGroupRole userGroupRole = null;
				if (controllerName == 'user') {
					User user = User.get(params.id?.toLong())
					Group group = Group.get(currentTenantId?.toLong())
					userGroupRole = (user && group) ? UserGroupRole.findWhere([user: user, tenantId: group?.id?.toInteger()]) : null;
				} else {
					userGroupRole = UserGroupRole.get(params.id?.toLong())
					userGroupRole = (userGroupRole?.group?.id?.toInteger() in childTenant.get()) ? userGroupRole : null;
				}
				if (!userGroupRole) {
					flash.message = context.getMessage("tenant.mismatch", new Object[0], "default message", LCH.getLocale())
					redirect(controller: controllerName, action: 'show', params: [id: params.id])
					return false
				}
			}
		}

		groupDeletionHandling(controller: 'group', action: 'delete') {
			before = {
				Integer parentTenantId = session.getAttribute("parentTenantId")?.toString()?.toInteger()
				if (parentTenantId == params.id?.toInteger()) {
					flash.message = context.getMessage("tenant.self.delete", new Object[0], "default message", LCH.getLocale())
					redirect action: 'show', params: [id: params.id]
					return false
				}
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}

		handleAnonymousUser(controller: 'user', action: 'edit|update|delete') {
			before = {
				User user = User.get(params.id as long)

				if (Nimbus4Constants.ANONYMOUS_USER.equalsIgnoreCase(user?.email)) {
					flash.message = context.getMessage("user.anonymous", new Object[0], "default message", LCH.getLocale())
					redirect action: 'show', params: [id: params.id]
					return false
				}
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}

		groupAddressCheck(controller: 'groupAddress', action: 'create|save') {
			before = {
				Group group = Group.read(currentTenant.get())
				GroupAddress groupAddress = group.groupAddress
				if (groupAddress) {
					flash.message = context.getMessage("group.address.already.exist", [groupAddress, group] as Object[], "default message", LCH.getLocale())
					redirect action: 'show', params: [id: groupAddress.id]
					return false
				}
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}
	}
}
