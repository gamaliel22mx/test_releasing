<%=packageName%>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<section id="show-${domainClass.propertyName}" class="first">

	<table class="table">
		<tbody>
		<%
			final List<String>  excludedProps = com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.SHOW_GSP_EXCLUDED_PROPS
			List<String> allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
			List<org.codehaus.groovy.grails.commons.GrailsDomainClassProperty> props = domainClass.properties.findAll {
				(
						(
								(
										allowedNames.contains(it.name)
												&& (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true)
								) || (
										domainClass.constrainedProperties[it.name]?.display
								)
						) && !excludedProps.contains(it.name)
				)
			}

			boolean showTenantId = domainClass.properties.any { it.name.equals("tenantId") }
			if(showTenantId){%>
		<tr class="prop">
			<td valign="top" class="name"><g:message code="${domainClass.propertyName}.tenantId.label" default="Tenant" /></td>

			<td valign="top" class="value"><g:link controller="group" action="show" id="\${${propertyName}?.tenantId}">\${com.verecloud.nimbus4.party.Group.findById(${propertyName}?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
		</tr>
			<%}

			Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
			props.each { p -> %>
		<tr class="prop">
			<td valign="top" class="name"><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></td>
			<% if (p.isEnum()) { %>
			<td valign="top" class="value">\${${propertyName}?.${p.name}?.encodeAsHTML()}</td>
			<% } else if (p.oneToMany || p.manyToMany) { %>
			<td valign="top" style="text-align: left;" class="value">
				<ul><% renderHasManyRelationship(propertyName, p); %></ul>
			</td>
			<% } else if (p.manyToOne || p.oneToOne) { %>
			<td valign="top" class="value"><g:link controller="\${${propertyName}?.${p.name}?.class?.simpleName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></td>
			<% } else if (p.type == Boolean || p.type == boolean) { %>
			<td valign="top" class="value"><g:formatBoolean boolean="\${${propertyName}?.${p.name}}"/></td>
			<%  } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
			<td valign="top" class="value"><g:formatDate date="\${${propertyName}?.${p.name}}"/></td>
			<%  } else if (domainClass?.constrainedProperties[p.name] && domainClass?.constrainedProperties[p.name]?.widget == 'ace') { %>
			<td valign="top" class="value">
				${com.verecloud.nimbus4.util.Nimbus4CoreUtil.createAce(domainClass, p, true)}
			</td>

			<% } else if (p.type == BigDecimal) { %>
			<td valign="top" class="value">
				<% if ((domainClass?.constrainedProperties[p.name] && domainClass?.constrainedProperties[p.name]?.scale)) { %>
				<g:formatNumber number="\${${propertyName}?.${p.name}}" type="number" maxFractionDigits="${domainClass?.constrainedProperties[p.name].scale}" />
				<% } else { %>
				\${fieldValue(bean: ${propertyName}, field: "${p.name}")}
				<% } %>
			</td>
			<% } else if (!p.type.isArray()) { %>
			<td valign="top" class="value">\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
			<% } %>
		</tr>
		<% } %>
		</tbody>
	</table>
</section>
${com.verecloud.nimbus4.util.Nimbus4CoreUtil.setupAceConfig(domainClass, props, true)}
</body>

</html>
<% private void renderHasManyRelationship(propertyName, p) { %>
<g:each in="\${${propertyName}?.${p.name}}" var="${p.name[0]}">
	<g:if test="\${${p.name[0]}}">
		<li>
			<g:if test="\${${p.name[0]}?.class?.enclosingClass}">
				<g:if test="\${javassist.util.proxy.ProxyFactory.isProxyClass(${p.name[0]}?.value?.class)}">
					<g:link
							controller="\${${p.name[0]}?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.value?.id}">
						\${${p.name[0]}?.key?.encodeAsHTML()+" : "+${p.name[0]}?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="\${${p.name[0]}?.value?.class?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.value?.id}">
						\${${p.name[0]}?.key?.encodeAsHTML()+" : "+${p.name[0]}?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="\${javassist.util.proxy.ProxyFactory.isProxyClass(${p.name[0]}?.class)}">
					<g:link
							controller="\${${p.name[0]}?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.id}">
						\${${p.name[0]}?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="\${${p.name[0]}?.class?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.id}">
						\${${p.name[0]}?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
<% } %>