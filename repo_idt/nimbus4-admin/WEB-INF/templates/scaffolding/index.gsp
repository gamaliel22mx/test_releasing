<%=packageName%>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart"/>
	<g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
	<title><g:message code="default.index.label" args="[entityName]"/></title>
</head>

<body>

<section id="index-${domainClass.propertyName}" class="first">
	<div id="horizontalScroll">
		<table class="table table-bordered margin-top-medium">
			<thead>
			<tr>
				<%
					final List<String>  excludedProps = com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.LIST_GSP_EXCLUDED_PROPS
					List<String> allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
					List<org.codehaus.groovy.grails.commons.GrailsDomainClassProperty> props = domainClass.properties.findAll {
						(
								allowedNames.contains(it.name)
										&& !excludedProps.contains(it.name)
										&& !Collection.isAssignableFrom(it.type)
										&& (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true)
						)
					}

					boolean showTenantId = domainClass.properties.any { it.name.equals("tenantId") }
					Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
				%>
				<th>\${entityName}</th>
				<%
					props.eachWithIndex { p, i ->
						if (p.isAssociation()) { %>
				<th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></th>
				<% } else { %>
					<g:sortableColumn property="${p.name}" title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${p.naturalName}')}" params="[query: searchCO?.query, searchField: searchCO?.searchField]"/>
				<% }
				}
				if(showTenantId){%>
				<th><g:message code="${domainClass.propertyName}.tenantId.label" default="Tenant"/></th>
				<%}%>
			</tr>
			</thead>
			<tbody>
			<g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
				<tr class="\${(i % 2) == 0 ? 'odd' : 'even'}">
					<td><g:link action="show" id="\${${propertyName}.id}">\${${propertyName}}</g:link></td>
					<% props.eachWithIndex { p, i ->
						if (p.oneToMany || p.manyToMany) { %>
					<td valign="top" style="text-align: left;" class="value">
						<ul><% renderHasManyRelationship(propertyName, p); %></ul>
					</td>
					<% } else if (p.manyToOne || p.oneToOne) { %>
					<td valign="top" class="value"><g:link controller="\${org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil.unwrapIfProxy(${propertyName}?.${p.name})?.class?.simpleName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></td>
					<% } else if (p.type == Boolean || p.type == boolean) { %>
					<td><g:formatBoolean boolean="\${${propertyName}.${p.name}}"/></td>
							<% } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
					<td><g:formatDate date="\${${propertyName}.${p.name}}"/></td>
					<% } else if (p.type == BigDecimal) { %>
					<td>
						<% if ((domainClass?.constrainedProperties[p.name] && domainClass?.constrainedProperties[p.name]?.scale)) { %>
                        <g:formatNumber number="\${${propertyName}?.${p.name}}" type="number" maxFractionDigits="${domainClass?.constrainedProperties[p.name].scale}" />
						<% } else { %>
						\${fieldValue(bean: ${propertyName}, field: "${p.name}")}
						<% } %>
					</td>
					<% } else { %>
					<td>\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
					<% }
					}
					if(showTenantId){%>
					<td valign="top" class="value"><g:link controller="group" action="show" id="\${${propertyName}?.tenantId}">\${com.verecloud.nimbus4.party.Group.findById(${propertyName}?.tenantId?.toLong())?.encodeAsHTML()}</g:link></td>
					<%} %>
				</tr>
			</g:each>
			</tbody>
		</table>
	</div>

	<div>
		<bs:paginate total="\${${propertyName}Count}" params="[query: searchCO?.query, searchField: searchCO?.searchField]" max="\${searchCO?.max}" sort="\${searchCO?.sort}" order="\${searchCO?.order}"/>
	</div>
</section>

</body>

</html>
<% private void renderHasManyRelationship(propertyName, p) { %>
<g:each in="\${${propertyName}?.${p.name}}" var="${p.name[0]}">
	<g:if test="\${${p.name[0]}}">
		<li>
			<g:if test="\${${p.name[0]}?.class?.enclosingClass}">
				<g:if test="\${javassist.util.proxy.ProxyFactory.isProxyClass(${p.name[0]}?.value?.class)}">
					<g:link
							controller="\${${p.name[0]}?.value?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.value?.id}">
						\${${p.name[0]}?.key?.encodeAsHTML()+" : "+${p.name[0]}?.value?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="\${${p.name[0]}?.value?.class?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.value?.id}">
						\${${p.name[0]}?.key?.encodeAsHTML()+" : "+${p.name[0]}?.value?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:if>
			<g:else>
				<g:if test="\${javassist.util.proxy.ProxyFactory.isProxyClass(${p.name[0]}?.class)}">
					<g:link
							controller="\${${p.name[0]}?.class?.genericSuperclass?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.id}">
						\${${p.name[0]}?.encodeAsHTML()}
					</g:link>
				</g:if>
				<g:else>
					<g:link
							controller="\${${p.name[0]}?.class?.simpleName}"
							action="show"
							id="\${${p.name[0]}?.id}">
						\${${p.name[0]}?.encodeAsHTML()}
					</g:link>
				</g:else>
			</g:else>
		</li>
	</g:if>
</g:each>
<% } %>