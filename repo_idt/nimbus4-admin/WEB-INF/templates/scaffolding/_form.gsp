<%=packageName%>

<%
	class DefaultEmbeddedGrailsDomainClassProperty {
		@groovy.lang.Delegate
		org.codehaus.groovy.grails.commons.GrailsDomainClassProperty defaultGrailsDomainClassProperty

		String embeddedName
		boolean isCurrentTenantFilter
		boolean isParentOrSupplierTenantFilter
	}

	final List<String>  excludedProps = com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.FORM_GSP_EXCLUDED_PROPS
	List<String> persistentPropNames = domainClass.persistentProperties*.name
	boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
	if (hasHibernate) {
		Class<org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder> grailsDomainBinder = getClass().classLoader.loadClass('org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder')
		if (grailsDomainBinder.newInstance().getMapping(domainClass)?.identity?.generator == 'assigned') {
			persistentPropNames << domainClass.identifier.name
		}
	}
	List<org.codehaus.groovy.grails.commons.GrailsDomainClassProperty> props = domainClass.properties.findAll {
		(
				(
						(
								persistentPropNames.contains(it.name)
										&& (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true)
						) || (
								domainClass.constrainedProperties[it.name]?.display
						)
				) && !excludedProps.contains(it.name)
		)
	}
	Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
	boolean hasMapProperty = false
	boolean hasAceProperty = false
	String propertyName
	String aceEditor
	for (p in props) {
		if (p.embedded) {
			String domainSuffix = "Instance"
			String domainInstance = domainClass.propertyName + domainSuffix

%><g:set var="${p?.typePropertyName}${domainSuffix}" value="\${${domainInstance}?.${p?.name}}"/><%
		def embeddedPropNames = p.component.persistentProperties*.name
		def embeddedProps = p.component.properties.findAll {
			embeddedPropNames.contains(it.name) && !excludedProps.contains(it.name)
		}
		Collections.sort(embeddedProps, comparator.constructors[0].newInstance([p.component] as Object[]))
%>
<fieldset class="embedded"><legend><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></legend>
	<%
			for (ep in p.component.properties) {
				DefaultEmbeddedGrailsDomainClassProperty emProp = new DefaultEmbeddedGrailsDomainClassProperty()
				emProp.defaultGrailsDomainClassProperty = ep
				emProp.embeddedName = "${p.name}.${ep.name}"
				renderFieldForProperty(emProp, p.component, "${p.name}.")
			}
	%></fieldset><%
		} else {
			Closure getDomainClass = {
				String className ->
					org.codehaus.groovy.grails.commons.GrailsClass grailsClass = domainClass.grailsApplication.getDomainClasses().find {
						org.codehaus.groovy.grails.commons.GrailsClass grailsClass ->
							grailsClass.name.equalsIgnoreCase(className)
					}
					return grailsClass?.clazz
			}
			boolean isCurrentPropertyTypeGroup = p?.referencedDomainClass?.fullName?.equals(com.verecloud.nimbus4.party.Group.class.name)
			boolean isCurrentDomainMultiTenant = grails.plugin.multitenant.core.MultiTenantDomainClass.isAssignableFrom(domainClass.clazz)

			boolean renderFieldAsTenant = false
			boolean isCurrentPropertyDomainMultiTenant = false
			boolean renderFieldAsCurrentTenantDataOnly = false
			boolean renderFieldAsParentOrSupplierTenantDataOnly = false
			boolean renderFieldAsParentOrSupplierGroupOnly = false
			boolean renderFieldAsCurrentTenantChildGroup = false
			boolean isCurrentPropertyDomainGroup = false
			boolean showTenantId = p.name.equals("tenantId")

			if (p.referencedDomainClass?.clazz) {
				isCurrentPropertyDomainMultiTenant = grails.plugin.multitenant.core.MultiTenantDomainClass.isAssignableFrom(p.referencedDomainClass.clazz)
				isCurrentPropertyDomainGroup = com.verecloud.nimbus4.party.Group.isAssignableFrom(p.referencedDomainClass.clazz)
			} else if (!p.persistent) {
				def domainClass = getDomainClass(p.referencedPropertyType.simpleName)

				if (domainClass) {
					isCurrentPropertyDomainMultiTenant = grails.plugin.multitenant.core.MultiTenantDomainClass.isAssignableFrom(p.domainClass.clazz)
					isCurrentPropertyDomainGroup = com.verecloud.nimbus4.party.Group.isAssignableFrom(p.domainClass.clazz)
				}
			}

			renderFieldAsCurrentTenantDataOnly = isCurrentPropertyDomainMultiTenant && com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.CURRENT_TENANT_DATA_ONLY.get(domainClass.name)?.contains(p.name)
			renderFieldAsParentOrSupplierTenantDataOnly = isCurrentPropertyDomainMultiTenant && com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.PARENT_OR_SUPPLIER_TENANT_DATA_ONLY.get(domainClass.name)?.contains(p.name)
			renderFieldAsParentOrSupplierGroupOnly = isCurrentPropertyDomainGroup && com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.PARENT_OR_SUPPLIER_TENANT_DATA_ONLY.get(domainClass.name)?.contains(p.name)

			if (isCurrentDomainMultiTenant && isCurrentPropertyTypeGroup) {
				renderFieldAsTenant = com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.CURRENT_TENANT_AS_GROUP.get(domainClass.name)?.contains(p.name)
				renderFieldAsCurrentTenantChildGroup = com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.CURRENT_TENANT_CHILD_GROUPS_ONLY.get(domainClass.name)?.contains(p.name)
			}

			if(showTenantId){
				renderTenantField(p)
			}else if (renderFieldAsTenant) {
				renderTenantField(p, domainClass)
			} else if (renderFieldAsCurrentTenantChildGroup) {
				renderCurrentTenantChildGroupField(p, domainClass)
			} else if (renderFieldAsParentOrSupplierGroupOnly) {
				renderParentOrSupplierGroupField(p, domainClass)
			} else {
				DefaultEmbeddedGrailsDomainClassProperty emProp = new DefaultEmbeddedGrailsDomainClassProperty()
				emProp.defaultGrailsDomainClassProperty = p
				emProp.embeddedName = "${p.name}"
				emProp.isCurrentTenantFilter = renderFieldAsCurrentTenantDataOnly
				emProp.isParentOrSupplierTenantFilter = renderFieldAsParentOrSupplierTenantDataOnly
				renderFieldForProperty(emProp, domainClass)
			}
		}

		if (!hasMapProperty && p.referencedPropertyType.isAssignableFrom(Map)) {
			hasMapProperty = true
			propertyName = p.name
		}
		aceEditor = domainClass?.constrainedProperties[p.name]?.widget
		if (aceEditor == 'ace') {
			hasAceProperty = true
			hasMapProperty = false
		}
	}

	if (hasMapProperty) {
		renderJSforMapHandling(propertyName)
	}

	if (hasAceProperty) {
%>
${com.verecloud.nimbus4.util.Nimbus4CoreUtil.setupAceConfig(domainClass, props, false)}
<%
	}
	private renderTenantField(p) {
		%>
<div class="\${hasErrors(bean: ${propertyName}, field: '${p.name}', 'error')} }">
	<label for="${p.name}" class="control-label"><g:message code="${domainClass.propertyName}.${p.name}.label" default="Tenant"/></label>

	<div>
		<g:textField class="form-control" disabled="" name="${p.name}.name" value="\${nimbus.findCurrentTenantGroup()}"/>
	</div>
</div>
<%
	}

	private renderTenantField(p, owningClass, prefix = "") {
		boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
		boolean required = false
		if (hasHibernate) {
			cp = owningClass.constrainedProperties[p.name]
			required = (cp ? !(cp.propertyType in [boolean, Boolean]) && !cp.nullable && (cp.propertyType != String || !cp.blank) : false)
		} %>
<div class="\${hasErrors(bean: ${propertyName}, field: '${prefix}${p.name}', 'error')} ${required ? 'required' : ''}">
	<label for="${prefix}${p.name}" class="control-label"><g:message code="${domainClass.propertyName}.${prefix}${p.name}.label" default="${p.naturalName}"/> <% if (required) { %><span class="required-indicator">*</span><% } %></label>

	<div>
		<g:if test="\${${propertyName}?.${prefix}${p.name}}">
			<g:textField class="form-control" disabled="" name="${prefix}${p.name}.name" value="\${${propertyName}.${prefix}${p.name}.collect{"\${it.name}(\${it.role.name()})"}.first()}"/>
			<g:hiddenField class="form-control" name="${prefix}${p.name}.id" value="\${${propertyName}.${prefix}${p.name}.id}"/>
		</g:if>
		<g:else>
			<g:textField class="form-control" disabled="" name="${prefix}${p.name}.name" value="\${nimbus.findTenant()}"/>
			<g:hiddenField class="form-control" name="${prefix}${p.name}.id" value="\${nimbus.findTenantId()}"/>
		</g:else>
	</div>
</div>
<%
	}

	private renderCurrentTenantChildGroupField(p, owningClass, prefix = "") {
		String domainSuffix = "Instance"
		String domainInstance = owningClass.propertyName + domainSuffix

		boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
		boolean required = false
		if (hasHibernate) {
			cp = owningClass.constrainedProperties[p.name]
			required = (cp ? !(cp.propertyType in [boolean, Boolean]) && !cp.nullable && (cp.propertyType != String || !cp.blank) : false)
		} %>
<div class="\${hasErrors(bean: ${propertyName}, field: '${prefix}${p.name}', 'error')} ${required ? 'required' : ''}">
	<label for="${prefix}${p.name}" class="control-label"><g:message code="${domainClass.propertyName}.${prefix}${p.name}.label" default="${p.naturalName}"/> <% if (required) { %><span class="required-indicator">*</span><% } %></label>
	<div>

		<g:select class="form-control" id="${p.name}" name="${p.name}.id" from="\${((com.verecloud.nimbus4.party.Group.findAllChildrenByGroup(com.verecloud.nimbus4.party.Group.get(nimbus.findTenantId().toLong()))) + com.verecloud.nimbus4.party.Group.createCriteria().list {suppliers {eq('id', nimbus.findTenantId().toLong())}})}" optionKey="id" required="" value="\${${domainInstance}?.${p.name}?.id}" class="many-to-one"/>

	</div>
</div>
<%
	}

	private renderParentOrSupplierGroupField(p, owningClass, prefix = "") {
		String domainSuffix = "Instance"
		String domainInstance = owningClass.propertyName + domainSuffix

		boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
		boolean required = false
		if (hasHibernate) {
			cp = owningClass.constrainedProperties[p.name]
			required = (cp ? !(cp.propertyType in [boolean, Boolean]) && !cp.nullable && (cp.propertyType != String || !cp.blank) : false)
		} %>
<div class="\${hasErrors(bean: ${propertyName}, field: '${prefix}${p.name}', 'error')} ${required ? 'required' : ''}">
	<label for="${prefix}${p.name}" class="control-label"><g:message code="${domainClass.propertyName}.${prefix}${p.name}.label" default="${p.naturalName}"/> <% if (required) { %><span class="required-indicator">*</span><% } %></label>
	<div>

		<g:select class="form-control" id="${p.name}" name="${p.name}.id" noSelection="\${['': '-----Select Group-----']}" from="\${com.verecloud.nimbus4.util.Nimbus4CoreUtil.getParentAndSupplierTenants(nimbus.findTenantId().toLong())}" optionKey="id" <% if (required) { %>required=""<% } %> value="\${${domainInstance}?.${p.name}?.id}" class="many-to-one"/>

	</div>
</div>
<%
	}

	private renderFieldForProperty(p, owningClass, prefix = "") {
		boolean hasHibernate = pluginManager?.hasGrailsPlugin('hibernate') || pluginManager?.hasGrailsPlugin('hibernate4')
		boolean required = false
		if (hasHibernate) {
			cp = owningClass.constrainedProperties[p.name]
			required = (cp ? !(cp.propertyType in [boolean, Boolean]) && !cp.nullable && (cp.propertyType != String || !cp.blank) : false)
		} %>
<div class="\${hasErrors(bean: ${propertyName}, field: '${prefix}${p.name}', 'error')} ${required ? 'required' : ''}">
	<label for="${prefix}${p.name}" class="control-label"><g:message code="${domainClass.propertyName}.${prefix}${p.name}.label" default="${p.naturalName}"/> <% if (required) { %><span class="required-indicator">*</span><% } %></label>
	<div>
		<% if (domainClass?.constrainedProperties[p.name] && domainClass?.constrainedProperties[p.name]?.widget == 'ace') { %>

		${com.verecloud.nimbus4.util.Nimbus4CoreUtil.createAce(domainClass, p)}
		<% } else { %>
			${renderEditor(p)}
		<% } %>
	</div>
</div>
<%
	}

	private renderJSforMapHandling(String propertyName) {
%>
<r:script>
	\$(document).ready(function () {
		refreshEditableTableWidget();
	});

	function addBlankRow(){
		\$('#editableTable  > tbody:last').append('<tr><td><g:checkBox name="editableTableCheckbox" checked="false"/></td><td></td><td></tr>');
		refreshEditableTableWidget();
	}

	function deleteCheckedRows(){
		\$('#editableTable tr').filter(':has(:checkbox:checked)').each(function() {
			\$(this).remove();
		});
	}

	function refreshEditableTableWidget(){
		\$('#editableTable').editableTableWidget();
		\$('#editableTable tr').filter(':has(:checkbox)').each(function() {
			\$(this).find('td:first').removeAttr('tabindex');
		});
	}

	\$("form[role='form']").submit(function(){updateMapField()});
	function updateMapField(){
		var json = ""
		\$('#editableTable tr').filter(':has(:checkbox)').each(function() {
            var key = \$(this).find("td[tabindex='1']").first().text();
            if(key){
				json+='"'+key+'":'
                var value = \$(this).find("td[tabindex='1']").last().text();
                if(value){
                    //if(jQuery.isNumeric(value) || isBoolean(value)){
						//json+=value+','
                    //}else{
						json += value+','
                    //}
                }else{
                    json+='null,'
                }
            }
		});
		json = json.substr(0,json.length-1)
		json = "{" + json + "}"
		\$('#${propertyName}').val(json)
	}
</r:script>
<% } %>
