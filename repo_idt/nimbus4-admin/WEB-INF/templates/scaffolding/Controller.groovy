<%=packageName ? "package ${packageName}" : ''%>

import com.verecloud.nimbus4.common.ApplicationController
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*
/**
 * ${className}Controller
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class ${className}Controller extends ApplicationController<${className}> {
	<%
	String fullClassName = domainClass.name
	fullClassName = fullClassName.indexOf(".") > 0 ? fullClassName.substring(fullClassName.lastIndexOf(".") + 1) : fullClassName
	String instanceName = fullClassName.substring(0, 1).toLowerCase() + fullClassName.substring(1) + "Instance"

	String  refPropName
	String  refPropInstanceName

	List<org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty> uniDirectionalCollectionAssociation = domainClass.properties.findAll {
		org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
			if (!property.bidirectional && property.association && Collection.isAssignableFrom(property.type)) {
				return property
			} else {
				return false
			}
	}
	List<org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty> manyToManyBiDirectionalCollectionAssociation = domainClass.properties.findAll {
		org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
			if (property.bidirectional && property.manyToMany && property.owningSide && property.association && Collection.isAssignableFrom(property.type)) {
				return property
			} else {
				return false
			}
	}
	List<org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty> basicCollectionProperties = domainClass.properties.findAll{
		org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
			if (property.basicCollectionType){
				return property
			}else{
				return false
			}
	}
	boolean isCurrentPropertyDomainMultiTenant = false
	Closure getDomainClass = {
		String className ->
			org.codehaus.groovy.grails.commons.GrailsClass grailsClass = domainClass.grailsApplication.getDomainClasses().find {
				org.codehaus.groovy.grails.commons.GrailsClass grailsClass ->
					grailsClass.name.equalsIgnoreCase(className)
			}
			return grailsClass?.clazz
	}
	List<org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty> currentTenantParentOrSupplierDataProperties = domainClass.properties.findAll {
		org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
			if (!property.basicCollectionType && com.verecloud.nimbus4.util.Nimbus4ScaffoldingConstants.PARENT_OR_SUPPLIER_TENANT_DATA_ONLY.get(domainClass.name)?.contains(property.name)) {
				if (property.referencedDomainClass?.clazz) {
					isCurrentPropertyDomainMultiTenant = grails.plugin.multitenant.core.MultiTenantDomainClass.isAssignableFrom(property.referencedDomainClass.clazz)
				} else if (!property.persistent) {
					def domainClass = getDomainClass(property.referencedPropertyType.simpleName)

					if (domainClass) {
						isCurrentPropertyDomainMultiTenant = grails.plugin.multitenant.core.MultiTenantDomainClass.isAssignableFrom(property.domainClass.clazz)
					}
				}
				return isCurrentPropertyDomainMultiTenant
			}
			return false
	}
	%>
	public ${className}Controller() {
		super(${className}.class)
	}

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond ${className}.list(params), model:[${propertyName}Count: ${className}.count()]
	}

	def list(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond ${className}.list(params), model:[${propertyName}Count: ${className}.count()]
	}

	def show(${className} ${propertyName}) {
		respond ${propertyName}, model:[clazz:${className}]
	}

	def create() {
		respond new ${className}(params)
	}

	@Transactional
	def save(${className} ${propertyName}) {
		if (${propertyName} == null) {
			notFound()
			return
		}
		<%
		if(basicCollectionProperties || currentTenantParentOrSupplierDataProperties){
			println "\n\t\t${instanceName}.clearErrors()"
			basicCollectionProperties.each {
				org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
					println "\t\tupdate${property.name.capitalize()}(${instanceName}, params.${property.name}?.toString()?.trim()?.split(\"\\n\")?.toList())"
			}
			currentTenantParentOrSupplierDataProperties.each {
				org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
					println "\t\tupdate${property.name.capitalize()}FromParentOrSupplierTenantData(${instanceName}, params.getLong('${property.name}.id'))"
			}
			println "\t\t${instanceName}.validate()"
		}%>
		if (${propertyName}.hasErrors()) {
			respond ${propertyName}.errors, view:'create'
			return
		}

		${propertyName}.save flush:true

		request.withFormat {
			form {
				flash.message = message(code: 'default.created.message', args: [message(code: '${propertyName}.label', default: '${className}'), ${propertyName}.id])
				redirect ${propertyName}
			}
			'*' { respond ${propertyName}, [status: CREATED] }
		}
	}

	def edit(${className} ${propertyName}) {
		respond ${propertyName}
	}

	@Transactional
	def update(${className} ${propertyName}) {
		if (${propertyName} == null) {
			notFound()
			return
		}
		<%
		if(uniDirectionalCollectionAssociation || manyToManyBiDirectionalCollectionAssociation || basicCollectionProperties || currentTenantParentOrSupplierDataProperties){
			println "\n\t\t${instanceName}.clearErrors()"
			basicCollectionProperties.each {
				org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
					println "\t\tupdate${property.name.capitalize()}(${instanceName}, params.${property.name}?.toString()?.trim()?.split(\"\\n\")?.toList())"
			}
			uniDirectionalCollectionAssociation.each {
				org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
					println "\t\tupdate${property.name.capitalize()}(${instanceName}, params.${property.name} instanceof Object[] ? params.${property.name} as List : [params.${property.name}])"
			}
			manyToManyBiDirectionalCollectionAssociation.each {
				org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
					println "\t\tupdate${property.name.capitalize()}(${instanceName}, params.${property.name} instanceof Object[] ? params.${property.name} as List : [params.${property.name}])"
			}
			currentTenantParentOrSupplierDataProperties.each {
				org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
					println "\t\tupdate${property.name.capitalize()}FromParentOrSupplierTenantData(${instanceName}, params.getLong('${property.name}.id'))"
			}
			println "\t\t${instanceName}.validate()"
		}%>
		if (${propertyName}.hasErrors()) {
			respond ${propertyName}.errors, view:'edit'
			return
		}

		${propertyName}.save flush: true

		request.withFormat {
			form {
				flash.message = message(code: 'default.updated.message', args: [message(code: '${className}.label', default: '${className}'), ${propertyName}.id])
				redirect ${propertyName}
			}
			'*'{ respond ${propertyName}, [status: OK] }
		}
	}

	@Transactional
	def delete(${className} ${propertyName}) {

		if (${propertyName} == null) {
			notFound()
			return
		}

		deleteRelationships(${propertyName})

		if (${propertyName}.hasErrors()) {
			flash.error = g.message(code: ${propertyName}.errors.fieldError.code)
			redirect(controller: controllerName, action: 'show', id: ${propertyName}.id)
			return
		}

		${propertyName}.delete flush:true

		request.withFormat {
			form {
				flash.message = message(code: 'default.deleted.message', args: [message(code: '${className}.label', default: '${className}'), ${propertyName}.id])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	protected void notFound() {
		request.withFormat {
			form {
				flash.message = message(code: 'default.not.found.message', args: [message(code: '${propertyName}.label', default: '${className}'), params.id])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
	<%
	if(uniDirectionalCollectionAssociation){
		uniDirectionalCollectionAssociation.each {
			org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
				refPropName = property.referencedPropertyType.name
				refPropInstanceName = refPropName.indexOf(".") > 0 ? refPropName.substring(refPropName.lastIndexOf(".") + 1) : refPropName
				refPropInstanceName = refPropInstanceName.substring(0, 1).toLowerCase() + refPropInstanceName.substring(1) + "Obj"

				println """
	private void update${property.name.capitalize()}(${fullClassName} ${instanceName}, List<String> ${property.name}){
		${instanceName}.${property.name}?.clear()
		${refPropName} ${refPropInstanceName}
		${property.name}?.each { String id ->
			${refPropInstanceName} = ${refPropName}.get(id as Long)
            if(${refPropInstanceName}){
			    ${instanceName}.addTo${property.name.capitalize()}(${refPropInstanceName})
            }
		}
	}"""
		}
	}
	if(manyToManyBiDirectionalCollectionAssociation){
		manyToManyBiDirectionalCollectionAssociation.each {
			org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
				refPropName = property.referencedPropertyType.name
				refPropInstanceName = refPropName.indexOf(".") > 0 ? refPropName.substring(refPropName.lastIndexOf(".") + 1) : refPropName
				refPropInstanceName = refPropInstanceName.substring(0, 1).toLowerCase() + refPropInstanceName.substring(1) + "Obj"

				println """
	private void update${property.name.capitalize()}(${fullClassName} ${instanceName}, List<String> ${property.name}){
		${instanceName}.${property.name}?.clear()
		${refPropName} ${refPropInstanceName}
		${property.name}?.each { String id ->
			${refPropInstanceName} = ${refPropName}.get(id as Long)
            if(${refPropInstanceName}){
			    ${instanceName}.addTo${property.name.capitalize()}(${refPropInstanceName})
            }
		}
	}"""
		}
	}
	if(currentTenantParentOrSupplierDataProperties){
		currentTenantParentOrSupplierDataProperties.each {
			org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
				refPropName = property.referencedPropertyType.name
				refPropInstanceName = refPropName.indexOf(".") > 0 ? refPropName.substring(refPropName.lastIndexOf(".") + 1) : refPropName
				refPropInstanceName = refPropInstanceName.substring(0, 1).toLowerCase() + refPropInstanceName.substring(1) + "Obj"

				println """
	private void update${property.name.capitalize()}FromParentOrSupplierTenantData(${fullClassName} ${instanceName}, Long ${property.name}Id){
		Long currentTenant = com.verecloud.nimbus4.common.ApplicationContextHolder.currentTenant.get()?.toLong()
		List<Integer> tenantIds = com.verecloud.nimbus4.util.Nimbus4CoreUtil.getParentAndSupplierTenants(currentTenant)*.id*.intValue()

		if(tenantIds){
			com.verecloud.nimbus4.party.Group.withoutTenantRestriction {
				${instanceName}.${property.name} = ${refPropName}.findByIdAndTenantIdInList(${property.name}Id, tenantIds)
			}
		}
	}"""
		}
	}
	if(basicCollectionProperties){
		basicCollectionProperties.each {
			org.codehaus.groovy.grails.commons.DefaultGrailsDomainClassProperty property ->
				println """
	private void update${property.name.capitalize()}(${fullClassName} ${instanceName}, List<String> ${property.name}){
		${instanceName}.${property.name}?.clear()
		${property.name}?.each { String value ->
            if(value?.trim()){
			    ${instanceName}.addTo${property.name.capitalize()}(value?.trim()?.to${property.referencedPropertyType.simpleName}())
            }
		}
	}"""
		}
	}%>
}
