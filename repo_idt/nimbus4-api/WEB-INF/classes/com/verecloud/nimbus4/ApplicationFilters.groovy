package com.verecloud.nimbus4

import com.verecloud.nimbus4.multitenancy.DomainTenantResolver
import com.verecloud.nimbus4.util.Nimbus4Constants
import com.verecloud.nimbus4.util.Nimbus4CoreUtil
import org.apache.log4j.Logger
import java.util.concurrent.atomic.AtomicLong

import static org.apache.log4j.Logger.getLogger

class ApplicationFilters {

	private static final Logger log = getLogger(Nimbus4Constants.NIMBUS4_API_INCOMING_CALLS_LOGGER)

	private static final AtomicLong REQUEST_NUMBER_COUNTER = new AtomicLong()
	private static final String START_TIME_ATTRIBUTE = 'Controller__START_TIME__'
	private static final String REQUEST_NUMBER_ATTRIBUTE = 'Controller__REQUEST_NUMBER__'

	def filters = {

		requestLogFilter(controller: '*', action: '*') {
			before = {
				long start = request[START_TIME_ATTRIBUTE]?.toString()?.toLong() ?: System.currentTimeMillis()
				long currentRequestNumber = request[REQUEST_NUMBER_ATTRIBUTE]?.toString()?.toLong() ?: REQUEST_NUMBER_COUNTER.incrementAndGet()

				request[START_TIME_ATTRIBUTE] = start
				request[REQUEST_NUMBER_ATTRIBUTE] = currentRequestNumber

				log.info "preHandle request #$currentRequestNumber: " +
						"'$request.servletPath$request.forwardURI', " +
						"from $request.remoteHost ($request.remoteAddr) " +
						"by ${Nimbus4CoreUtil.securityPrincipal} (${DomainTenantResolver.currentTenantId} - $request.remoteUser) " +
						" at ${new Date()}, Ajax: $request.xhr, controller: $controllerName, action: $actionName}"

				return true
			}

			after = { Map model ->
				long start = request[START_TIME_ATTRIBUTE]?.toString()?.toLong() ?: 0
				long end = System.currentTimeMillis()
				long requestNumber = request[REQUEST_NUMBER_ATTRIBUTE]?.toString()?.toLong() ?: 0

				log.info "postHandle request #$requestNumber: end ${new Date()}, total time ${end - start}ms"
			}

			afterView = { Exception ex ->
				long start = request[START_TIME_ATTRIBUTE]?.toString()?.toLong() ?: 0
				long end = System.currentTimeMillis()
				long requestNumber = request[REQUEST_NUMBER_ATTRIBUTE]?.toString()?.toLong() ?: 0

				log.info "afterCompletion request #$requestNumber: end ${new Date()}, total time ${end - start}ms"
				if (ex) {
					log.info "exception: $ex.message", ex
				}
			}
		}
	}
}