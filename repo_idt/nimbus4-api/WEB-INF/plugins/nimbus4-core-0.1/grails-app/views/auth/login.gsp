<%@ page import="com.verecloud.nimbus4.party.UserGroupRole; com.verecloud.nimbus4.party.User; com.verecloud.nimbus4.party.GroupConfigurationService" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="kickstart"/>
    <title>Login</title>
</head>
<%
    GroupConfigurationService groupConfigurationService = grailsApplication.mainContext.getBean("groupConfigurationService");
    %>

<body>
<div class="container" style="margin-top: 6%;">
    <div class="row" id="login">

        <div class="well span5 col-sm-4 col-sm-offset-4 login-box">
            <div class='fheader'><div class="alert alert-info">Welcome to Admin Portal: Please Login</div></div>

            <g:form action='signIn' method='POST' id='loginForm' class="form-horizontal" autocomplete='off'
                    role="form">
                <input type="hidden" name="targetUri" value="${targetUri}" />
                <div class="input-group" style="margin-bottom: 5px;">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type='text' name='username' id='username' class="form-control" placeholder="Email"
                           required="required" value="${request.getParameter('username')}">
                </div>

                <div class="input-group" style="margin-bottom: 5px;">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                    <input type='password' name='password' id='password' class="form-control" placeholder="Password"
                           required="required">
                </div>

                <div class="input-group" style="margin-bottom: 5px;">
                    <g:if test="${ flash.message == message(code: "multiple.groups.associated")}">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                        <g:select id="subdomain" name="subdomain"  class="form-control"  noSelection="${['':'-----Select Group-----']}" from="${UserGroupRole.findAllByUser(User.findByEmail(params.username))*.group*.name}" required="required" value="${request.getParameter('subdomain')}"/>
                    </g:if>
                </div>

                <div class="form-group">
                    <div class="col-sm-8">
                        %{--<div class="checkbox" id="remember_me_holder">--}%
                            %{--<label>--}%
                                %{--<input type='checkbox' name='rememberMe' id='remember_me'--}%
                                       %{--value="${rememberMe}"/>Remember me--}%
                            %{--</label>--}%
                        %{--</div>--}%
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button type='submit' id="submit" class="btn btn-primary" style="width: 100%"
                                value='Login"'>Login&nbsp;<span
                                class="glyphicon glyphicon-log-in"></span></button>
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
<g:javascript>
    $("#username").focus()
</g:javascript>
</body>
</html>
