<?xml version="1.0" encoding="UTF-8"?>
<service>

	<name>Symantec EndPoint Protection</name>
	<description>Symantec EndPoint Protection</description>
	<controlPanelURL>https://clients.partnerdev.messagelabs.net/</controlPanelURL>
	<logo>prodBrand1000-small.png</logo>
	<version>3</version>
	<serviceSpecificationId>SSP11-000-000-003</serviceSpecificationId>
	<autoSyncSchedule>00 00 03 * * ? *</autoSyncSchedule>
	<autoSyncTimeZone>GMT-6</autoSyncTimeZone>

	<groupService>
		<attributes label="customerName" id="accountId">
			<attribute>
				<name>accountId</name>
				<labelCode>symantec.emailProtection.group.accountId.label</labelCode>
				<descriptionCode>symantec.emailProtection.group.accountId.description</descriptionCode>
				<type>string</type>
				<required>false</required>
				<discovery>true</discovery>
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>1</order>
			</attribute>
			<attribute>
				<name>accountReference</name>
				<labelCode>symantec.endpointProtection.group.accountReference.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.accountReference.description</descriptionCode>
				<type>string</type>
				<required>false</required>
				<discovery>true</discovery>
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>2</order>
			</attribute>

			<attribute>
				<name>customerName</name>
				<labelCode>symantec.endpointProtection.group.customerName.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.customerName.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="${group.name}" />
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<validators>
					<validator type="containsAlphabeticChars">
						<param name="min" value="3"/>
						<param name="length" value="4"/>
					</validator>
				</validators>				
				<multiplicity>one</multiplicity>
				<order>3</order>
			</attribute>
			<attribute>
				<name>contactFirstName</name>
				<labelCode>symantec.endpointProtection.group.contactFirstName.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.contactFirstName.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="First Name" />
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>4</order>
			</attribute>

			<attribute>
				<name>contactLastName</name>
				<labelCode>symantec.endpointProtection.group.contactLastName.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.contactLastName.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="Last Name" />
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>5</order>
			</attribute>

			<attribute>
				<name>contactEmailAddress</name>
				<labelCode>symantec.endpointProtection.group.contactEmailAddress.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.contactEmailAddress.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="admin@${'${group.name}.com'?replace('.com.com', '.com')}" />
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<validators>
					<validator type="email" />
				</validators>
				<order>6</order>
			</attribute>

			<attribute>
				<name>endpointService</name>
				<labelCode>symantec.endpointProtection.group.endpoint.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.endpoint.description</descriptionCode>
				<type>boolean</type>
				<required>true</required>
				<defaultValue expression="true"/>
				<discovery>false</discovery>
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>editable</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>7</order>
			</attribute>
			<attribute>
				<name>totalEndPointCount</name>
				<labelCode>symantec.endpointProtection.group.totalEndPointCount.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.totalEndPointCount.description</descriptionCode>
				<type>integer</type>
				<required>true</required>
				<discovery>false</discovery>
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>editable</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>8</order>
			</attribute>
			<attribute>
				<name>backup</name>
				<labelCode>symantec.endpointProtection.group.backup.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.backup.description</descriptionCode>
				<type>boolean</type>
				<required>true</required>
				<defaultValue expression="true"/>
				<discovery>false</discovery>
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>editable</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>9</order>
			</attribute>
			<attribute>
				<name>backupCapacity</name>
				<labelCode>symantec.endpointProtection.group.backupCapacity.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.backupCapacity.description</descriptionCode>
				<type>integer</type>
				<required>true</required>
				<discovery>false</discovery>
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>editable</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>10</order>
			</attribute>

			<attribute>
				<name>country</name>
				<labelCode>symantec.endpointProtection.group.isoCountryCode.label</labelCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="${group.groupAddress.country}" />
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>11</order>
			</attribute>
			<attribute>
				<name>state</name>
				<labelCode>symantec.endpointProtection.group.state.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.state.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression='${group.groupAddress.state!"None (International)"}' />
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>12</order>
			</attribute>

			<attribute>
				<name>city</name>
				<labelCode>symantec.endpointProtection.group.town.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.town.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<defaultValue expression='${group.groupAddress.city}' />
				<discovery>false</discovery>
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>13</order>
			</attribute>

			<attribute>
				<name>zipCode</name>
				<labelCode>symantec.endpointProtection.group.zip.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.zip.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="${group.groupAddress.zipCode}" />
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>14</order>
			</attribute>

			<attribute>
				<name>address1</name>
				<labelCode>symantec.endpointProtection.group.address1.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.address1.description</descriptionCode>
				<type>string</type>
				<required>true</required>
				<discovery>false</discovery>
				<defaultValue expression="${group.groupAddress.addressLine1}" />
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>15</order>
			</attribute>
			<attribute>
				<name>address2</name>
				<labelCode>symantec.endpointProtection.group.address2.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.address2.description</descriptionCode>
				<type>string</type>
				<required>false</required>
				<discovery>false</discovery>
				<defaultValue expression='${group.groupAddress.addressLine2!""}' />
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>read-only</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>16</order>
			</attribute>

			<attribute>
				<name>initialNumberOfUsers</name>
				<labelCode>symantec.endpointProtection.group.initialNumberOfUser.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.initialNumberOfUser.description</descriptionCode>
				<type>integer</type>
				<required>true</required>
				<discovery>false</discovery>
				<orderEntryVisibility>editable</orderEntryVisibility>
				<controlPanelVisibility>hidden</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>17</order>
				<validators>
					<validator type="integerMultiple10">
						<param name="min" value="10" />
					</validator>
				</validators>				
			</attribute>
			<attribute>
				<name>password</name>
				<labelCode>symantec.endpointProtection.group.password.label</labelCode>
				<descriptionCode>symantec.endpointProtection.group.password.description</descriptionCode>
				<type>string</type>
				<required>false</required>
				<discovery>false</discovery>
				<orderEntryVisibility>hidden</orderEntryVisibility>
				<controlPanelVisibility>hidden</controlPanelVisibility>
				<multiplicity>one</multiplicity>
				<order>18</order>
			</attribute>
		</attributes>
 		<fulfillmentTransactions>
			<transaction name="addService"/>
			<transaction name="changeService"/>
			<transaction name="deleteService"/>
			<transaction name="suspendService"/>
			<transaction name="reactivateService"/>
		</fulfillmentTransactions>

		<usage>
			<schedule>00 00 02 * * ? *</schedule>
			<scheduleTimeZone>GMT-6</scheduleTimeZone>
			<data>
				<dataField>
					<name>endpointService</name>
					<labelCode>symantec.endpointProtection.usage.hasEndpointService</labelCode>
					<type>boolean</type>
				</dataField>
				<dataField>
					<name>backup</name>
					<labelCode>symantec.endpointProtection.usage.hasBackup</labelCode>
					<type>boolean</type>
				</dataField>
				<dataField>
					<name>totalEndPointCount</name>
					<labelCode>symantec.endpointProtection.usage.usedEndpointService</labelCode>
					<type>integer</type>
				</dataField>
				<dataField>
					<name>backupCapacity</name>
					<labelCode>symantec.endpointProtection.usage.usedBackup</labelCode>
					<type>integer</type>
					<unit>GB</unit>
				</dataField>
			</data>
		</usage>
	</groupService>

	<serviceStatistics>
		<defaultFrequency>300000</defaultFrequency>
		<maxTimeRetrieve>1500000</maxTimeRetrieve>
		<data>
			<dataField>
				<name>available</name>
				<labelCode>symantec.endpointProtection.statistics.datafield.available.label</labelCode>
				<descriptionCode>symantec.endpointProtection.statistics.datafield.available.description
				</descriptionCode>
				<type>boolean</type>
			</dataField>
			<dataField>
				<name>responseTime</name>
				<labelCode>symantec.endpointProtection.statistics.datafield.responseTime.label
				</labelCode>
				<descriptionCode>symantec.endpointProtection.statistics.datafield.responseTime.description
				</descriptionCode>
				<type>integer</type>
			</dataField>
		</data>
	</serviceStatistics>

</service>
