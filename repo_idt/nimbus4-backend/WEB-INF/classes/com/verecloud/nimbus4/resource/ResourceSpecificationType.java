package com.verecloud.nimbus4.resource;

public enum ResourceSpecificationType {
	SUPPLIER, PARENT
}
