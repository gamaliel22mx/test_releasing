package com.verecloud.nimbus4.processor.adapters;

import javax.xml.bind.annotation.XmlElement;

public class ProductCodeEntry {
     String regularExpression;
     String externalProductCode;

    @XmlElement(name = "product")
    public String getRegularExpression() {
        return regularExpression;
    }

    public void setRegularExpression(String regularExpression) {
        this.regularExpression = regularExpression;
    }

    @XmlElement(name = "productCode")
    public String getExternalProductCode() {
        return externalProductCode;
    }

    public void setExternalProductCode(String externalProductCode) {
        this.externalProductCode = externalProductCode;
    }
}