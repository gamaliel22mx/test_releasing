package com.verecloud.nimbus4.processor.adapters;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "productCodes")
public class ProductCodesDictionary {
     String defaultProductExternalCode;
     List<ProductCodeEntry> productCodes;

    @XmlElement(name = "productCode")
    public List<ProductCodeEntry> getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(List<ProductCodeEntry> productCodes) {
        this.productCodes = productCodes;
    }

    @XmlAttribute(name = "default")
    public String getDefaultProductExternalCode() {
        return defaultProductExternalCode;
    }

    public void setDefaultProductExternalCode(String defaultProductExternalCode) {
        this.defaultProductExternalCode = defaultProductExternalCode;
    }
}