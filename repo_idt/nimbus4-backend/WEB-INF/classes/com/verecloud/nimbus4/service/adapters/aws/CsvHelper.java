package com.verecloud.nimbus4.service.adapters.aws;

import com.csvreader.CsvReader;
import com.verecloud.nimbus4.service.InvoiceCsvLine;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Helper to process the csv file
 * 
 * @author pmagnago
 * 
 */
public class CsvHelper {

	/**
	 * Return a instance of CsvReader from a InputStream
	 * 
	 * @param inputStream
	 * @param readHeaders
	 * @return CsvReader
	 * @throws Exception
	 */
	public CsvReader getCsvReader(InputStream inputStream, boolean readHeaders)
			throws Exception {

		CsvReader csvReader = new CsvReader(new BufferedReader(
				new InputStreamReader(inputStream)));
		if (readHeaders) {
			csvReader.readHeaders();
		}
		return csvReader;
	}

	/**
	 * Create a new ChargingCsvLine
	 * 
	 * @param csvReader
	 * @param accountId
	 * @param chargeType
	 * @param amount
	 * @return ChargingCsvLine
	 * @throws Exception
	 */
	public InvoiceCsvLine createChargingCsvLine(CsvReader csvReader,
			String accountId, String chargeType, String amount)
			throws Exception {

		InvoiceCsvLine chargingCsvLine = new InvoiceCsvLine(accountId);
		chargingCsvLine.setChargeType(chargeType);

		// suppress charge amount for Reseller Discount records
		double totalAmount = Double.parseDouble(csvReader.get(amount));
		String chargeDescript = csvReader
				.get(AwsConstants.AMAZON_COLUMN_CHARGE_DESCRIPTION);
		if ((totalAmount < 0)
				&& (chargeDescript.contains("AWS Reseller Program Discount"))) {
			chargingCsvLine.setAmount("0");
		} else {
			chargingCsvLine.setAmount(csvReader.get(amount));
		}

		String invoiceId = csvReader.get(AwsConstants.AMAZON_COLUMN_INVOICE_ID);
		String chargeMode = AwsConstants.CHARGE_MODE_INVOICE_DEFAULT_VALUE;

		try {
			Integer.valueOf(invoiceId);
		} catch (NumberFormatException e) {
			invoiceId = "";
			chargeMode = AwsConstants.CHARGE_MODE_ESTIMATE_DEFAULT_VALUE;
		}

		chargingCsvLine.setInvoiceId(invoiceId);
		chargingCsvLine.setChargeMode(chargeMode);

		// invoice date sometimes crosses billing cycle range so we need to use bill cycle end date.
		chargingCsvLine.setDate(csvReader
				.get(AwsConstants.AMAZON_COLUMN_BILL_PERIOD_END_DATE));

		chargingCsvLine.setAccountName(StringUtils.isBlank((csvReader
				.get(AwsConstants.AMAZON_COLUMN_ACCOUNT_NAME))) ? csvReader
				.get(AwsConstants.AMAZON_COLUMN_PAYER_ACCOUNT_NAME) : csvReader
				.get(AwsConstants.AMAZON_COLUMN_ACCOUNT_NAME));
		chargingCsvLine.setProductCode(csvReader
				.get(AwsConstants.AMAZON_COLUMN_PRODUCT_CODE));
		
		// Temp hack to handle empty charge code lines from marketplace orders.  Assume any empty codes as this type.
		if (StringUtils.isBlank(csvReader.get(AwsConstants.AMAZON_COLUMN_CHARGE_CODE))) {
			chargingCsvLine.setChargeCode("AWS:Marketplace");
		} else {
			chargingCsvLine.setChargeCode(csvReader.get(AwsConstants.AMAZON_COLUMN_CHARGE_CODE));
		}
		chargingCsvLine.setChargeDescription(csvReader
				.get(AwsConstants.AMAZON_COLUMN_CHARGE_DESCRIPTION));
		chargingCsvLine.setCurrency(csvReader
				.get(AwsConstants.AMAZON_COLUMN_CURRENCY));
		// chargingCsvLine.setQuantity(csvReader.get(AwsConstants.AMAZON_COLUMN_USAGE_QUANTITY));

		// set default value (RESELLER)
		chargingCsvLine.setRateType(AwsConstants.RATE_TYPE_DEFAULT_VALUE);

		return chargingCsvLine;
	}

}
